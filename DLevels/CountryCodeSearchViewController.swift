//
//  CountryCodeSearchViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 05/07/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import Foundation

protocol MyProtocol {
    func setResultOfBusinessLogic(valueSent: String)
}

class CountryCodeSearchViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate

{

    @IBOutlet weak var tblvwresult: UITableView!
    @IBOutlet weak var searchcontroller: UISearchBar!
    
    var delegate:MyProtocol?
    
    var countryListArray = [NSDictionary]()
    var countryListArrayToBindTableView = [NSDictionary]()
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.isOpaque = false
        view.backgroundColor = .black // try other colors, say: .white or black with Alpha etc
        addLoader()
        actInd.startAnimating()
        countrylist()
        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // called whenever text is changed.
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        loadDataForSearchResult(termval: searchText)
        if(searchText == "")
        {
            //ba self.countryListArray.removeAll()
            self.tblvwresult.reloadData()
            
        }
    }
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }

    func loadDataForSearchResult(termval : String)
    {
        
        self.countryListArray.removeAll()
        for dict in self.countryListArrayToBindTableView {
            
            let countryname = dict.value(forKey: "CountryName") as! String
            
            
            if(countryname.hasPrefix(termval))
            {
                countryListArray.append(dict)
            }
            if (termval == ""){
                countryListArray = countryListArrayToBindTableView
            }
            
            
        }
        
        //	print(countryListArray)
        self.tblvwresult.reloadData()
    }
    
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return countryListArray.count
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35.0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

        CountryCode.countryCode = countryListArray[indexPath.row].value(forKey: "CountryCode") as! String

          self.navigationController?.popViewController(animated: false)
               
    }
    
    
    
    
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"searchcell", for: indexPath)
        cell.selectionStyle = .none
        
        
        
        let index = countryListArray[indexPath.row]
        
        let CountryName = cell.viewWithTag(1001) as! UILabel
        CountryName.text = index.value(forKey: "CountryName") as? String
        CountryName.adjustsFontSizeToFitWidth = true
        
        
        
               
        let CountryCode = cell.viewWithTag(1002) as! UILabel
        CountryCode.text = index.value(forKey: "CountryCode") as? String
        
        
        
        return cell
    }
    
    //MARK: Getting Data From Array
    func countrylist()
    {
        let obj = WebService()
        self.actInd.startAnimating()
        obj.callWebServices(url: Urls.country_code, methodName: "GET", parameters: "", istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            //print("country code : \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                let tempArrayDict : [NSDictionary] = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for dictValue in tempArrayDict {
                    
                    self.countryListArray.append(dictValue)
                    self.countryListArrayToBindTableView.append(dictValue)
                    
                }
                
                
                
                DispatchQueue.main.async {
                    
//                    if self.countryListArray.count == 0 {
//                        self.lblNodataAvailable.isHidden = false
//                    } else {
//                        self.lblNodataAvailable.isHidden = true
//                    }
                    let sectionIndex = IndexSet(integer: 0)
                    self.tblvwresult.reloadSections(sectionIndex, with: .automatic)
                    self.actInd.stopAnimating()
                }
            }
            else
            {
                //let alertobj = AppManager()
                
                // print(jsonData.value(forKey: "errmsg") as! String)
                
            }
        }
    }

    
    
    
    @IBAction func btnCancelClick(_ sender: Any) {
       navigationController?.popViewController(animated: true)
        
    }
    
    
}
