//
//  NRIFPIViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 16/10/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class NRIFPIViewController: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var txtData: UITextView!
    @IBOutlet weak var webvw: UIWebView!
    @IBOutlet weak var btnHamberger: UIButton!
    var isShowingMenu = false
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    var dataArr:     [NSDictionary] = []
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webvw.delegate = self
        getData()
        addLoader()
        
        webvw.scrollView.showsHorizontalScrollIndicator =  false
        
        
        
        // Do any additional setup after loading the view.
    }
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Getting Data
    func getData()
    {
        let obj = WebService()
        self.actInd.startAnimating()
        obj.callWebServices(url: Urls.NRIFPI, methodName: "GET", parameters: "", istoken: false, tokenval: "") { (returnValue, jsonData) in
           // print(jsonData)
            if jsonData.value(forKey: "errmsg") as! String == "" {
                let respDic = jsonData.value(forKey: "response") as! NSArray
                for dict in respDic {
                    let dictValue = dict as! NSDictionary
                    self.dataArr.append(dictValue)
                }
                DispatchQueue.main.async {
                    print(self.dataArr)
                    
                    if let benefits = self.dataArr[0].value(forKey: "introduction") as? String {
                        
                        do {
                            let benefitsAttibutedString    = try NSAttributedString(data: benefits.data(using: String.Encoding.utf8)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                            
                            self.txtData.attributedText = benefitsAttibutedString
                            self.webvw.loadHTMLString(benefits, baseURL: nil)
                            self.actInd.stopAnimating()
                            
                        } catch {
                            self.actInd.stopAnimating()
                            print(error)
                        }
                        
                    }
                }
                    
                    /*
                    self.webvw?.loadHTMLString(self.dataArr[0].value(forKey: "introduction") as! String!, baseURL: nil) */
                
            }
        }
    }
    
    
    @IBAction func onClick_Hamberger(_ sender: UIButton) {
        
        if !isShowingMenu {
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            btnHamberger.frame.origin.x += 250
            isShowingMenu = true
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            btnHamberger.frame.origin.x -= 250
            isShowingMenu = false
        }
        
    }
    
    @IBAction func btnNRIClick(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "NRIViewController") as! NRIViewController
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webvw.frame.size.height = 1
        webvw.frame.size = webvw.sizeThatFits(.zero)
        webvw.scrollView.isScrollEnabled = false
        self.view.layoutIfNeeded()
    }
    
    @IBAction func btnFPI(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "FPIViewController") as! FPIViewController
        self.navigationController?.pushViewController(viewController, animated: false)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
