//
//  Terms&ConditionsViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 23/05/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class Terms_ConditionsViewController: UIViewController {
    
    @IBOutlet weak var btnHamburger: UIButton!
    
    
    @IBOutlet weak var lblDisclaimer: UILabel!
    
    var isShowingMenu = false
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        var disclosureText = ""
        let att = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 14)]
        let boldText = NSMutableAttributedString(string:disclosureText, attributes:att)
        
        // UIView.animate(withDuration: 0.2) {
        
        disclosureText =
            "By using this service the user agrees to indemnify, defend and hold harmless the owner, its suppliers, agents, directors, officers, employees, representatives, successors and assignees from and against any and all claims, damages, loss, liabilities, costs and expenses, including legal fees, arising out of or in connection with the service by the user.\n" +
            "The content or information contained or any related service, is provided on an “as is” basis and without warranties of any kind, express or implied, as to the service / data, hence the data / price/ charts/ buy or sell indicatives, provided here may not be accurate, meaning they may be indicative and not appropriate. Therefore Dynamic Levels or its owners do not bear any responsibility if there is a trading loss by using the data / price / service.\n" +
            "\n" +
            "Access to and use of this information therein is at the user’s risk and the owner does not undertake any accountability for any irregularities, viruses or damage to any handheld device that results from accessing, availing or downloading of any information. This service does not constitute an offer of sale or a solicitation to make investment or trading to any person in any jurisdiction.\n" +
            "\n" +
            "All intellectual property rights are owned by Dynamic Equities Pvt. Ltd. or its owners, absolutely. No part shall be reproduced, redistributed, commercially exploited, stored in retrieval system, or transmitted in any form or by any means – electronic, electrostatic, magnetic tape, mechanical, printing, photocopying, recording, or otherwise including the right of translation in any language, without the express permission of the owner. The contents herein may be used only for personal and non-commercial use. The owner retains the complete right to bar any person from using the service, at its discretion, without any prior notice.\n" +
            "\n" +
            "Any dispute arising between the registered user and the owner shall be subject to the jurisdiction of the relevant authority and court of law situated in Kolkata and governed by the laws of India\n" +
            "\n" +
        "Hence NRI’S or Foreign Nationals users are recommended to MultibaggerLanding their eligibility after verification as per the laws of their land and for which Dynamic Equities Pvt. Ltd or the owners shall not be held liable.\n"
        
        
        
        /* let disclaimerText = "By using this service, the user agrees to indemnify, defend and hold harmless the owner, its suppliers, agents, directors, officers, employees, representatives, successors and assignees from and against any and all claims, damages, loss, liabilities, costs and expenses, including legal fees, arising out of or in connection with the service by the user. \n" +
         "The content or information contained or any related service, is provided on an “as is” basis and without warranties of any kind, express or implied, as to the service / data, hence the data / price/ charts/ buy or sell indicatives, provided here may not be accurate, meaning they may be indicative and not appropriate. Therefore Dynamic Levels or its owners do not bear any responsibility if there is a trading loss by using the data / price / service.\n" +
         
         "Access to and use of this information therein is at the user’s risk and the owner does not undertake any accountability for any irregularities, viruses or damage to any handheld device that results from accessing, availing or downloading of any information. This service does not constitute an offer of sale or a solicitation to make investment or trading to any person in any jurisdiction.\n" +
         
         "All intellectual property rights are owned by Dynamic Equities Pvt. Ltd. or its owners, absolutely. No part shall be reproduced, redistributed, commercially exploited, stored in retrieval system, or transmitted in any form or by any means – electronic, electrostatic, magnetic tape, mechanical, printing, photocopying, recording, or otherwise including the right of translation in any language, without the express permission of the owner. The contents herein may be used only for personal and non-commercial use. The owner retains the complete right to bar any person from using the service, at its discretion, without any prior notice.\n" +
         
         "Any dispute arising between the registered user and the owner shall be subject to the jurisdiction of the relevant authority and court of law situated in Kolkata and governed by the laws of India.\n" +
         
         "Hence NRI’S or Foreign Nationals users are recommended to check their eligibility after verification as per the laws of their land and for which Dynamic Equities Pvt. Ltd or the owners shall not be held liable.\n" */
        
        let normalDisclaimer = NSMutableAttributedString(string: disclosureText)
        boldText.append(normalDisclaimer)
        
        self.lblDisclaimer.attributedText = boldText
        
        
        //   }
        
        view.layoutIfNeeded()

        
    }
    
    @IBAction func btnback(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
        /*DispatchQueue.main.async {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUPScreenViewController") as! SignUpViewController
             self.navigationController?.pushViewController(viewController, animated: false)
            
        }*/
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
