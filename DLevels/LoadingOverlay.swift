//
//  LoadingOverlay.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 17/05/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
public class LoadingOverlay{
    
    var overlayView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    class var shared: LoadingOverlay {
        struct Static {
            static let instance: LoadingOverlay = LoadingOverlay()
        }
        return Static.instance
    }
    
    public func showOverlay(view: UIView) {
        
        
        overlayView.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        overlayView.center = view.center
        
        overlayView.backgroundColor = UIColor(colorLiteralRed: 68/255, green: 68/255, blue: 68/255, alpha: 0.7)
        
        overlayView.clipsToBounds = true
        overlayView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        
        
        activityIndicator.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
        
        overlayView.addSubview(activityIndicator)
        view.addSubview(overlayView)
        
        activityIndicator.startAnimating()
        
        view.isUserInteractionEnabled = false
    }
    
    public func hideOverlayView(view: UIView) {
        activityIndicator.stopAnimating()
        overlayView.removeFromSuperview()
        view.isUserInteractionEnabled = true
    }
}
