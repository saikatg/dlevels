//
//  ChartViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 10/04/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import Charts

class ChartViewController: UIViewController,ChartViewDelegate {
    
   
    
    @IBOutlet weak var lineChartView: LineChartView!
    
    var months = [String]()
    var values = [String]()
    var levelval = [String]()
    var dates = [String]()
    
    override func viewDidLoad() {
      
        super.viewDidLoad()
        //barChartView.noDataText = "You need to provide data for the chart."
        //let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        //let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]
        
       // setChart(dataPoints: months, values: unitsSold)
     
                 setChart()
        
       /* self.lineChartView.delegate = self
        // 2
        self.lineChartView.descriptionText = "Tap node for details"
        // 3
        self.lineChartView.descriptionTextColor = UIColor.white
        self.lineChartView.gridBackgroundColor = UIColor.darkGray
        // 4
        self.lineChartView.noDataText = "No data provided"
        // 5
        setChartData(months: months)*/
        
        // Do any additional setup after loading the view.
    }
    func getdata(){
    
        let obj = WebService()
        let paremeters = "symbol=ACC%20IS%20EQUITY"
        var result = ""
        var resultval = ""
        var chartData=""
        var linechartDataSet = LineChartDataSet()
        
        //actInd.startAnimating()
        let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3dzLmRsZXZlbHMuY29tL2xvZ2luIiwiaWF0IjoxNDkxODkzNzg2LCJuYmYiOjE0OTE4OTM3ODYsImp0aSI6ImhCa0ZWdXFrdTFhN25DRFgiLCJzdWIiOjIwOTk4OCwieHBhc3MiOiIkUCRCTHVBdDVrYTBUOGc0N2hiYllHV2huN2VUOVhiMDAwIn0.HdXThhfTuf3uFpCOhTXmjZP_BwcA3q_3jFKTA97y1qc"
        
        
        //Mark: Call Service for Chart Data
        obj.callWebServices(url: Urls.chart_data, methodName: "GET", parameters: paremeters, istoken: true, tokenval: token) { (returnValue, jsonData) in
            
            print("Json Data is :  \(jsonData)")
            if jsonData.value(forKey: "errmsg") as! String == "" {
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                for dict in tempArray {
                    let dictValue = dict as! NSDictionary
                    result = dictValue.value(forKey: "DISPLAY_DATE") as! String
                    self.months.append(result)
                    self.values.append(dictValue.value(forKey: "Volume") as! String)
                    
                    
                }
            }
    
        }
    
        obj.callWebServices(url: Urls.correction_report, methodName: "GET", parameters: paremeters, istoken: true, tokenval: token) { (returnValue, jsonData) in
            
            print("Json Data is :  \(jsonData)")
            if jsonData.value(forKey: "errmsg") as! String == "" {
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                for dict in tempArray {
                    let dictValue = dict as! NSDictionary
                    resultval = dictValue.value(forKey: "Level_Val") as! String
                    self.levelval.append(resultval)
                    // self.dates.append(dictValue.value(forKey: "Level_Date") as! String)
                    
                    
                }
                //barChartView.noDataText = "You need to provide data for the chart."
                
                var dataEntries: [ChartDataEntry] = []
                
                for i in 0..<self.levelval.count {
                    
                    // let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
                    let dataEntry = ChartDataEntry(x: Double(i), y: Double(i))
                    dataEntries.append(dataEntry)
                }
            }
        }
    
    setChart()
    
    }
    
    //func setChart(dataPoints: [String], values: [Double]) {
         func setChart() {
            
        lineChartView.delegate=self
            
        let obj = WebService()
        let paremeters = "symbol=ACC%20IS%20EQUITY"
            var result = ""
            var resultval = ""
        var chartData=""
        var linechartDataSet = LineChartDataSet()
            
        //actInd.startAnimating()
        let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3dzLmRsZXZlbHMuY29tL2xvZ2luIiwiaWF0IjoxNDkxODkzNzg2LCJuYmYiOjE0OTE4OTM3ODYsImp0aSI6ImhCa0ZWdXFrdTFhN25DRFgiLCJzdWIiOjIwOTk4OCwieHBhc3MiOiIkUCRCTHVBdDVrYTBUOGc0N2hiYllHV2huN2VUOVhiMDAwIn0.HdXThhfTuf3uFpCOhTXmjZP_BwcA3q_3jFKTA97y1qc"
       
            
            //Mark: Call Service for Chart Data
        obj.callWebServices(url: Urls.chart_data, methodName: "GET", parameters: paremeters, istoken: true, tokenval: token) { (returnValue, jsonData) in
            
            print("Json Data is :  \(jsonData)")
             if jsonData.value(forKey: "errmsg") as! String == "" {
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                for dict in tempArray {
                   let dictValue = dict as! NSDictionary 
                    result = dictValue.value(forKey: "DISPLAY_DATE") as! String
                    self.months.append(result)
                     self.values.append(dictValue.value(forKey: "Volume") as! String)
                    
                    
                }
                //barChartView.noDataText = "You need to provide data for the chart."
                
                var dataEntries: [ChartDataEntry] = []
                 
                 for i in 0..<self.months.count {
                 
                 // let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
                 let dataEntry = ChartDataEntry(x: Double(i), y: Double(self.values[i])!)                 
                 dataEntries.append(dataEntry)
                 }
                 
                 linechartDataSet = LineChartDataSet(values: dataEntries, label: "Units Sold")
                 linechartDataSet.axisDependency = .left
                 linechartDataSet.circleColors = [NSUIColor.white]
                 linechartDataSet.setCircleColor(UIColor.red)
                 linechartDataSet.circleRadius = 2.0 // the radius of the node circle
                 linechartDataSet.fillAlpha = 65 / 255.0
                 linechartDataSet.fillColor = UIColor.red
                 linechartDataSet.highlightColor = UIColor.white
                 linechartDataSet.drawCircleHoleEnabled = true
                
                 
               
                 self.lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: self.months)
                 self.lineChartView.xAxis.granularity = 1
                 self.lineChartView.xAxis.labelPosition = .bottom
                 self.lineChartView.rightAxis.enabled = false
                 self.lineChartView.data?.setDrawValues(false)
                 self.lineChartView.leftAxis.drawGridLinesEnabled = false
                 self.lineChartView.xAxis.drawGridLinesEnabled = false
                
                 //chartData = LineChartData(dataSet: linechartDataSet)
                //self.lineChartView.data = chartData
                
            
            }
            
            
            
        }
        
        
        /* END Chart Section */
            
 
            
             //Mark: Call Service for Correction Report in Chart
            obj.callWebServices(url: Urls.correction_report, methodName: "GET", parameters: paremeters, istoken: true, tokenval: token) { (returnValue, jsonData) in
                
                print("Json Data is :  \(jsonData)")
                if jsonData.value(forKey: "errmsg") as! String == "" {
                    let tempArray = jsonData.value(forKey: "response") as! NSArray
                    for dict in tempArray {
                        let dictValue = dict as! NSDictionary
                        resultval = dictValue.value(forKey: "Level_Val") as! String
                        self.levelval.append(resultval)
                        // self.dates.append(dictValue.value(forKey: "Level_Date") as! String)
                        
                        
                    }
                    //barChartView.noDataText = "You need to provide data for the chart."
                    
                    var dataEntries: [ChartDataEntry] = []
                    
                    for i in 0..<self.levelval.count {
                        
                        // let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
                        let dataEntry = ChartDataEntry(x: Double(i), y: Double(i))
                        dataEntries.append(dataEntry)
                    }
                    
                    let linechartDataSet1 = LineChartDataSet(values: dataEntries, label: "")
                    linechartDataSet1.axisDependency = .left
                    linechartDataSet1.drawValuesEnabled = false
                    linechartDataSet1.drawCirclesEnabled = false
                    linechartDataSet1.circleColors = [NSUIColor.white]
                    linechartDataSet1.setCircleColor(UIColor.white)
                    linechartDataSet1.circleRadius = 0.0 // the radius of the node circle
                    //linechartDataSet1.fillAlpha = 65 / 255.0
                    linechartDataSet1.fillColor = UIColor.white
                    linechartDataSet1.highlightColor = UIColor.white
                    linechartDataSet1.drawCircleHoleEnabled = false
                    linechartDataSet1.colors = [UIColor.white]
                    linechartDataSet1.valueColors = [UIColor.white]
 
                    
                    
                   
                    //let chartData1 = LineChartData(dataSet: linechartDataSet1)
                    //self.lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: self.dates)
                    //self.lineChartView.xAxis.granularity = 1
                    //self.lineChartView.xAxis.labelPosition = .bottom
                    //self.lineChartView.xAxis.enabled = false
                    self.lineChartView.leftAxis.valueFormatter = IndexAxisValueFormatter(values: self.levelval)
                    self.lineChartView.rightAxis.enabled = false
                    self.lineChartView.data?.setDrawValues(false)
                    self.lineChartView.leftAxis.drawGridLinesEnabled = true
                    self.lineChartView.xAxis.drawGridLinesEnabled = false
                    self.lineChartView.doubleTapToZoomEnabled = true
 
                    
                    
                    //self.lineChartView.data = chartData1
                    var dataSets : [LineChartDataSet] = [LineChartDataSet]()
                    dataSets.append(linechartDataSet)
                   // dataSets.append(linechartDataSet1)
                    
                    //4 - pass our months in for our x-axis label value along with our dataSets
                    let data: LineChartData = LineChartData(dataSets: dataSets)
                    data.setValueTextColor(UIColor.red)
                    
                    //5 - finally set our data
                    self.lineChartView.data = data
                    
                }
                
                
                
            }
        
    }
    
    
    
    
    
   /* func setChartData(months : [String]) {
    
     
        let dollars1 = [1453.0,2352,5431,1442,5451,6486,1173,5678,9234,1345,9411,2212]
        // 1 - creating an array of data entries
        var yVals1 : [ChartDataEntry] = [ChartDataEntry]()
        for i in 0 ..< months.count {
            yVals1.append(ChartDataEntry(x: dollars1[i], y: Double(i)))
        }
        
        // 2 - create a data set with our array
        let set1: LineChartDataSet = LineChartDataSet(values: yVals1, label: "First Set")
        set1.axisDependency = .left // Line will correlate with left axis values
        set1.setColor(UIColor.red.withAlphaComponent(0.5)) // our line's opacity is 50%
        set1.setCircleColor(UIColor.red) // our circle will be dark red
        set1.lineWidth = 2.0
        set1.circleRadius = 6.0 // the radius of the node circle
        set1.fillAlpha = 65 / 255.0
        set1.fillColor = UIColor.red
        set1.highlightColor = UIColor.white
        set1.drawCircleHoleEnabled = true
        
        //3 - create an array to store our LineChartDataSets
        var dataSets : [LineChartDataSet] = [LineChartDataSet]()
        dataSets.append(set1)
        
        //4 - pass our months in for our x-axis label value along with our dataSets
        let data: LineChartData = LineChartData(xVals: months, dataSets: dataSets)
        data.setValueTextColor(UIColor.white)
        
        //5 - finally set our data
        self.lineChartView.data = data
    
    
    }*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
