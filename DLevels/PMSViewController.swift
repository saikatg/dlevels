//
//  PMSViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 16/10/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class PMSViewController: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var btnHamberger: UIButton!
    @IBOutlet weak var txtDesc: UITextView!
    @IBOutlet weak var wbWebView: UIWebView!
    
    var isShowingMenu = false
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    var dictValue:          [NSDictionary] = []
    var dataArr:     [NSDictionary] = []

    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wbWebView.delegate = self
        getData()
        addLoader()
        // Do any additional setup after loading the view.
    }
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Getting Data
    func getData()
    {
        let obj = WebService()        
        self.actInd.startAnimating()
        obj.callWebServices(url: Urls.PMS, methodName: "GET", parameters: "", istoken: false, tokenval: "") { (returnValue, jsonData) in
            //print(jsonData)
            if jsonData.value(forKey: "errmsg") as! String == "" {
                let respDic = jsonData.value(forKey: "response") as! NSArray
                self.dictValue = respDic as! [NSDictionary]
                
                
                let desc = self.dictValue[0].value(forKey: "desc") as! String
                let pmsData = self.dictValue[0].value(forKey: "col1") as! String
                
                let final_data = "\(desc)<br>\(pmsData)" as? String
                
                
                DispatchQueue.main.async {
                    
                    if let benefits = final_data {
                        
                        do {
                            let benefitsAttibutedString    = try NSAttributedString(data: benefits.data(using: String.Encoding.utf8)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                            
                            self.txtDesc.attributedText = benefitsAttibutedString
                            self.wbWebView.loadHTMLString(benefits, baseURL: nil)
                            self.actInd.stopAnimating()
                            
                        } catch {
                            self.actInd.stopAnimating()
                            print(error)
                        }
                        
                    }
                    
                }
            }
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        wbWebView.frame.size.height = 1
        wbWebView.frame.size = wbWebView.sizeThatFits(.zero)
        wbWebView.scrollView.isScrollEnabled = false
        self.view.layoutIfNeeded()
    }
    
    @IBAction func onClick_Hamberger(_ sender: UIButton) {
        
        if !isShowingMenu {
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            btnHamberger.frame.origin.x += 250
            isShowingMenu = true
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            btnHamberger.frame.origin.x -= 250
            isShowingMenu = false
        }
        
    }
    
    @IBAction func btnContactUsClick(_ sender: Any) {
        
        let url = URL(string: "telprompt://8336087004")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
