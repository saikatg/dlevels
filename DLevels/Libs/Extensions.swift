//
//  Extensions.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 11/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit


extension UILabel{
    
    // Set lebel height according to text.
    
    func setHeight()-> CGFloat{
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = self.font
        label.text = self.text
        
        label.sizeToFit()
        
        return label.frame.height
    }

    
}

extension UIViewController {
    
    func hideKeyboardOnTapOutside() {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    // Hide Keyboard
    func dismissKeyboard(){
        view.endEditing(true)
    }
}

extension Int {
    
    func generateRandomDigits(_ digitNumber: Int) -> String {
        var number = ""
        for i in 0..<digitNumber {
            var randomNumber = arc4random_uniform(10)
            while randomNumber == 0 && i == 0 {
                randomNumber = arc4random_uniform(10)
            }
            number += "\(randomNumber)"
        }
        return number
    }
}

extension UIView{
    
    
    func addCornerRadius(value: CGFloat){
        
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.5
        self.layer.masksToBounds = true
        self.layer.cornerRadius = value
    }
    
    
    func addCornerRadiusWithColor(value: CGFloat, colorCode: String){
        
        self.layer.borderColor = UIColor.gray.cgColor //UIColor(hex: "\(colorCode)").cgColor
        self.layer.borderWidth = 0.5
        self.layer.masksToBounds = true
        self.layer.cornerRadius = value
        
        
        self.layer.cornerRadius = value;
        
        // A thin border.
        self.layer.borderColor = UIColor(hex: "\(colorCode)").cgColor
        self.layer.borderWidth = 0.5;
        
        // Drop shadow.
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 1.0;
        self.layer.shadowRadius = 2.0;
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
    }
    
    func addDropShadow(){
        
        self.layer.masksToBounds = false;
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowOpacity = 2.0;
    }
    
    
    func setCornerRadiusAndShadow(cornerRadiusValue: CGFloat) {
        // Rounded corners.
        //self.layer.cornerRadius = cornerRadiusValue;
        
        // A thin border.
          self.layer.borderColor = UIColor.gray.cgColor
          self.layer.borderWidth = 0.3;
        
        // Drop shadow.
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.5;
        self.layer.shadowRadius = 2.0;
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        
    }
    
}

//MARK: Hex Color Support
extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}


