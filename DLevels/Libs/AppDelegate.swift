//
//  AppDelegate.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 23/12/16.
//  Copyright © 2016 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import Firebase
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder,  UIApplicationDelegate{

    var window: UIWindow?
    var myInterstitial : GADInterstitial?
    
    var orientationLock = UIInterfaceOrientationMask.all
   
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UIApplication.shared.statusBarStyle = .lightContent
        
        var configError : NSError?
        GGLContext.sharedInstance().configureWithError(&configError)
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        assert(configError == nil, "Error configuring Google services: \(String(describing: configError))")
        
        // Fabric Crashlystics Implementation
        Fabric.with([Crashlytics.self])

        
        
        // Use Firebase library to configure APIs.
       FIRApp.configure()
        
        //FirebaseApp.configure()
        
        // Initialize the Google Mobile Ads SDK.
       //GADMobileAds.configure(withApplicationID: "ca-app-pub-4603335742863525/5299548097")
        
        // TODO: Move this to where you establish a user session
        self.logUser()

        
        return true
    }
    
    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail("anwesha@dlevels.com")
        Crashlytics.sharedInstance().setUserIdentifier("dynamic123")
        Crashlytics.sharedInstance().setUserName("DLevels")
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        FBSDKAppEvents.activateApp()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    /*
 
     func application(application: UIApplication,
     openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
     var options: [String: AnyObject] = [UIApplicationOpenURLOptionsSourceApplicationKey: sourceApplication,
     UIApplicationOpenURLOptionsAnnotationKey: annotation]
     return GIDSignIn.sharedInstance().handleURL(url,
     sourceApplication: sourceApplication,
     annotation: annotation)
     }
     
     return GIDSignIn.sharedInstance().handleURL(url,
     sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as? String,
     annotation: options[UIApplicationOpenURLOptionsAnnotationKey])
     }
 */
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        
        if(url.scheme!.isEqual("fb150907365424798```")) {
         //   return FBSDKApplicationDelegate.shared.application(app, open: url, options: options)
            
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
            
        } else {
            
    return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: [UIApplicationOpenURLOptionsKey.annotation])
        }
    }
    
    // MARK: - Google SignIn Methodes
    
    
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
     
       // myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
    //    self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
      //  self.dismissViewControllerAnimated(true, completion: nil)
    }


    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "DLevels")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        
    }
    
    
    internal var shouldRotate = false
    func application(_ application: UIApplication,
                     supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return orientationLock //shouldRotate ? .allButUpsideDown : .portrait
    }
    

}

