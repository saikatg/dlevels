//
//  WebService.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 12/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//
import UIKit
import Foundation

typealias complition_dictDAta = (_ data: NSDictionary) -> Void
typealias CompletionHandler = (_ success:Bool, _ dictData: NSDictionary) -> Void

class WebService: NSObject {
    
    public override init() {
        
    }
    //
    func callWebServices(url: String, methodName: String, parameters: String,istoken: Bool, tokenval: String, completion: @escaping CompletionHandler){
        
        var url_param = url
        
        if(methodName == "GET" && parameters != "")
        {
           url_param = url_param + "?" + parameters
        }
        
       // print(url_param)
        
        var request = URLRequest(url: URL(string: url_param)!)
        request.httpMethod = methodName
        
        if(methodName == "POST" && parameters != "")
        {
            let postString = parameters
            request.httpBody = postString.data(using: .utf8)
        }
        
        if(istoken)
        {
            request.setValue("Bearer " + tokenval , forHTTPHeaderField: "Authorization")
        }
 
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else {
                print("error=\(String(describing: error))")
                return
            }
            
            if let httpsStatus = response as? HTTPURLResponse, httpsStatus.statusCode != 200 {
                print("Status Code should be 200, but it is \(httpsStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            do {
                let dictData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSDictionary
                
                
                DispatchQueue.main.async {
                    completion(true, dictData)
                }
                
                
            } catch {
                print("error is : \(error)")
            }
        }
        task.resume()
    }
    
    
    
    func callWebServices2(urls: String, methodName: String, parameters: String,istoken: Bool, tokenval: String, completion: @escaping CompletionHandler){
        
        var url_param = urls
        
        if(methodName == "GET" && parameters != "")
        {
            url_param = url_param + "?" + parameters
        }
        
       // print(url_param)
        
        var request = URLRequest(url: URL(string: url_param)!)
        request.httpMethod = methodName
        
        if(methodName == "POST" && parameters != "")
        {
            let postString = parameters
            request.httpBody = postString.data(using: .utf8)
        }
        
        if(istoken)
        {
            request.setValue("Bearer " + tokenval , forHTTPHeaderField: "Authorization")
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else {
                print("error=\(String(describing: error))")
                return
            }
            
            if let httpsStatus = response as? HTTPURLResponse, httpsStatus.statusCode != 200 {
                print("Status Code should be 200, but it is \(httpsStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            do {
                let dictData = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSDictionary
                
                completion(true, dictData)
                
            } catch {
                print("error is : \(error)")
            }
        }
        task.resume()
    }
    
    func performLogin(email : String, password: String, regThrough: String) {
        
        let appManager = AppManager()
        let webService = WebService()
        
        let parameters = "user_email=\(email)&password=\(password)&logthrough=\(regThrough)"
        
        if appManager.isValidEmail(email: email) {
            
            webService.callWebServices(url: Urls.login, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
                
                print("Login Return Result: \(dictData)")
                
                User.email      = email //responce.value(forKey: "emailID") as! String
                User.password   = password //responce.value(forKey: "phone") as! String
            })
        }
    }

    
}
