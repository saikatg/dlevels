//
//  splashtblvwcell.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 28/12/16.
//  Copyright © 2016 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class splashtblvwcell: UITableViewCell {
    
    @IBOutlet weak var lblsymbol: UILabel!
    
    @IBOutlet weak var lblltp: UILabel!

    @IBOutlet weak var lbldiff: UILabel!
    
    @IBOutlet weak var imgvwindicator: UIImageView!
    
    @IBOutlet weak var lblpercent: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    
}
