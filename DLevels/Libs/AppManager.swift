//
//  AppManager.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 28/12/16.
//  Copyright © 2016 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class AppManager: NSObject {
    
    
    func shouldAutorotate() -> Bool {
        if (UIDevice.current.orientation == UIDeviceOrientation.portrait ||
            UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown ||
            UIDevice.current.orientation == UIDeviceOrientation.unknown) {
            return true;
        } else {
            return false;
        }
    }
    
    func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    func halfTextRedColorChange(fullText : String , changeText : String ) -> NSMutableAttributedString {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
        return attribute
    }
    
    func halfTextGreenColorChange(fullText : String , changeText : String ) -> NSMutableAttributedString {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
        return attribute
    }
    
    
    
    func setStatusBarBackgroundColor() {
        
        let color = UIColor.init(colorLiteralRed: 248/255, green: 200/255, blue: 74/255, alpha: 1)
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = color
    }
    
    
    
    // MARK: Validate Email
    func isValidEmail(email:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
        
    }
    
    
    // MARK: Validate Phone No
    func isValidPhoneNo(phoneNo: String) -> Bool {
        
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: phoneNo)
        return result
        
    }
    
    func showAlert(title: String, message: String, navigationController: UINavigationController) {
        
        
        DispatchQueue.main.async(execute: {
            
            let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default) { (alertAction) in
                
            }
            alertViewController.addAction(alertAction)
            navigationController.present(alertViewController, animated: true, completion: nil)
            
        })
    }
    
  
    
    // MARK: Dispath After
    class func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    /*
     delay(0.4) {
     // do stuff
     }
     */
    
    
    
    
    
    
  /*  func pushTo(viewController: String, navigationController: UINavigationController){
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController")
        //     self.navigationController?.pushViewController(viewController, animated: true)
        
    } */
    
}


struct AppUtility {
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        
        self.lockOrientation(orientation)
        
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
    }
    
}
