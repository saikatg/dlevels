//
//  Constraints.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 29/12/16.
//  Copyright © 2016 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static let someNotification = "TEST"
}


struct Urls {
    
    static let main_url                 =   "https://ws.dlevels.com/"
    
    static var login                    =   main_url + "login"
    static var signUp                   =   main_url + "register"
    
    static var logOut                   =   main_url + "logout"
    static var getUser                  =   main_url + "get-user"
    
    static var resendOtp                =   main_url + "generate-otp"
    static var accountStatus            =   main_url + "account-status"
    static var checkPhoneOtp            =   main_url + "check-phone-otp"
    static var forGotPassword           =   main_url + "forgot-password"
    static var changepassword           =   main_url + "change-password"
    
    static var generatePhoneOtp         =   main_url + "generate-phone-otp"
    static var generateEmailOtp         =   main_url + "generate-email-otp"
    static var updatePhoneNo            =   main_url + "update-phone"
    static var updateEmail              =   main_url + "update-email"
    static var dashboardTicker          =   main_url + "dashboard-ticker"
    static var onBoarding               =   main_url + "onboarding"
    static var multibaggerlist          =   main_url + "multibagger-list"
    static var datewise_Performence     =   main_url + "multibagger-daywise-performance"
    static var sectorwise_Performence   =   main_url + "multibagger-sector"
    static var sectorwise_Performence_Quarter   =   main_url + "multibagger-sector-quarterly"
    static var multibaggerStatus        =   main_url + "multibagger-stats"
    static var multibagger_call_history =   main_url + "multibagger-call-history"
    static var past_Performance         =   main_url + "multibagger-stats"
    static var sector_Wise_Symbol       =   main_url + "multibagger-sector-wise-symbol"
    static var sector_Wise_Symbol_Quarterly       =   main_url + "multibagger-sector-wise-symbol-quarterly"
    static var multibagger_gainers      =   main_url + "multibagger-gainers"
    static var wellcome_phone           =   main_url + "wellcome-phone"
    static var wellcome_email           =   main_url + "wellcome-email"
    static var chart_data               =   main_url + "chart-data"
    static var correction_report        =   main_url + "correction-report"
    static var live_price               =   main_url + "live-price"
    static var check_version                        =   main_url + "check-version"
    static var webinar_video                        =   main_url + "get-video"
    static var autosearch_stock                     =   main_url + "get-autosearch-stock"
    static var portfolio_checker                    =   main_url + "portfolio-checker"
    static var seminar_ad                           =   main_url + "seminar-ad"
    static var sector_performance_detail            =   main_url + "sector-performance-detail"
    static var country_code                         =   main_url + "country-code"
    static var login_without_emailverification      =   main_url + "login-without-email"
    
    
    static var indexstock               = main_url + "index-stock-v1"
    static var indexfactsheet           = main_url + "index-factsheet-v1"
    
    static var NRIFPI               = main_url + "index-nri-fpi"
    static var PMS                  = main_url + "index-pms-hnis"
    static var DPP                  = main_url + "index-dp-list"
    
    static var NRIFPIPMS            = main_url + "nri-fpi-pms"
    
    static var addUpdateFCMToken = main_url + "add-update-fcm-token"
    
    
}

struct CountryCode {
    
    static var countryCode   = ""
  
    
}

struct SearchForMultibaggerSector {
    
    static var serchValue   = ""
    static var seg_date     = ""
    
}
struct SearchForCurrentMultibagger {
    
    static var serchValue   = ""
    static var instrument_4     = ""
    static var viewcontrollername = ""

    
}
struct MultibaggerType {
    
    static var midCap                       =       "MID"
    static var largeCap                     =       "LARGE"
    
    
    static var midCapForHeatmap             =       "MLT-MID"
    static var largeCapForHeatmap           =       "MLT-LARGE"
    
    static var midCapForPastPerformance     =       "Mid%20Cap"
    static var LargeCapForPastPerformance   =       "Large%20Cap"
    
    
    static var typeOfMultibagger            =       ""
    static var typeOfMultibaggerForHeatMap  =       ""
    static var typeOfMultibaggerForPastPer  =       ""
}

struct ViewToHighLight {
    
    static var vwRiskView               =       ""
    static var vwInvestment             =       ""
    
}


struct FactSheet {
    static var characteristics = [String]()
}

struct MultibaggerStats {
    
    static var segQtrDateArr = [String]()
    static var segQtrDisplayArr = [String]()
    
    static var segYearDateArr = [String]()
    static var segYearDisplayArr = [String]()
    
    static var multibaggersmallcaplistdata = [NSDictionary]()
    static var multibaggermidcaplistdata = [NSDictionary]()
    static var multibaggerlargecaplistdata = [NSDictionary]()
    
    
    
    
    static var multibaggersmallcapgainerlooserdata = [NSDictionary]()
    static var multibaggersegmentdata = [NSDictionary]()
    static var multibaggerquarterlysegmentdata = [NSDictionary]()
    
    
    static var multibaggersmallcapyearwisedata = [NSDictionary]()
    
    
    static var multibaggerlargecapgainerlooserdata = [NSDictionary]()
    static var multibaggerlargecapyearwisedata = [NSDictionary]()
    
    static var correctionlevel = [String]()
    static var tblitems = [String]()
    
    static var qutrsmallcapindex = [String]()
    static var qutrmidcapindex = [String]()
    static var qutrniftyindex = [String]()
    
    
    static var newqutrsmallcapindex = [String]()
    static var newqutrmidcapindex = [String]()
    static var newqutrniftyindex = [String]()
    
    
    static var yearsmallcapindex = [String]()
    static var yearniftyindex = [String]()
    
    static var qutrperformancetxt = [String]()
    static var yearperformancetxt = [String]()
    
    
    
}


struct seminar_ad{
    
    static var url         =   ""
    static var templete        =   ""
    
}

struct Phone {
    
    static let iPhone_5s        =   568       //  UIScreen.main.bounds.size.height - 568
    static let iPhone_7s        =   667       // UIScreen.main.bounds.size.height-667
    static let iPhone_7s_Plus   =   736       // UIScreen.main.bounds.size.height-736
}

struct User {
    
    static var socialId         =   ""
    static var firstName        =   ""
    static var lastName         =   ""
    static var email            =   ""
    static var phone            =   ""
    static var socialToken      =   ""
    static var password         =   ""
    static var regThrough       =   ""
    static var token            =   ""
    static var OnboardinString  =   ""
    static var countryCode      =   ""    
    static var isLandingDataLaded = false
    
    static var isCommingFromSpecificPage = false
    static var instanceOfSectorPerformance = SectorPerformanceDetailsViewController()
    static var tempIndePath: IndexPath!
    
}


struct RegThrough {
    
    static var normal           =   "normal"
    static var gmail            =   "gmail"
    static var facebook         =   "facebook"
}

struct PMS_DATA {
    
    static var PMS1  =   ""
    static var PMS2  =   ""
    static var PMS3  =   ""
    
    
}

struct NRI_DATA {
    
    static var NRI1  =   ""
    static var NRI2  =   ""
    static var NRI3  =   ""
    
    
}

struct FPI_DATA {
    
    static var FPI1  =   ""
    static var FPI2  =   ""
    static var FPI3  =   ""
    
    
}
