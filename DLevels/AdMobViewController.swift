//
//  AdMobViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 19/05/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdMobViewController: UIViewController, GADInterstitialDelegate {
    
    var interstitial: GADInterstitial!
    var navController: UINavigationController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadAddMob(success: { (success) in
            if success {
                
            }
        })
        
    
    }
    
    //MARK: Adding AdMob
    func loadAddMob(success: (_ success:Bool) -> Void) {
        
        interstitial = GADInterstitial(adUnitID: "ca-mb-app-pub-8834194653550774/3987450882")
        interstitial.delegate = self
        let request = GADRequest()
        //   request.testDevices = [kGADSimulatorID]
        //        request.testDevices = @[ kGADSimulatorID ];
        interstitial.load(request)
        success(true)
    }
    
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        
        print("Testing Will PresentScreen")
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        
        print("Add id Received")
        self.interstitial.present(fromRootViewController: self)
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
        navController?.pushViewController(viewcontroller, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
