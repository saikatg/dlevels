//
//  AllMultibaggersViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 15/11/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class AllMultibaggersViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    var pageMenu : CAPSPageMenu!
    var controllerArray : [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addingAllView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addingAllView() {
        
        let smallcap = self.storyboard!.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
        smallcap.title = "Smallcap"
        controllerArray.append(smallcap)
        
        let midcap = self.storyboard!.instantiateViewController(withIdentifier: "MultibaggerMidCapViewController") as! MultibaggerMidCapViewController
        midcap.title = "Midcap"
        controllerArray.append(midcap)
        
        let largecap = self.storyboard!.instantiateViewController(withIdentifier: "MultibaggerLargeCapViewController") as! MultibaggerLargeCapViewController
        largecap.title = "How to get started"
        controllerArray.append(largecap)
        
        let qSectorPerformance = self.storyboard!.instantiateViewController(withIdentifier: "MultibaggerQuarterlySectorViewController") as! MultibaggerQuarterlySectorViewController
        qSectorPerformance.title = "Quarterly Sector Performance"
        controllerArray.append(qSectorPerformance)
        
        let ySectorPerformance = self.storyboard!.instantiateViewController(withIdentifier: "MultibaggerSectorViewController") as! MultibaggerSectorViewController
        ySectorPerformance.title = "Yearly Sector Performance"
        controllerArray.append(ySectorPerformance)
        
        
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: containerView.frame.width, height: containerView.frame.height), pageMenuOptions: nil)
        containerView.backgroundColor = UIColor.white
        containerView.addSubview(pageMenu!.view)
    }
    
    @IBAction func btnBack_Action(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
