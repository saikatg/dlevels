//
//  AllFPIViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 08/11/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class AllFPIViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnHamburger: UIButton!
    var pageMenu : CAPSPageMenu!
    var controllerArray : [UIViewController] = []
    
    var isShowingMenu = false
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nrifpipmsload(completion: {
            self.addingAllView()
        })
        // Do any additional setup after loading the view.
    }
    
    func nrifpipmsload(completion : @escaping ()->())
    {
        let obj = WebService()
        let data = "ALL"
        let param = "ptype=\(data)"
        
        obj.callWebServices(url: Urls.NRIFPIPMS, methodName: "GET", parameters: param, istoken: false, tokenval: "") { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                // print(jsonData)
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                
                for arr in tempArray
                {
                    
                    if(arr.value(forKey: "p_type") as! String == "PMS")
                    {
                        PMS_DATA.PMS1 = arr.value(forKey: "p_why") as! String
                        PMS_DATA.PMS2 = arr.value(forKey: "p_what") as! String
                        PMS_DATA.PMS3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "NRI")
                    {
                        NRI_DATA.NRI1 = arr.value(forKey: "p_what") as! String
                        NRI_DATA.NRI2 = arr.value(forKey: "p_why") as! String
                        NRI_DATA.NRI3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "FPI")
                    {
                        FPI_DATA.FPI1 = arr.value(forKey: "p_what") as! String
                        FPI_DATA.FPI2 = arr.value(forKey: "p_why") as! String
                        FPI_DATA.FPI3 = arr.value(forKey: "p_how") as! String
                    }
                }
                
                
            }
        }
        
        return completion()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func searchClick(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    
    func addingAllView() {
        
        let fpi1instance = self.storyboard!.instantiateViewController(withIdentifier: "FPI1ContainerViewController") as! FPI1ContainerViewController
        fpi1instance.title = "What are FPIs"
        controllerArray.append(fpi1instance)
        
        let fpi2instance = self.storyboard!.instantiateViewController(withIdentifier: "FPI2ContainerViewController") as! FPI2ContainerViewController
        fpi2instance.title = "Why Dynamic PMS"
        controllerArray.append(fpi2instance)
        
        let fpi3instance = self.storyboard!.instantiateViewController(withIdentifier: "FPI3ContainerViewController") as! FPI3ContainerViewController
        fpi3instance.title = "How to get started"
        controllerArray.append(fpi3instance)
        
        
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: containerView.frame.width, height: containerView.frame.height), pageMenuOptions: nil)
        containerView.backgroundColor = UIColor.white
        containerView.addSubview(pageMenu!.view)
    }
    
    @IBAction func btnBack_Action(_ sender: UIButton) {
        
        if !isShowingMenu {
            
            //addGestureRecogniser()
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x += 250
            isShowingMenu = true
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x -= 250
            isShowingMenu = false
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
