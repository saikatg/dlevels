//
//  ChangePasswordViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 20/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {
    
    var  emailAddr : String?
    
    
    
    @IBOutlet weak var txtnewpassword: UITextField!

    @IBOutlet weak var txtconfirmpassword: UITextField!
    
    @IBOutlet weak var txtotp: UITextField!
    
    @IBAction func btnsubmit(_ sender: Any) {
        
        let obj = WebService()
        
        let newpass = txtnewpassword.text
        let confirmpassword = txtconfirmpassword.text
        let otpval = txtotp.text
        if newpass == confirmpassword
        {
        obj.callWebServices(url: Urls.changepassword, methodName: "POST", parameters: "email=" + emailAddr! + "&password=" + newpass! + "&otp=" + otpval! , istoken:false, tokenval: "")
            {(returnValue, jsonData) in
            
            //print("hhjhjhjjh\(jsonData)")
            
            if(jsonData.value(forKey: "errmsg") as! String == "")
            {
                DispatchQueue.main.async(execute: {
                    
                    let chngpasssucessvwcontroller = self.storyboard?.instantiateViewController(withIdentifier: "passwordchanged") as! ChangePassSucessViewController
                    
                    //chngpassvwcontroller.emailAddr = self.txtemail.text
                    self.navigationController?.pushViewController(chngpasssucessvwcontroller, animated: true)
                })
            }
                
            else
            {
                let alertobj = AppManager()
                alertobj.showAlert(title: "Error", message: jsonData.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
            }
        
    }
        }
        else{
            let alertobj = AppManager()
            alertobj.showAlert(title: "Error", message: "Mismatch in new password and confirm password", navigationController: self.navigationController!)
        
        }
    }
    
        override func viewDidLoad() {
        super.viewDidLoad()
            txtnewpassword.delegate = self
            txtnewpassword.tag = 0 //Increment accordingly
            
            self.addDoneButtonOnKeyboard()
        
        
        

        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtotp {
            
            UIView.animate(withDuration: 0.2, animations: {
                self.view.frame.origin.y -= 200
            })
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtotp {
            UIView.animate(withDuration: 0.2, animations: {
                self.view.frame.origin.y += 200
            })
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickReturn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    //MARK: Add Done Button to MobileNo keyboard
    func addDoneButtonOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(ChangePasswordViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.txtotp.inputAccessoryView = doneToolbar
    }

    
    // Done Button Action to remove keyboard
    func doneButtonAction() {
        self.txtotp.resignFirstResponder()
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
