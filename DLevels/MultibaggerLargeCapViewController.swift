//
//  MultibaggerLargeCapViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 13/10/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import GoogleMobileAds

class MultibaggerLargeCapViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, GADInterstitialDelegate {

    @IBOutlet weak var lblReleasedMsg: UILabel!
    @IBOutlet weak var lblReleasedDate: UILabel!
    @IBOutlet weak var lblChngPerDate: UILabel!
    @IBOutlet weak var lblEpsDate: UILabel!
    @IBOutlet weak var lblPatDate: UILabel!
    @IBOutlet weak var lblInstitution: UILabel!
    @IBOutlet weak var lblPledge: UILabel!
    @IBOutlet weak var lblAvgVol: UILabel!
    
    @IBOutlet weak var lblAvgPerf: UILabel!
    @IBOutlet weak var lbltotalNoOfStocks: UILabel!
    
    
    @IBOutlet weak var accordianViewHeightConstant: NSLayoutConstraint!
    //@IBOutlet weak var accordingvw: UITextView!
    @IBOutlet weak var lbl_textmsg: UILabel!
    
    @IBOutlet weak var lastpartview: UIView!
    @IBOutlet weak var accordionvw: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var segmentVwSpace: UIView!
    
    @IBOutlet weak var btnChangeMonths: UIButton!
    @IBOutlet weak var tickerlistview: UITableView!
    @IBOutlet weak var bottomScrollView: UIScrollView!
    var  sortedBydata : NSArray = []
    
    var segmentindex = 0
    var interstitial: GADInterstitial! // Creating Add Mob
    
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var navigationView: UIView!
    var flag_sort:Int = 1
    var isAccordianTap = false
    
    @IBOutlet weak var img_sort_btn: UIButton!
    @IBOutlet weak var btnHamberger: UIButton!
    
    let names   = ["SmallCap List","MidCap List","LargeCap List","Quarterly Sector Performance","Yearly Sector Performance"]
    let hint    = ["SmallCap List","MidCap List","LargeCap List","Quarterly Sector Performance","Yearly Sector Performance"]
    
    
    var arrNewlyAdded =     [NSDictionary]()
    var arrRetained   =     [NSDictionary]()
    var arrRemoved    =     [NSDictionary]()
    
    var segmentarr = [String]()
    var arrRepType   =  [String]()
    
    var typeOfMutibagger = ""
    
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    var isShowingMenu = false
    
    var response = [NSDictionary]()
    var alldata = [NSDictionary]()
    var isTapped = false
    var tablevwbindarr = [NSDictionary]()
    var col = "Current Trailing PE"
    var highlightcolor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
    var index = 0
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var avg_perf = 0.00
    
    @IBOutlet weak var lbl_indecperf: UILabel!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnInfo.imageEdgeInsets = UIEdgeInsetsMake(0, self.btnInfo.frame.size.width - 20 , 0, 0)
        
        UIView.animate(withDuration: 2) {
            self.accordionvw.isHidden = true
            self.accordianViewHeightConstant.constant = 0
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = false // or false to disable rotation
        
        
        //flag_sort = 0
        prepareScrollView()
        self.segmenmtDataLoad()
        loadAllData()
        var index = ""
        
        lblHeader.text = "Multibaggers"
        
        if(MultibaggerStats.qutrniftyindex.count > 0)
        {
            self.lbltotalNoOfStocks.text = "Nifty Idx Perf :"
            index = MultibaggerStats.qutrniftyindex[0]
            self.lbl_indecperf.text = "\(MultibaggerStats.qutrniftyindex[0])%"
            
            if((index as NSString).floatValue > 0)
            {
                self.lbl_indecperf.textColor = UIColor(red: 29/255, green: 204/255, blue: 146/255, alpha: 1)
            }
            else
            {
                self.lbl_indecperf.textColor = UIColor.red
            }
            
        }
        else
        {
            self.lbltotalNoOfStocks.text = "Nifty Idx Perf :"
            index = ""
            self.lbl_indecperf.text = ""
        }
        
        if(MultibaggerStats.qutrperformancetxt.count > 0)
        {
            self.lbl_textmsg.text = "Sorted by chng % for \(MultibaggerStats.qutrperformancetxt[0])"
        }
        else
        {
            self.lbl_textmsg.text = "Sorted by chng % for"
        }
        
        
        
        tickerlistview.tableFooterView = UIView()
        
        appDelegate.shouldRotate = false // or false to disable rotation
        
        
        addLoader()
        actInd.startAnimating()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppManager().setStatusBarBackgroundColor()
        self.navigationController?.isNavigationBarHidden = true
        
        
        tickerlistview.estimatedRowHeight = 1000
        tickerlistview.rowHeight = UITableViewAutomaticDimension
        
        
        let range = Range(uncheckedBounds: (lower: 0, upper: self.tickerlistview.numberOfSections))
        self.tickerlistview.reloadSections(IndexSet(integersIn: range), with: .none)
        
    }
    
    //MARK: Adding AdMob
    func loadAddMob(success: (_ success:Bool) -> Void) {
        
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-4603335742863525/5299548097")
        interstitial.delegate = self
        let request = GADRequest()
        //   request.testDevices = [kGADSimulatorID]
        //        request.testDevices = @[ kGADSimulatorID ];
        interstitial.load(request)
        success(true)
    }
    
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        
        print("Testing Will PresentScreen")
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        
        print("Add id REceived")
        self.interstitial.present(fromRootViewController: self)
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        
        
    }
    
    func buttonViewLoad()
    {
        
        var originx = 0
        
        for (index, element) in arrRepType.enumerated() {
            
            // print(segmentVwSpace.frame.width);
            
            let button = UIButton(frame: CGRect(x:originx , y: Int(segmentVwSpace.frame.origin.x-8), width: (Int(segmentVwSpace.frame.width/4) - 4) , height: 40))
            button.backgroundColor = UIColor.white
            button.setTitle(element, for: .normal)
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpOutside)
            
            
            button.setTitleColor(UIColor(red: 103/255, green: 128/255, blue: 138/255, alpha: 1), for: .normal)
            button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 12)
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
            
            button.layer.borderColor = UIColor(red: 103/255, green: 128/255, blue: 138/255, alpha: 1).cgColor
            button.layer.borderWidth = 1.0
            button.tag = index
            
            if(index == 0)
            {
                button.backgroundColor = self.highlightcolor
            }
            
            
            
            segmentVwSpace.addSubview(button)
            originx  = originx + Int(button.frame.width )
            
            // print(button.frame.width)
        }
        
        
    }
    
    
    
    func buttonAction(sender: UIButton!) {
        
        var index = ""
        self.loadDataForMultibaggerList(dateval: segmentarr[sender.tag], arrindex: sender.tag)
        
        let msg = arrRepType[sender.tag] as String
        
        accordiondataload(myDateString: segmentarr[sender.tag], quarterdisplay: msg,index: sender.tag)
        
        index = MultibaggerStats.qutrniftyindex[sender.tag]
        self.lbltotalNoOfStocks.text = "Nifty Idx Perf : "
        self.lbl_indecperf.text = "\(MultibaggerStats.qutrniftyindex[sender.tag])%"
        
        if((index as NSString).floatValue > 0)
        {
            self.lbl_indecperf.textColor = UIColor(red: 29/255, green: 204/255, blue: 146/255, alpha: 1)
        }
        else
        {
            self.lbl_indecperf.textColor = UIColor.red
        }
        
        //var msgarr = msg.components(separatedBy: "-")
        self.lbl_textmsg.text = "Sorted by chng % for \(MultibaggerStats.qutrperformancetxt[sender.tag])"
        
        //print("ssssss \(msgarr[0])")
        self.tickerlistview.reloadData()
        //self.tickerlistview.reloadSections(sectionIndex, with:  .automatic)
        sender.backgroundColor = self.highlightcolor
        
        if self.segmentVwSpace != nil{
            
            for v in self.segmentVwSpace.subviews{
                if(v.tag != sender.tag)
                {
                    v.backgroundColor = UIColor.white
                }
            }
        }
    }
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    @IBAction func btnsort(_ sender: Any) {
        
        
        self.response = self.response.reversed()
        
        let sectionIndex = IndexSet(integer: 0)
        self.tickerlistview.reloadSections(sectionIndex, with: .top)
        self.view.layoutIfNeeded()
    }
    
    //MARK: 3 Months Button Action
    @IBAction func btnTableHeaderAction(_ sender: UIButton) {
        
        let actionSheeController = UIAlertController(title:"Data Fields", message: "", preferredStyle: .actionSheet)
        actionSheeController.view.tintColor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
        
        actionSheeController.addAction(UIAlertAction(title: "Current Trailing PE", style: .default, handler: { (success) in
            self.btnChangeMonths.setTitle("Trailing PE", for: .normal)
            self.col = "Current Trailing PE"
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tickerlistview.reloadSections(sectionIndex, with: .top)
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "EPS", style: .default, handler: { (success) in
            self.col = "EPS"
            self.btnChangeMonths.setTitle("EPS", for: .normal)
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tickerlistview.reloadSections(sectionIndex, with: .top)
                self.view.layoutIfNeeded()
            }
        }))
        
        
        
        actionSheeController.addAction(UIAlertAction(title: "PAT(in Cr)", style: .default, handler: { (success) in
            
            self.col = "PAT(in Cr)"
            self.btnChangeMonths.setTitle("PAT(in Cr)", for: .normal)
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tickerlistview.reloadSections(sectionIndex, with: .top)
                self.view.layoutIfNeeded()
            }
        }) )
        
        actionSheeController.addAction(UIAlertAction(title: "Institution", style: .default, handler: { (success) in
            
            self.col = "Institution"
            self.btnChangeMonths.setTitle("Institution", for: .normal)
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tickerlistview.reloadSections(sectionIndex, with: .top)
                self.view.layoutIfNeeded()
            }
        }) )
        
        actionSheeController.addAction(UIAlertAction(title: "Pledge", style: .default, handler: { (success) in
            
            self.col = "Pledge"
            self.btnChangeMonths.setTitle("Pledge", for: .normal)
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tickerlistview.reloadSections(sectionIndex, with: .top)
                self.view.layoutIfNeeded()
            }
        }) )
        
        
        actionSheeController.addAction(UIAlertAction(title: "Avg Value(in Cr)", style: .default, handler: { (success) in
            self.col = "Avg Value(in Cr)"
            self.btnChangeMonths.setTitle("Avg Value", for: .normal)
            UIView.animate(withDuration: 0.4) {
                let sectionIndex = IndexSet(integer: 0)
                self.tickerlistview.reloadSections(sectionIndex, with: .top)
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        
        // mone rakho
        self.present(actionSheeController, animated: false) {
            
        }
        
    }
    
    
    
    
    func removeDuplicates(array: [String]) -> [String] {
        var encountered = Set<String>()
        var result: [String] = []
        for value in array {
            if encountered.contains(value) {
                // Do not add a duplicate element.
            }
            else {
                // Add value to the set.
                encountered.insert(value)
                // ... Append the value.
                result.append(value)
            }
        }
        return result
    }
    
    
    func convertDateFormate(date : Date, dateformat: String) -> String{
        
        // Day
        let calendar = Calendar.current
        let anchorComponents = calendar.dateComponents([.day, .month, .year], from: date)
        
        // Formate
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = dateformat
        let newDate = dateFormate.string(from: date)
        
        var day  = "\(anchorComponents.day!)"
        switch (day) {
        case "1" , "21" , "31":
            day.append("st")
        case "2" , "22":
            day.append("nd")
        case "3" ,"23":
            day.append("rd")
        default:
            day.append("th")
        }
        return day + " " + newDate
    }
    
    
    func accordiondataload(myDateString : String, quarterdisplay :String, index: Int)
    {
        //var myDateString = segmentarr[0]
        
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var myDate = dateFormatter.date(from: myDateString)!
        
        let releasedate = convertDateFormate(date: myDate, dateformat: "MMM")
        
        
        myDate = dateFormatter.date(from: myDateString)!
        //let avgvoldt = convertDateFormate(date: myDate, dateformat: "MMM yy")
        
        
        
        let prevdate = Calendar.current.date(byAdding: .month, value: -3, to: myDate)
        //print(prevdate)
        
        
        
        
        dateFormatter.dateFormat = "MMM yy"
        let newdate = dateFormatter.string(from: prevdate!)
        
        //        print(releasedate)
        //        print(newdate)
        //        print(avgvoldt)
        
        lblReleasedDate.text = "\(quarterdisplay) : "
        lblReleasedMsg.text = "The list was released on \(releasedate)."
        lblChngPerDate.text = "The price performance for \(MultibaggerStats.qutrperformancetxt[index]) in %."
        
        lblEpsDate.text     = "As Per quarter ended \(newdate)."
        lblPatDate.text     = "As Per quarter ended \(newdate)."
        lblInstitution.text = "As Per quarter ended \(newdate)."
        lblPledge.text      = "As Per quarter ended \(newdate)."
        lblAvgVol.text      = "Average daily value for last one year."
        
    }
    
    
    
    
    func segmenmtDataLoad()
    {
        
        segmentarr = MultibaggerStats.segQtrDateArr
        arrRepType = MultibaggerStats.segQtrDisplayArr
        
        self.buttonViewLoad()
        
        //print(segmentarr)
        
        accordiondataload(myDateString: segmentarr[0], quarterdisplay:  arrRepType[0],index: 0)
        
        
        
    }
    
    
    //MARK: Creating Segment View
    func segmentViewLoad()
    {
        
        //print("segment---------\(arrRepType)")
        
        let segmentedControl = UISegmentedControl(items: arrRepType)
        
        segmentedControl.frame = CGRect(x:0 , y: bottomScrollView.frame.origin.x + 10, width: UIScreen.main.bounds.width - 40 , height: 30)
        segmentedControl.addTarget(self, action: #selector(MultibaggerViewController.segmentedControlValueChanged(segment:)), for: .valueChanged)
        
        let font = UIFont.systemFont(ofSize: 12)
        segmentedControl.setTitleTextAttributes([NSFontAttributeName: font],
                                                for: .normal)
        
        // segmentedControl.tintColor   = UIColor(red: 247/255, green: 197/255, blue: 58/255, alpha: 1)
        
        segmentedControl.tintColor   =  UIColor(red: 247/255, green: 197/255, blue: 58/255, alpha: 1)
        //segmentedControl.tintColor   =  UIColor.black
        segmentedControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(red: 40/255, green: 76/255, blue: 90/255, alpha: 1)], for: .selected)
        
        
        
        segmentedControl.backgroundColor = UIColor.white
        segmentedControl.selectedSegmentIndex = 0
        
        
        
        
        segmentVwSpace.addSubview(segmentedControl)
        
        
    }
    
    
    
    
    /******************/
    
    
    func loadAllData()
    {
        //segmenmtDataLoad()
        
        var arrdata = [NSDictionary]()
        arrdata = MultibaggerStats.multibaggerlargecaplistdata
        
        self.actInd.startAnimating()
        
        
        if(arrdata.count == 0)
        {
            let segmentval = "MLT-LARGE"
            let typeval = "QUARTER"
            
            let obj = WebService()
            let paremeters = "segment=\(segmentval)&type=\(typeval)&date=1900-01-01"
            
            obj.callWebServices(url: Urls.multibaggerlist, methodName: "POST", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
                
                //print("Json Data is :  \(jsonData)")
                
                if jsonData.value(forKey: "errmsg") as! String == "" {
                    
                    let tempArray = jsonData.value(forKey: "response") as! NSArray
                    
                    self.alldata.removeAll()
                    
                    for dict in tempArray {
                        let dictValue = dict as! NSDictionary
                        self.alldata.append(dictValue)
                    }
                    
                    DispatchQueue.main.async(execute: {
                        Thread.current.cancel()
                        MultibaggerStats.multibaggerlargecaplistdata = self.alldata
                        self.loadDataForMultibaggerList(dateval: self.segmentarr[0], arrindex: 0)
                        
                        //MultibaggerStats.niftyindex[sender.tag]
                        self.actInd.stopAnimating()
                        
                    })
                }
                else
                {
                    self.actInd.stopAnimating()
                    let alertobj = AppManager()
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
                
            }
            
        }
        else
        {
            self.alldata = MultibaggerStats.multibaggerlargecaplistdata
            
            
            
            DispatchQueue.main.async(execute: {
                Thread.current.cancel()
                self.loadDataForMultibaggerList(dateval: self.segmentarr[0], arrindex: 0)
                
                
                
                self.actInd.stopAnimating()
                
            })
        }
        
        
    }
    
    
    
    
    
    
    //MARK: Getting data for Mulibagger/ TableView List
    func loadDataForMultibaggerList(dateval: String, arrindex: Int)
    {
        
        var datevalue = ""
        var avgpreftext = ""
        var datearr =  [String]()
        let datedisplay = [String]()
        
        self.actInd.startAnimating()
        self.response.removeAll()
        var avg = ""
        var annualavg = ""
        for arr in alldata {
            
            if (arr.value(forKey: "mrd_Date") as! String) == dateval  {
                self.response.append(arr)
            }
        }
        
        
        //print(response)
        
        
        var performance: Double = 0.00
        
        let tempArray = response as NSArray
        
//        for dict in tempArray {
//            let dictValue = dict as! NSDictionary
//
//            performance += Double(dictValue.value(forKey: "mrd_Performance") as! String)!
//
//        }
        
        performance = Double(MultibaggerStats.newqutrniftyindex[arrindex])!
        
        
        //        annualavg = String(format: "%.2f", (performance)/Double(response.count) * 4)
        //        avg = String(format: "%.2f", (performance)/Double(response.count))
        
        annualavg = String(format: "%.2f", (performance) * 4)
        avg = String(format: "%.2f", performance)
        
        
        if(tempArray.count > 0)
        {
            //          _al = tempArray[0] as! NSDictionary
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yy"
            let result = formatter.string(from: date)
            
            datearr = dateval.components(separatedBy: "-")
            datevalue = String(datearr[0].characters.suffix(2))
            // print(datevalue)
            // print(result)
            
            
            // print(datedisplay)
            
            
            
            avgpreftext = "Average Quarterly return \(MultibaggerStats.qutrperformancetxt[arrindex])"
            
            
            
        }
        
        //_ = avg_perf / Double(self.response.count)sdasd
        
        
        DispatchQueue.main.async(execute: {
            
//            if(arrindex > 0)
//            {
//
//                //Thread.current.cancel()
//                self.lblAvgPerf.text = "\(avgpreftext) : \(avg)% (\(annualavg)% p.a.) of \(self.response.count) stocks"
//            }
//            else
//            {
//                self.lblAvgPerf.text = "\(avgpreftext) : \(avg)% of \(self.response.count) stocks"
//            }
            // self.lbltotalNoOfStocks.text = "\(smallcaplbl)"
            
            self.lblAvgPerf.text = ""
            
            let attrs1 = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 12), NSForegroundColorAttributeName : UIColor(red: 29/255, green: 204/255, blue: 146/255, alpha: 1)]
            
            let attrs2 = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 12), NSForegroundColorAttributeName : UIColor.red]
            
            avgpreftext = "\(avgpreftext) : "
            avg = "\(avg)%"
            annualavg = "\(annualavg)% "
            
            // check
            
            if(arrindex > 0)
            {
                let app_manager = AppManager()
                if(performance > 0)
                {
                    let attributedString1 = NSMutableAttributedString(string:avg, attributes:attrs1)
                    let attributedString2 = NSMutableAttributedString(string:annualavg, attributes:attrs1)
                    
                    let data = NSMutableAttributedString(string: avgpreftext)
                    data.append(attributedString1)
                    data.append(NSMutableAttributedString(string: " ("))
                    data.append(attributedString2)
                    data.append(NSMutableAttributedString(string: "p.a.)"))
                    data.append(NSMutableAttributedString(string: " of \(self.response.count) Stocks"))
                    
                    
                    self.lblAvgPerf.attributedText = data
                    
                }
                
                if(performance < 0)
                {
                    let attributedString1 = NSMutableAttributedString(string:avg, attributes:attrs2)
                    let attributedString2 = NSMutableAttributedString(string:annualavg, attributes:attrs2)
                    
                    
                    let data = NSMutableAttributedString(string: avgpreftext)
                    data.append(attributedString1)
                    data.append(NSMutableAttributedString(string: " ("))
                    data.append(attributedString2)
                    data.append(NSMutableAttributedString(string: "p.a.)"))
                    data.append(NSMutableAttributedString(string: " of \(self.response.count) Stocks"))
                    
                    
                    self.lblAvgPerf.attributedText = data
                }
                
                
            }
            else
            {
                let attributedString1 = NSMutableAttributedString(string:avg, attributes:attrs1)
                let attributedString2 = NSMutableAttributedString(string:annualavg, attributes:attrs1)
                
                let data = NSMutableAttributedString(string: avgpreftext)
                data.append(attributedString1)
                
                
                
                self.lblAvgPerf.attributedText = data
                
                
            }
            
            
            
            
            
            self.tickerlistview.reloadSections(IndexSet(integer: 0), with: .automatic)
            self.actInd.stopAnimating()
            
        })
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        //         var val = 0.0
        let cell = tableView.dequeueReusableCell(withIdentifier:"tickercell", for: indexPath)
        cell.selectionStyle = .none
        
        cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
        
        let lblMultibagerName = cell.viewWithTag(1000) as! UILabel
        // lblMultibagerName.text = self.response[indexPath.row].value(forKey: "mrd_Instrument_4") as? String
        
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        let underlineAttributedString = NSAttributedString(string: (self.response[indexPath.row].value(forKey: "mrd_Instrument_4") as? String)!, attributes: underlineAttribute)
        lblMultibagerName.attributedText = underlineAttributedString
        
        
        let lblLTP = cell.viewWithTag(1001) as! UILabel
        lblLTP.text =  self.response[indexPath.row].value(forKey: "LTP") as? String
        
        
        let lblSector = cell.viewWithTag(1004) as! UILabel
        lblSector.text = self.response[indexPath.row].value(forKey: "mrd_Sector") as? String
        
        let lblChange = cell.viewWithTag(1003) as! UILabel
        
        if self.col == "Current Trailing PE" {
            lblChange.text =  self.response[indexPath.row].value(forKey: "mrd_PE") as? String
        } else if self.col == "EPS" {
            lblChange.text =  self.response[indexPath.row].value(forKey: "mrd_EPS") as? String
        }  else if self.col == "PAT(in Cr)" {
            lblChange.text =  self.response[indexPath.row].value(forKey: "mrd_PAT") as? String
        }else if self.col == "Institution" {
            lblChange.text =  self.response[indexPath.row].value(forKey: "mrd_Institution") as? String
        }else if self.col == "Pledge" {
            lblChange.text =  self.response[indexPath.row].value(forKey: "mrd_Pledge") as? String
        }else if self.col == "Avg Value(in Cr)" {
            lblChange.text =  self.response[indexPath.row].value(forKey: "mrd_Value") as? String
        }
        
        let lblPERatio = cell.viewWithTag(1002) as! UILabel // mrd_PERatio
        lblPERatio.text =  self.response[indexPath.row].value(forKey: "mrd_Performance") as? String
        
        return cell
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return response.count
        
        if response.count > 0 {
            return response.count
        }
        
        return 0
    }
    
    
    func addDashedBorder() {
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let color = UIColor.red.cgColor
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let shapeRect = CGRect(x: 0, y: 0, width: screenWidth, height: 1)
        
        let ypos = bottomScrollView.bounds.origin.y.adding(10)
        
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: bottomScrollView.bounds.origin.x, y: ypos)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.view.layer.addSublayer(shapeLayer)
    }
    
    //MARK: Prepare Scroll View
    private func prepareScrollView() {
        
        bottomScrollView.showsHorizontalScrollIndicator = false
        var fromLeft: CGFloat = 10
        
        for index in 0..<5 {
            
            let button = UIButton()
            button.contentMode       = .scaleAspectFill
            button.setTitle(names[index], for: .normal)
            button.setTitleColor(UIColor(red: 40/255, green: 76/255, blue: 90/255, alpha: 1), for: .normal)
            button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 12)
            
            button.accessibilityHint = names[index]
            
            button.titleLabel?.lineBreakMode = .byClipping
            button.frame = CGRect(x: fromLeft, y: 0, width: 175, height: 35)
            button.contentHorizontalAlignment = .center
            button.layer.borderColor = UIColor.blue.cgColor
            
            fromLeft +=  175
            button.addTarget(self, action: #selector(self.buttonTappedInScrollView(sender:)), for: .touchUpInside)
            //button.backgroundColor = UIColor.yellow
            
            button.tag = index
            bottomScrollView.addSubview(button)
            
            let highlightvw = UIView(frame: CGRect(x: button.frame.origin.x, y: button.frame.origin.y + button.frame.height, width: 175, height: 2.5))
            
            highlightvw.tag = index
            
            //   print("--vvvvvv----\(index)")
            if(index == 2)
            {
                highlightvw.backgroundColor = highlightcolor
                button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 13)
            }
            else
            {
                highlightvw.backgroundColor = UIColor.white
                button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 11)
            }
            
            bottomScrollView.addSubview(highlightvw)
            bottomScrollView.contentSize = CGSize(width: 900, height: 1)
            
        }
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.bottomScrollView.contentOffset.x = CGFloat(175*1)
            }, completion: nil)
        }
        
        self.view.layoutIfNeeded()
    }
    
    
    //MARK: Scroll View Tap Action
    func buttonTappedInScrollView(sender: UIButton){
        
        
        if let page = sender.accessibilityHint {
            
            if page == "SmallCap List" {
                
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
                
                
            }
            if page == "MidCap List" {
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerMidCapViewController") as! MultibaggerMidCapViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
                
            }
            if page == "LargeCap List" {
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerLargeCapViewController") as! MultibaggerLargeCapViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
                
            }
            if page == "Yearly Sector Performance" || page == "YEARLY SECTOR PERFORMANCE" {
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerSectorViewController") as! MultibaggerSectorViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
            }
            
            if page == "Quarterly Sector Performance" || page == "QUARTERLY SECTOR PERFORMANCE" {
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerQuarterlySectorViewController") as! MultibaggerQuarterlySectorViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
            }
        }
    }
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        SearchForCurrentMultibagger.serchValue = response[indexPath.row].value(forKey: "mrd_Security") as! String
        _  =  SearchForCurrentMultibagger.serchValue
        SearchForCurrentMultibagger.instrument_4 = response[indexPath.row].value(forKey: "mrd_Instrument_4") as! String
        SearchForCurrentMultibagger.viewcontrollername = "MultibaggerList"
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "StockSpecificNewViewController") as! StockSpecificNewViewController
        self.navigationController?.pushViewController(viewController, animated: false)
        
    }
    
    
    
    
    
    @IBAction func btn_info(_ sender: Any) {
        
        if !isAccordianTap{
            isAccordianTap = true
            
            UIView.animate(withDuration: 1, animations: {
                self.btnInfo.transform = CGAffineTransform(scaleX: 1, y: -1);
            })
            
            UIView.animate(withDuration: 2, animations: {
                
                self.accordianViewHeightConstant.constant += 270
                self.accordionvw.isHidden = false
                
            })
            
        } else {
            isAccordianTap = false
            
            UIView.animate(withDuration: 1, animations: {
                self.btnInfo.transform = CGAffineTransform(scaleX: 1, y: 1);
            })
            UIView.animate(withDuration: 2) {
                
                self.accordianViewHeightConstant.constant -= 270
                self.accordionvw.isHidden = true
            }
        }
        
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    @IBAction func onClick_Hamberger(_ sender: UIButton) {
        
        if !isShowingMenu {
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            btnHamberger.frame.origin.x += 250
            isShowingMenu = true
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            btnHamberger.frame.origin.x -= 250
            isShowingMenu = false
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}
