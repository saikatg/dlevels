//
//  OnboardingInvestmentViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 30/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class OnboardingInvestmentViewController: UIViewController {
    
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var vwBelow1Lakh: UIView!
    @IBOutlet weak var vwBetween1to3Lakh: UIView!
    @IBOutlet weak var vw3LakhAndAbove: UIView!
    
    var onBoardingText = ",3|"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        btnDone.isEnabled = true
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        
        if ViewToHighLight.vwInvestment == "vwBelow1Lakh" {
            
            self.vw3LakhAndAbove.backgroundColor        =   UIColor.white
            self.vwBelow1Lakh.backgroundColor           =   UIColor.white
            self.vwBetween1to3Lakh.backgroundColor      =   UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
        
        } else if ViewToHighLight.vwInvestment == "vwBetween1to3Lakh" {
            self.vw3LakhAndAbove.backgroundColor        =   UIColor.white
            self.vwBelow1Lakh.backgroundColor           =   UIColor.white
            self.vwBetween1to3Lakh.backgroundColor      =   UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
            
        } else if ViewToHighLight.vwInvestment == "vw3LakhAndAbove" {
            
            self.vw3LakhAndAbove.backgroundColor        =   UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
            self.vwBetween1to3Lakh.backgroundColor      =   UIColor.white
            self.vwBelow1Lakh.backgroundColor           =   UIColor.white
            
        } else {
            
            self.vwBetween1to3Lakh.backgroundColor      =   UIColor.white
            self.vwBelow1Lakh.backgroundColor           =   UIColor.white
            self.vw3LakhAndAbove.backgroundColor        =   UIColor.white
        }
    }
    
    @IBAction func onclickBtnBelowOneLakh(_ sender: UIButton) {
        
        onBoardingText                              = ",3|<3"
        self.vw3LakhAndAbove.backgroundColor        =   UIColor.white
        self.vwBetween1to3Lakh.backgroundColor      =   UIColor.white
        self.vwBelow1Lakh.backgroundColor           =   UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
        ViewToHighLight.vwInvestment  = "vwBelow1Lakh"
        
    }
    
    @IBAction func onClickBtn1To3(_ sender: UIButton) {
        
        onBoardingText                           = ",3|1-3"
        self.vw3LakhAndAbove.backgroundColor        =   UIColor.white
        self.vwBelow1Lakh.backgroundColor          =   UIColor.white
        self.vwBetween1to3Lakh.backgroundColor           =   UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
        ViewToHighLight.vwInvestment  = "vwBetween1to3Lakh"
        
    }
    
    @IBAction func onClick3LakhAbove(_ sender: UIButton) {
        
        onBoardingText                               = ",3|3<"
        self.vw3LakhAndAbove.backgroundColor            =   UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
        self.vwBetween1to3Lakh.backgroundColor          =   UIColor.white
        self.vwBelow1Lakh.backgroundColor              =   UIColor.white
        ViewToHighLight.vwInvestment                  =   "vw3LakhAndAbove"
        
    }
    
    func pushViewToNextViewController() {
        
        let webServiceCall = WebService()
        let appManager     = AppManager()
        
        let parameter   =     "values=\(User.OnboardinString)"
        print("User token is:\(User.token)")
        
        webServiceCall.callWebServices(url: Urls.onBoarding, methodName: "POST", parameters: parameter, istoken: true, tokenval: User.token) { (success, jsonData) in
            
            print(jsonData)
            if jsonData.value(forKey: "response") as! Bool {
                
                print("User Onboarding Data is \(User.OnboardinString)")
                DispatchQueue.main.async {
                    self.delay(1, closure: {
                        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
                        self.navigationController?.pushViewController(viewController, animated: true)
                    })
                }
                
            }else {
                
                DispatchQueue.main.async {
                    
                    self.btnDone.isEnabled = true
                    if let msg = jsonData.value(forKey: "error") {
                        appManager.showAlert(title: "Error", message: "\(msg)", navigationController: self.navigationController!)
                    }
                    
                }
            }
        }
        
    }
    
    @IBAction func onClickBackButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Done Button Click Action
    @IBAction func onClickDoneButton(_ sender: UIButton) {
        
        btnDone.isEnabled = false
        
        User.OnboardinString    = "\(User.OnboardinString)\(onBoardingText)"
        pushViewToNextViewController()
        
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    
}
