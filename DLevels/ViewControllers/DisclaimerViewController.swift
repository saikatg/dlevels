//
//  DisclaimerViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 11/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class DisclaimerViewController: UIViewController {
    
    @IBOutlet weak var btnHamburger: UIButton!
    
   
    @IBOutlet weak var lblDisclaimer: UILabel!
    
    var isShowingMenu = false
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var disclosureText = ""
        let att = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 14)]
        let boldText = NSMutableAttributedString(string:disclosureText, attributes:att)
       
       // UIView.animate(withDuration: 0.2) {
            
            disclosureText =       
                "Privacy Policy: DLEVELS respects the privacy rights of its users and is strongly committed to protecting their privacy. It collects personal information that you voluntarily provide when subscribing to it and may include your name, address, e-mail address, etc. Consistent with the choice you make when providing personal information, Dynamiclevels may use your personal information to provide you with information on updates and modifications to the application via various means including e-mail. You may instruct us at any time not to use your personal information covered by this Privacy Policy to provide you with information on updates and modifications by contacting the administrators. Please note that if you exercise your choice in this manner, you will continue to receive communications regarding password resets and in response to any other requests for information by you. Please also note that this opt-out process may take some time to complete, consistent with relevant law and operational delays, if any.\n " +
            "The contents of the application may not be made available outside of the private sphere of the end user and/or copied for a purpose other than for private use. Downloading and storage on any computer, disk or other device designed to reproduce the matter contained here would also be considered as copying.\n " +
            "\n " +
            "Collection and Use of Personal Information: Dynamiclevels.com may place, view, and/or use “cookies”, web server logs, web beacons, or other electronic tools to collect statistical and other information about your use of this application. This information may include information about the IP address of your computer, browser type, language, operating system, your mobile device, geo-location data, the state or country from which you accessed this application, the web pages visited and the date and the time of a visit.\n " +
           
            "Such information may be collected for various reasons and/or for own purposes including research, analysis and to better serve visitors to this application. However, if you have deleted and disabled cookies, these uses will not be possible to the extent they are based on cookie information.\n " +
            "\n " +
            "Any personal information you provide to dynamiclevels.com is kept on secure servers. The application uses reasonable administrative, technical, personnel, and physical measures \n (a) to safeguard personal information against loss, theft, unauthorized use, disclosure, or modification; and \n (b) to ensure the integrity of the personal information. To help us protect your privacy, you should maintain the secrecy of the login ID and password you may have set up in connection with your participation in this Web site’s service.\n "
        
        
            
           /* let disclaimerText = "By using this service, the user agrees to indemnify, defend and hold harmless the owner, its suppliers, agents, directors, officers, employees, representatives, successors and assignees from and against any and all claims, damages, loss, liabilities, costs and expenses, including legal fees, arising out of or in connection with the service by the user. \n" +
                "The content or information contained or any related service, is provided on an “as is” basis and without warranties of any kind, express or implied, as to the service / data, hence the data / price/ charts/ buy or sell indicatives, provided here may not be accurate, meaning they may be indicative and not appropriate. Therefore Dynamic Levels or its owners do not bear any responsibility if there is a trading loss by using the data / price / service.\n" +
                
                "Access to and use of this information therein is at the user’s risk and the owner does not undertake any accountability for any irregularities, viruses or damage to any handheld device that results from accessing, availing or downloading of any information. This service does not constitute an offer of sale or a solicitation to make investment or trading to any person in any jurisdiction.\n" +
                
                "All intellectual property rights are owned by Dynamic Equities Pvt. Ltd. or its owners, absolutely. No part shall be reproduced, redistributed, commercially exploited, stored in retrieval system, or transmitted in any form or by any means – electronic, electrostatic, magnetic tape, mechanical, printing, photocopying, recording, or otherwise including the right of translation in any language, without the express permission of the owner. The contents herein may be used only for personal and non-commercial use. The owner retains the complete right to bar any person from using the service, at its discretion, without any prior notice.\n" +
                
                "Any dispute arising between the registered user and the owner shall be subject to the jurisdiction of the relevant authority and court of law situated in Kolkata and governed by the laws of India.\n" +
                
            "Hence NRI’S or Foreign Nationals users are recommended to check their eligibility after verification as per the laws of their land and for which Dynamic Equities Pvt. Ltd or the owners shall not be held liable.\n" */
            
            let normalDisclaimer = NSMutableAttributedString(string: disclosureText)
            boldText.append(normalDisclaimer)
            
            self.lblDisclaimer.attributedText = boldText
            
            
     //   }
        
        view.layoutIfNeeded()
        
        
    }
    
    @IBAction func searchClick(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    
    
    @IBAction func backButton_Action(_ sender: UIButton) {
        
        if !isShowingMenu {
            
            //addGestureRecogniser()
            
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x += 250
            isShowingMenu = true
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x -= 250
            isShowingMenu = false
        }
        
    }
    
    // MARK: Adding Gesture Recogniser For Hiding Menu on tap any where
    func addGestureRecogniser(){
        //let  tapGesture = UITapGestureRecognizer(target: self, action: #selector(MultibaggerLandingViewController.tapAction(sender:)))
        // self.view.addGestureRecognizer(tapGesture)
        
        let  swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(MultibaggerLandingViewController.tapAction(sender:)))
        swipeGesture.direction = .left
        self.view.addGestureRecognizer(swipeGesture)
    }
    
    //MARK: GESTURE RECONGISER METHODE
    func tapAction(sender: UITapGestureRecognizer) {
        
        instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
        btnHamburger.frame.origin.x -= 250
        isShowingMenu = false
    }

    
    
}


