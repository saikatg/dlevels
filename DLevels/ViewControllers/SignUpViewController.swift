//
//  SignUpViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 27/12/16.
//  Copyright © 2016 Dynamic-Mac-02. All rights reserved.
//

import Foundation
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit



class SignUpViewController: UIViewController, UITextFieldDelegate, GIDSignInDelegate, GIDSignInUIDelegate, MyProtocol {
    
    @IBOutlet weak var txtcountrycode: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    
    @IBOutlet weak var btnSignUP: UIButton!
    
   // @IBOutlet weak var signUpButtonHeightConstraints: NSLayoutConstraint!
    var instanceOfLeftSlideMenu = SectorPopup()
    var isShowingMenu = false
    var countrycode = ""
    
    var valueSentFromSecondViewController:String? = ""
    

    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareScreen()
        if(CountryCode.countryCode != ""){
            self.txtcountrycode.text = "+\(CountryCode.countryCode)"}
        else{
            self.txtcountrycode.text = "+91"
        }
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        // GIDSignIn.sharedInstance().signInSilently()
        
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       
        if(CountryCode.countryCode != ""){
        self.txtcountrycode.text = "+\(CountryCode.countryCode)"}
        else{
            self.txtcountrycode.text = "+91"
        }
        actInd.stopAnimating()
        
        self.view.isUserInteractionEnabled = true
        
        dismissKeyboard()
        
       
        
    }
    
    //4. Implement MyProtocol's function to make FirstViewContoller conform to MyProtocol
    
    // MARK: MyProtocol functions
    func setResultOfBusinessLogic(valueSent: String) {
        self.txtcountrycode.text = valueSent
    }

    
  
    
    private func prepareScreen(){
        
        if UIScreen.main.bounds.height > 568 {
            //signUpButtonHeightConstraints.constant = 30
            
        }
        
        self.addDoneButtonOnKeyboard()
        self.hideKeyboardOnTapOutside()
        AppManager().setStatusBarBackgroundColor()
    }
    
    
    @IBAction func signInSctreen_Action(_ sender: UIButton) {
        
        //self.navigationController?.popViewController(animated: true)
        
        let LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(LoginViewController, animated: true)
        
    }
    
    
    @IBAction func tempLogOut_Action(_ sender: UIButton) {
        
        GIDSignIn.sharedInstance().signOut()
        
        let manager = FBSDKLoginManager()
        manager.logOut()
    }
    
    
    // Mark : Google Integration Delegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error == nil {
            
            // ***
            User.firstName      =   user.profile.givenName
            User.lastName       =   user.profile.familyName
            User.email         =   user.profile.email
            User.socialId       =   user.userID
            User.password       =   user.authentication.idToken
            print(" access token is : \(user.authentication.idToken)")
            User.regThrough     = RegThrough.gmail
            // var idToken  = GIDGoogleUser.getauto
            print("Acess token is:  \(user.authentication.accessToken)")
            print(" User Id:  \(user.userID)")
            
            User.socialToken    =   user.authentication.idToken
            
            isEmailExist(email: User.email, regthrough: User.regThrough)
           
         
            
        } else{
            print(error.localizedDescription)
        }
    }
    
    // Mark : Google Integration Delegate
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        
      //   actInd.startAnimating()
       self.dismiss(animated: true, completion: nil)
        
    //    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerLandingViewController") as! MultibaggerLandingViewController
     //   self.navigationController?.present(viewController, animated: false, completion: nil)
        
        
        
        
        
        
    }
    
    
    // Mark : Signup button tap Action
    @IBAction func signUp_Action(_ sender: UIButton) {
        actInd.startAnimating()
        
        self.view.isUserInteractionEnabled = false
        
        let instanceOfAppManager = AppManager()
        
        
        print("Field are \(txtEmail.text), \(txtMobileNumber.text!) \(txtFirstName.text!) \(txtLastName.text!) \(txtPassword.text)")
            if txtEmail.text! != ""  || txtMobileNumber.text! != "" || txtFirstName.text! != "" || txtLastName.text! != "" || txtPassword.text! != "" || txtcountrycode.text != "" {
            
                if AppManager().isValidEmail(email: txtEmail.text!) {
                    
                    if (txtPassword.text?.characters.count)! > 0 {
                        
                        if (txtFirstName.text?.characters.count)! > 0 {
                            
                            if(txtcountrycode.text == "+91" || txtcountrycode.text == "91")
                            {
                                if(txtMobileNumber.text?.characters.count == 10)
                                {
                                    
                                    User.email         =   txtEmail.text!
                                    User.phone         =   txtMobileNumber.text!
                                    User.firstName      =   txtFirstName.text!
                                    User.lastName       =   txtLastName.text!
                                    User.password       =   txtPassword.text!
                                    User.regThrough     =   RegThrough.normal
                                    User.countryCode    = txtcountrycode.text!
                                    
                                    
                                    
                                    isEmailExist(email: User.email, regthrough: User.regThrough)
                                }
                                else
                                {
                                    DispatchQueue.main.async {
                                        self.actInd.stopAnimating()
                                        self.view.isUserInteractionEnabled = true
                                        instanceOfAppManager.showAlert(title: "Mobile should be 10 digits!", message: "", navigationController: self.navigationController!)
                                    }

                                }
                            }
                            else
                            {
                                User.email         =   txtEmail.text!
                                User.phone         =   txtMobileNumber.text!
                                User.firstName     =   txtFirstName.text!
                                User.lastName      =   txtLastName.text!
                                User.password      =   txtPassword.text!
                                User.regThrough    =   RegThrough.normal
                                User.countryCode    = txtcountrycode.text!
                                
                                isEmailExist(email: User.email, regthrough: User.regThrough)

                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                self.actInd.stopAnimating()
                                self.view.isUserInteractionEnabled = true
                                instanceOfAppManager.showAlert(title: "First Name should not be blank!", message: "", navigationController: self.navigationController!)
                            }
                            
                        }
                    }
                        else {
                            
                            DispatchQueue.main.async {
                                self.actInd.stopAnimating()
                                self.view.isUserInteractionEnabled = true
                                instanceOfAppManager.showAlert(title: "Password should not be blank.", message: "", navigationController: self.navigationController!)
                                
                           }
                    }
                    
                    } else {
                        
                        DispatchQueue.main.async {
                            self.actInd.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                            instanceOfAppManager.showAlert(title: "Invalid Email Format.", message: "", navigationController: self.navigationController!)
                        }
                    }
                    
                } else {
                    
                    DispatchQueue.main.async {
                        self.actInd.stopAnimating()
                        self.view.isUserInteractionEnabled = true
                        instanceOfAppManager.showAlert(title: "All Fields are mandatory.", message: "", navigationController: self.navigationController!)
                        
                    }
                }
    }
    
    // Mark : Checking Existance Of Email
    func isEmailExist(email : String, regthrough: String) {
         actInd.startAnimating()
        
       // self.view.backgroundColor=UIColor.red;
        let webService = WebService()
        let parameters = "email=\(email)"
        
        webService.callWebServices(url: Urls.accountStatus, methodName: "POST", parameters: parameters, istoken: false, tokenval: "") { (status, response) in
            print("Response is:  \(response)")
            
            if response.value(forKeyPath: "errmsg") as! String == ""{
                
                if(regthrough == "normal")
                {
                    DispatchQueue.main.async(execute: {
                        
                        self.actInd.stopAnimating()
                        let alrt = UIAlertController(title: "Error", message: "Email Already Exist.", preferredStyle: .alert)
                        
                        self.view.isUserInteractionEnabled = true
                        
                        
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alrt.addAction(okAction)
                        
                        self.present(alrt, animated: true, completion: {
                            
                        })
                    })
                }
                
                if(regthrough == "gmail")
                {
                    self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
                }
                
                if(regthrough == "facebook")
                {
                     self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
                }
                
                
            }
            else
            {
                if(regthrough == "normal")
                {
                    self.performSignUP(password: User.password, firstName: User.firstName, lastName: User.lastName, email: User.email, mobile: User.phone, platForm: "iOS", browser: "Mobile", regthrough: User.regThrough, socialId: "", socialToken: "")
                    
                 }
                
                if(regthrough == "gmail")
                {
                    
                    DispatchQueue.main.async(execute: {
                        
                        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "PhoneValidationViewController") as! PhoneValidationViewController
                        self.navigationController?.pushViewController(viewController, animated: true)
                    })

                    
                 }

                if(regthrough == "facebook")
                {
                    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "PhoneValidationViewController") as! PhoneValidationViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
        
        
        //  actInd.stopAnimating()
    }
    
    
    // Mark: Perform SignUp Action
    func performSignUP(password: String, firstName: String, lastName:String, email: String, mobile: String, platForm: String, browser: String, regthrough: String, socialId: String, socialToken: String) {
        
        let appManager = AppManager()
        let webservice = WebService()
        
        let deviceID: String! = UIDevice.current.identifierForVendor?.uuidString
        
        print("Device ID is \(deviceID)")
        
        var parameters = ""
       
        let countrycode: Int64	 = Int64(txtcountrycode.text!)!
        
        if regthrough == RegThrough.normal {
            
            parameters = "password=\(password)&fname=\(firstName)&lname=\(lastName)&email=\(email)&platform=iOS&browser=Mobiled&phone=\(mobile)&phcode=\(countrycode)&fbid=Normal&regthrough=\(regthrough)&ip=\(deviceID)"
            
            
            webservice.callWebServices(url: Urls.signUp, methodName: "POST", parameters: parameters, istoken: false, tokenval: "") { (sucdcess, jsonDict) in
                
                
                print(" Sign Up Return Data: \(jsonDict)")
                
                if jsonDict.value(forKey: "errmsg") as! String == "" {
                    
                    DispatchQueue.main.async {
                        self.actInd.stopAnimating()
                    }
                    
                    if regthrough == RegThrough.normal{
                        
                        DispatchQueue.main.async(execute: {
                            
                            self.actInd.stopAnimating()
                            
                            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                            viewController.checkotptype = "phone"
                            self.navigationController?.pushViewController(viewController, animated: true)
                        })
                    }
                    
                    
                    
                } else {
                 
                    DispatchQueue.main.async {
                        
                    self.view.isUserInteractionEnabled = true
                    appManager.showAlert(title: jsonDict.value(forKey: "errmsg") as! String, message: "" 
                        , navigationController: self.navigationController!)
                    self.actInd.stopAnimating()
                    }
                }
            }
            
        }
    }
    
    
    // Mark: Perform Gmail SignUp Action
    func performGmailSignUP(password: String, firstName: String, lastName:String, email: String, mobile: String, platForm: String, browser: String, regthrough: String, gTokenId: String, gUid: String) {
        
        let appManager = AppManager()
        let webservice = WebService()
        
        let deviceID: String! = UIDevice.current.identifierForVendor?.uuidString
        
        print("Device ID is \(deviceID)")
        
        var parameters = ""
        
        if regthrough == RegThrough.gmail {
            
            parameters = "password=\(password)&fname=\(firstName)&lname=\(lastName)&email=\(email)&platform=iOS&browser=Mobiled&phone=\(mobile)&phcode=91&gtokenid=\(gTokenId)&guid=\(gUid)&regthrough=\(regthrough)&ip=\(deviceID)"
            
            
            webservice.callWebServices(url: Urls.signUp, methodName: "POST", parameters: parameters, istoken: false, tokenval: "") { (sucdcess, jsonDict) in
                
                
                print(" Sign Up Return Data: \(jsonDict)")
                
                if jsonDict.value(forKey: "errmsg") as! String == "" {
                    
                    DispatchQueue.main.async {
                        self.actInd.stopAnimating()
                        
                        self.performLogin(email: email, password: password, regThrough: regthrough)
                    }
                    
//                    if regthrough == RegThrough.gmail{
//                        
//                        DispatchQueue.main.async(execute: {
//                            
//                            self.actInd.stopAnimating()
//                            
//                            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
//                            viewController.checkotptype = "phone"
//                            self.navigationController?.pushViewController(viewController, animated: true)
//                        })
//                    }
                    
                    
                    
                } else {
                    
                    DispatchQueue.main.async {
                        
                        self.view.isUserInteractionEnabled = true
                        appManager.showAlert(title: jsonDict.value(forKey: "errmsg") as! String, message: ""
                            , navigationController: self.navigationController!)
                        self.actInd.stopAnimating()
                    }
                }
            }
            
        }
    }

    
    
    // Mark: Term And Conditions
    @IBAction func termsAndConditions_Action(_ sender: UIButton) {
        
        //txtMobileNumber.resignFirstResponder()
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "Terms_ConditionsViewController") as! Terms_ConditionsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    // Mark: Signup With Facebook
    @IBAction func facbookSignUp_Action(_ sender: UIButton) {
        // self.view.isUserInteractionEnabled = false
        actInd.startAnimating()
        let fbLoginManger : FBSDKLoginManager = FBSDKLoginManager()
        
        fbLoginManger.loginBehavior = .native
        
        
        fbLoginManger.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if error == nil {
                
                let fbLoginResult : FBSDKLoginManagerLoginResult = result!
                
                if (result?.isCancelled)! {
                    
                    
                    fbLoginManger.logOut()
                    
                    return
                }
                
                if (fbLoginResult.grantedPermissions.contains("email")) {
                    
                    self.getFBUserDate()
                }
            }
        }
        self.actInd.stopAnimating()
    }
    
    func getFBUserDate() {
        
        if FBSDKAccessToken.current() != nil {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, name, first_name, last_name, picture.type(large),email"]).start(completionHandler: { (connection, result, error) in
                
                if error == nil {
                    
                    
                    print(result!)
                    
                    let returnResult : [String: Any] = result as! [String : Any]
                    
                    if returnResult["email"] as! String != "" {
                        
                        print(returnResult["email"] as! String)
                        print(returnResult["first_name"] as! String)
                        print(returnResult["last_name"] as! String)
                        print(returnResult["id"] as! String)
                        
                        User.email          =  returnResult["email"] as! String //email
                        User.firstName      =  returnResult["first_name"] as! String//firstName
                        User.lastName       =  returnResult["last_name"] as! String //lastName
                        User.socialToken    =  FBSDKAccessToken.current().tokenString
                        User.socialId       =  returnResult["id"] as! String
                        User.regThrough     =   RegThrough.facebook
                        
                        User.password =  FBSDKAccessToken.current().tokenString
                        
                        
                        self.isEmailExist(email: User.email, regthrough: User.regThrough)
                        
                    }else {
                        self.view.isUserInteractionEnabled = true
                        let alert = UIAlertController(title: "Eror", message: "Unable to fetch your details from Facebook. Do you wish to continue with the normal registration procedure?", preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                            
                                   let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
                                 self.navigationController?.pushViewController(viewController, animated: true)
                        })
                        
                        let noAction = UIAlertAction(title: "NO", style: .default, handler: { (alert) in
                            
                            
                        })
                        alert.addAction(okAction)
                        alert.addAction(noAction)
                        
                        self.present(alert, animated: true, completion: {
                            
                        })
                    }
                    print(returnResult["email"]!)
                }
            })
        }
    }
    
    // Mark: Google Sign Up Action Button
    @IBAction func googleSignUp_Action(_ sender: UIButton) {
        
        GIDSignIn.sharedInstance().signOut()
        //self.view.isUserInteractionEnabled = false
        //actInd.startAnimating()
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    // MARK: UITextField Delegate Methode to return keyboard on return key press
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        textField.resignFirstResponder()
        
        if textField == txtFirstName && txtFirstName.text != "" {
            
            txtLastName.becomeFirstResponder()
            
        } else if textField == txtLastName && txtLastName.text != "" {
            
            txtEmail.becomeFirstResponder()
            
        } else if textField == txtEmail  && txtEmail.text != ""{
            
            txtPassword.becomeFirstResponder()
            
        } else if textField == txtPassword && txtPassword.text != "" {
            
              txtMobileNumber.becomeFirstResponder()
        }

        return true
    }
 
    // Mark: TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtMobileNumber {

                //    User.isBackFromTermsConditions = false
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.frame.origin.y -= 200
                })
        
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    // Mark: TextField Delegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtMobileNumber {

            UIView.animate(withDuration: 0.2, animations: {
                self.view.frame.origin.y += 200
            })
            
        }
       
    }
    
    //MARK: Add Done Button to MobileNo keyboard
    func addDoneButtonOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(SignUpViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.txtMobileNumber.inputAccessoryView = doneToolbar
    }
    
    // Done Button Action to remove keyboard
    func doneButtonAction() {
        self.txtMobileNumber.resignFirstResponder()
    }
    

    
    func getUser() {
        
        print(User.token)
        
        let webServices = WebService()
        webServices.callWebServices(url: Urls.getUser, methodName: "GET", parameters: "", istoken: true, tokenval: User.token) { (success, jsonDict) in
            
            
            if jsonDict.value(forKey: "errmsg") as! String == "" {
                
                let response = jsonDict.value(forKey: "response") as! [NSDictionary]
                
                User.firstName = response[0].value(forKey: "firstName") as! String
                User.lastName = response[0].value(forKey: "lastName") as! String
                User.email = response[0].value(forKey: "emailID") as! String
                User.phone = response[0].value(forKey: "phone") as! String
                
                
                DispatchQueue.main.async {
                    
                    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                
            } else{
                let appManager = AppManager()
                appManager.showAlert(title: "Error!", message: jsonDict.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
            }
        }
        
        
        
    }
    func verifyEmail(email: String, phone: String) {
        
        let objWebService = WebService()
        var parameter = ""
        if phone == "" {
            parameter = "email=\(email)"
        } else {
            parameter = "email=\(email)"
        }
        
        objWebService.callWebServices(url: Urls.generateEmailOtp, methodName: "POST", parameters: parameter, istoken: false, tokenval: "") { (success, jsonResult) in
            
            print(jsonResult)
            
            if jsonResult.value(forKey: "response") as!String == "success" {
                DispatchQueue.main.async(execute: {
                    
                    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                    viewController.checkotptype = "email"
                    self.navigationController?.pushViewController(viewController, animated: true)
                })
                
            } else {
                
                let appManager = AppManager()
                appManager.showAlert(title: "Error", message: jsonResult.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
            }
            
            
        }
        
    }


    @IBAction func btnclick_countrycode(_ sender: Any) {
        
        self.txtMobileNumber.resignFirstResponder()
        DispatchQueue.main.async(execute: {
            
            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "CountryCodeSearchViewController") as! CountryCodeSearchViewController
            self.navigationController?.pushViewController(viewController, animated: true)
            
 
            
            
        })

        
//        instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
//        isShowingMenu = true
        
        
    }
    
    //MARK: Performing login Action
    func performLogin(email : String, password: String, regThrough: String) {
        
        let appManager = AppManager()
        let webService = WebService()
        
        let parameters = "user_email=\(email)&password=\(password)&logthrough=\(regThrough)"
        
        if appManager.isValidEmail(email: email) {
            
          //  webService.callWebServices(url: Urls.login, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
            webService.callWebServices(url: Urls.login_without_emailverification, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
                
                print("Login Return Result: \(dictData)  \n   Wait here...")
                
                UserDefaults.standard.set(User.email, forKey: "email")
                UserDefaults.standard.set(User.password, forKey: "password")
                UserDefaults.standard.set(User.regThrough, forKey: "regThrough")
                
                if dictData.value(forKey: "error") as! NSInteger == 216 {
                    
                    
                    
                    let errDict = dictData.value(forKey: "errmsg") as! NSDictionary
                    if errDict.value(forKey: "phone") as! Int == 0 {
                        self.actInd.stopAnimating()
                        DispatchQueue.main.async(execute: {
                            
                            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "PhoneValidationViewController") as! PhoneValidationViewController
                            viewController.isLogin = true
                            
                            self.navigationController?.pushViewController(viewController, animated: true)
                            
                        })
                        
                    } else if errDict.value(forKey: "email") as! Int == 0 {
                        
                        self.actInd.stopAnimating()
                        DispatchQueue.main.async(execute: {
                            self.verifyEmail(email: User.email, phone: "")
                        })
                    }
                    
                } else if dictData.value(forKey: "error") as! NSInteger == 0 {
                    self.actInd.stopAnimating()
                    
                    
                    print(dictData)
                    
                    let responce = dictData.value(forKey: "response") as! NSDictionary
                    User.token = responce.value(forKey: "token") as! String
                    self.getUser()
                    
                    
                    
//                    if responce.value(forKey: "onboarding") as! Int == 1 {
//                        DispatchQueue.main.async(execute: {
//
//                            User.token = responce.value(forKey: "token") as! String
//
//                            self.getUser()
//                        })
//                    } else {
//
//                        DispatchQueue.main.async(execute: {
//
//                            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
//                            self.navigationController?.pushViewController(viewController, animated: true)
//                        })
//                    }
                    
                } else {
                    let appManager = AppManager()
                    
                    DispatchQueue.main.async(execute: {
                        self.actInd.stopAnimating()
                        self.view.isUserInteractionEnabled = true
                    })
                    
                    appManager.showAlert(title: "Error", message: dictData.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
                }
            })
            
        } else{
            
            self.actInd.stopAnimating()
            
            appManager.showAlert(title: "Error", message: "Email Id Not Valid! \n ", navigationController: self.navigationController!)
        }
    }

}
