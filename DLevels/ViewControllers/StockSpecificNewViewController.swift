//
//  StockSpecificNewViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 30/05/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit
import Charts
import GoogleMobileAds

class StockSpecificNewViewController: UIViewController,UITableViewDataSource,UITableViewDelegate , UIWebViewDelegate , ChartViewDelegate, GADBannerViewDelegate{

    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet weak var navigatinViewHeaderHeight: NSLayoutConstraint!
    
    // Bottom View Constant
    @IBOutlet weak var botomCosntantWithTableView: NSLayoutConstraint!
    @IBOutlet weak var bottomViewConstant: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var chartViewTopConstant: NSLayoutConstraint!
    
    @IBOutlet weak var wvinvestingchart: UIWebView!
    
   
    @IBOutlet weak var vwButtomContainer: UIView!
    @IBOutlet weak var vwChartViewHeightCobtraint: NSLayoutConstraint!
    @IBOutlet weak var btnhamburger: UIButton!
    @IBOutlet weak var lbl_categorydesc: UILabel!
    @IBOutlet weak var chartviewheader: UILabel!
    @IBOutlet weak var lbl_ltp: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var tblHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var lbl_diff: UILabel!
    @IBOutlet weak var img_up_down: UIImageView!
    @IBOutlet weak var tblcorrection: UITableView!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var tblFundamentalHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var scrollvw: UIScrollView!
    @IBOutlet weak var tblsupportresistance: UITableView!
    @IBOutlet weak var tbl_Fundamentals: UITableView!
    
    @IBOutlet weak var chartTopHeightRemovalConstant: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblYearEndingHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblYearEnding: UILabel!
    @IBOutlet weak var lbl_stockname: UILabel!
    @IBOutlet weak var lbl_sector: UILabel!
    
    @IBOutlet weak var lbl_catg: UILabel!
    var fundamentalitems = [String]()
    var fundamentalvalue = [String]()
    var fundamentalColName = [String]()
    var items = [String]()
    var correctionlevel = [String]()
    var tblitems = [String]()
    var supreslevel = [String]()
    var months = [String]()
    var values = [String]()
   
    var levelval = [String]()
    @IBOutlet weak var lbl_category: UILabel!
    var dates = [String]()
    var symbolval = ""
    var isLoadedWebView = false
    var sector = ""
    var category = ""
    var seg_date = ""
    var highlightcolor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
    var categoryFlag = ""
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()

    
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.wvinvestingchart.isHidden = true
        
        bannerView.delegate = self
        
        //Test
        self.bottomView.isHidden = true
        wvinvestingchart.delegate = self
        chartviewheader.text = SearchForCurrentMultibagger.instrument_4
        
        tblcorrection.dataSource = self
        tblcorrection.delegate = self
        tblsupportresistance.estimatedRowHeight = 150
        tblsupportresistance.rowHeight = UITableViewAutomaticDimension
        
        symbolval = SearchForCurrentMultibagger.serchValue.replacingOccurrences(of: " ", with: "%20")
        
        addLoader()
        actInd.startAnimating()
        
      
        loadDataForTickerList(symbolval: symbolval)
        loadDataForPortfolioChecker(symbolval: symbolval)
       
        lineChartView.noDataText = ""
        
        //MARK : Plese open For Chart
         setChart(symbolval: symbolval)
         loadDataForCorrectionList(symbolval: symbolval)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = true // or false to disable rotation
        
        webViewTest()
        
        
        // In this case, we instantiate the banner with desired ad size.
        //bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        bannerView.adUnitID = "ca-mb-app-pub-8834194653550774/9862407200"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())

        
    }
    
    
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let orientationValue = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(orientationValue, forKey: "orientation")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let viewControllersList = self.navigationController?.viewControllers {
            
            
            for tempView  in viewControllersList {
                if tempView.isKind(of: SectorPerformanceDetailsViewController.self) {
                    
                    count += 1
                    print(count)
                    if count > 1
                    {
                        tempView.removeFromParentViewController()
                    }
                }
            }
        }
        
    }
    
    /*
    func removeNavigationFromSuperView(viewCont: UIViewController) {
        
        if let viewControllersList = self.navigationController?.viewControllers {
            for tempView  in viewControllersList {
                //                if tempView.isKind(of: StockSpecificNewViewController.self) {
                //                    tempView.removeFromParentViewController()
                //                }
                
                if tempView.isKind(of: vi) {
                    tempView.removeFromParentViewController()
                }
            }
        }
    } */
   
    func webViewTest() {
        
        let symbolval = SearchForCurrentMultibagger.serchValue.replacingOccurrences(of: " ", with: "%20")
        
        let url = NSURL(string: "https://www.dynamiclevels.com/charting/mobile_black.html?symbol=\(symbolval)&ctype=Candles&internal=D&style=white")
        //print(url!)
        let requestObj = URLRequest(url: url! as URL)
        wvinvestingchart.loadRequest(requestObj)
        
    }
//    func UITableView_Auto_Height()
//    {
//        if(self.tblsupportresistance.contentSize.height < self.tblsupportresistance.frame.height){
//            var frame: CGRect = self.tblsupportresistance.frame;
//            frame.size.height = self.tblsupportresistance.contentSize.height;
//            self.tblsupportresistance.frame = frame;
//        }
//    }
    override public func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.navigatinViewHeaderHeight.constant = -20
        
        
        
        if UIDevice.current.orientation.isLandscape {
            self.wvinvestingchart.isHidden = false
            self.wvinvestingchart.alpha = 1
            
            DispatchQueue.main.async {
                
                guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
                statusBar.backgroundColor = UIColor.clear
                
                if !self.isLoadedWebView {
                    self.isLoadedWebView = true
                    self.navigatinViewHeaderHeight.constant = 0
                    self.webViewTest()
                }
                
                /*
                 
                 
                 let viewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewChartViewController") as! WebViewChartViewController
                 self.navigationController?.pushViewController(viewController, animated: false)*/
            }
            
        } else if UIDevice.current.orientation.isPortrait {
            
            self.wvinvestingchart.isHidden = true
            self.wvinvestingchart.alpha = 0.1
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool{
        print("webview asking for permission to start loading")
        
        return true
    }
    
    
    
    
    public func webViewDidStartLoad(_ webView: UIWebView)
    {
        print("webview did start loading")
    }
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        print("webview did fail load with error: \(error)")
        
    }
    
    
    
    
    public func webViewDidFinishLoad(_ webView: UIWebView){
       // print("webview did finish load!")
        
        
    }

    
    //MARK: Getting data for Ticker List
    func loadDataForTickerList( symbolval : String)
    {
        
        var diff = ""
        var diffper = ""
        var date = ""
        
        
        let obj = WebService()
        let paremeters = "symbol=\(symbolval)"
        
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.live_price, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            //print("Json Data is  Live Price:  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                
                if(tempArray.count > 0)
                {
                    
                    let indexval = tempArray[0] as! NSDictionary                    
                    diff = indexval.value(forKey: "DIFF") as! String
                    diffper = indexval.value(forKey: "DIFF_PER") as! String
                    date = indexval.value(forKey: "UpdtTime") as! String
                    
                    DispatchQueue.main.async(execute: {
                        
                        self.lbl_ltp.text = (indexval.value(forKey: "LastTradedPrice") as! String)
                        self.lbl_ltp.sizeToFit()
                        if ((diff as NSString).floatValue >= 0)
                        {
                            self.lbl_diff.text = "+\(diff)(+\(diffper)%)"
                            self.lbl_diff.textColor = UIColor(red: 29/255, green: 204/255, blue: 146/255, alpha: 1)
                            self.img_up_down.image = #imageLiteral(resourceName: "up")
                            
                            
                        }
                        else
                        {
                            self.lbl_diff.text = "\(diff)(\(diffper)%)"
                            self.lbl_diff.textColor = UIColor.red
                            self.img_up_down.image = #imageLiteral(resourceName: "Down")
                        }
                        self.lbl_diff.sizeToFit()
                        self.lbl_date.text = "As on \(date)"
                        self.lbl_date.sizeToFit()
                        
                        
                    })
                    
                }
                else
                {
                    let alertobj = AppManager()
                    
                    alertobj.showAlert(title: "Error!", message: "No Data Found!" , navigationController: self.navigationController!)
                    
                    DispatchQueue.main.async {
                        
                        self.actInd.stopAnimating()
                    }
                    
                }
                
            }
            else
            {
                let alertobj = AppManager()
                DispatchQueue.main.async {
                    
                    self.actInd.stopAnimating()
                    
                    
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                    
                }
            }
            
            
        }
    }
    
    //MARK: Getting data for Corection List
    func loadDataForPortfolioChecker(symbolval : String)
    {
        //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3dzLmRsZXZlbHMuY29tL2xvZ2luIiwiaWF0IjoxNDkxODkzNzg2LCJuYmYiOjE0OTE4OTM3ODYsImp0aSI6ImhCa0ZWdXFrdTFhN25DRFgiLCJzdWIiOjIwOTk4OCwieHBhc3MiOiIkUCRCTHVBdDVrYTBUOGc0N2hiYllHV2huN2VUOVhiMDAwIn0.HdXThhfTuf3uFpCOhTXmjZP_BwcA3q_3jFKTA97y1qc"
        
        
        
        var category_desc = ""
        
        
        var marketCapType = ""
        var stockname = ""
        var lastyearperf = ""
        var lasqtrperf = ""
        var yearEnding = ""
        var yearlyData = ""
       // var  level_val = ""
        var indexval: NSDictionary = NSDictionary()
        let obj = WebService()
        let paremeters = "symbol=\(symbolval)"
        //print(paremeters)
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.portfolio_checker, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print("Json Data is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                if(tempArray.count > 0)
                {
                    
                    
                    DispatchQueue.main.async(execute: {
                        
                        for dict in tempArray {
                            
                            
                            indexval = dict as! NSDictionary
                            category_desc = indexval.value(forKey: "CategoryDesc") as! String
                            self.category = indexval.value(forKey: "CategoryLong") as! String
                            self.sector = indexval.value(forKey: "SECTOR") as! String
                            marketCapType = indexval.value(forKey: "MarketCapType") as! String
                            stockname = indexval.value(forKey: "INSTRUMENT_2") as! String
                            self.seg_date = indexval.value(forKey: "ReportDate") as! String
                            yearlyData = indexval.value(forKey: "YearlyData") as! String
                            
                            if (indexval.value(forKey: "YearEnding") is NSNull){
                                yearEnding = ""
                            }
                            else{
                                yearEnding = indexval.value(forKey: "YearEnding") as! String
                            }
                            
                            
                            self.categoryFlag = indexval.value(forKey: "CategoryFlag") as! String
                            
                            
                            
                            
                            
                            //level_val = indexval.value(forKey: "Level_Val") as! String
                           
                            self.fundamentalitems.append("Average Value")
                            self.fundamentalvalue.append("\(indexval.value(forKey: "Avg_Value") as! String)")
                            self.fundamentalColName.append("Avg_Value")
                           
                            
                            if yearlyData == "N" {
                                self.fundamentalitems.append("PAT")
                            }else{
                                self.fundamentalitems.append("PAT ( \(yearlyData) )")
                            }
                            self.fundamentalvalue.append("\(indexval.value(forKey: "ReportedPAT") as! String) Cr.")
                            self.fundamentalColName.append("ReportedPAT")
                            
                            if yearlyData == "N" {
                                self.fundamentalitems.append("PE Ratio")
                            }
                            else{
                                 self.fundamentalitems.append("PE Ratio ( \(yearlyData) )")
                            }
                            self.fundamentalvalue.append("\(indexval.value(forKey: "PERatio") as! String)")
                            self.fundamentalColName.append("PERatio")
                            
                            self.fundamentalitems.append("Market Capitalization")
                            self.fundamentalvalue.append("\(indexval.value(forKey: "MarketCap") as! String) Cr.")
                            self.fundamentalColName.append("MarketCap")
                            
                            self.fundamentalitems.append("Debt to Equity Ratio")
                            self.fundamentalvalue.append("\(indexval.value(forKey: "DERatio") as! String)")
                            self.fundamentalColName.append("DERatio")
                            
                            self.fundamentalitems.append("Dividend Yield")
                            self.fundamentalvalue.append("\(indexval.value(forKey: "DividendYield") as! String)%")
                            self.fundamentalColName.append("DividendYield")
                            
                            self.fundamentalitems.append("Book Value")
                            self.fundamentalvalue.append("\(indexval.value(forKey: "BookValue") as! String)")
                            self.fundamentalColName.append("BookValue")
                            
                            self.fundamentalitems.append("Institutions Holding")
                            self.fundamentalvalue.append("\(indexval.value(forKey: "Institutions") as! String)")
                            self.fundamentalColName.append("Institutions")
                            
                            if  self.sector != "BANKS-PRIVATE AND PSU"{
                                if yearlyData == "N" {
                                    self.fundamentalitems.append("EBITDA")
                                }else{
                                    self.fundamentalitems.append("EBITDA ( \(yearlyData) )")
                                }
                                self.fundamentalvalue.append("\(indexval.value(forKey: "Operating_Profit") as! String) Cr.")
                                self.fundamentalColName.append("Operating_Profit")
                            }
                            self.fundamentalitems.append("Promoter's Pledge")
                            self.fundamentalvalue.append("\(indexval.value(forKey: "PledgedShares") as! String)%")
                            self.fundamentalColName.append("PledgedShares")
                            
                            
                            
                            lastyearperf = "(\(indexval.value(forKey: "LastYear") as! String))"
                            
                            self.fundamentalitems.append("Last Year Performance " + lastyearperf)
                            self.fundamentalvalue.append("\(indexval.value(forKey: "LastYearPerf") as! String)%")
                            self.fundamentalColName.append("LastYearPerf")

                            lasqtrperf = "(\(indexval.value(forKey: "LastQtr") as! String))"
                            self.fundamentalitems.append("Last Quater Performance  " + lasqtrperf)
                            self.fundamentalvalue.append("\(indexval.value(forKey: "LastQtrPerf") as! String)%")
                            self.fundamentalColName.append("LastQtrPerf")
                          
                            self.fundamentalitems.append("Last Year Sector Performance  " + lastyearperf)
                            self.fundamentalvalue.append("\(indexval.value(forKey: "LastYearSectorPerf") as! String)%")
                            self.fundamentalColName.append("LastYearSectorPerf")

                            //print (self.fundamentalitems)
                            //print(self.fundamentalvalue)
                           
                        }
              //          self.vwButtomContainer.isHidden = false

                        
                       
                        
                        self.lbl_categorydesc.text = category_desc
                        self.lbl_categorydesc.sizeToFit()
                        
                        //let a = self.category.replacingOccurrences(of: "(", with: "\n(")
                        //print("Replace: \(a)")
                        
                        self.lbl_category.text = self.category.replacingOccurrences(of: "(", with: "\n(")
                        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
                        let underlineAttributedString = NSAttributedString(string: self.sector, attributes: underlineAttribute)
                        self.lbl_sector.attributedText = underlineAttributedString
                        self.lbl_sector.sizeToFit()
                        
                        self.lbl_catg.text = marketCapType
                        self.lbl_stockname.text = stockname
                        
                        
                        if yearlyData == "N" {
                            self.lblYearEndingHeightConstraint.constant = 40
                            self.lblYearEnding.isHidden = false
                            self.lblYearEnding.text = "Quarterly results for : \(yearEnding)"
                        }
                        else{
                            self.lblYearEnding.isHidden = true
                            self.lblYearEndingHeightConstraint.constant = 0
                        }
                        
                        
                        self.tblFundamentalHeightConstant.constant = CGFloat(self.fundamentalitems.count * 42)
                        
                        if(self.category == "EXIT"){
                            self.lbl_category.textColor = UIColor.red
                        
                        }   else if (self.category == "BUY"){
                            self.lbl_category.textColor = UIColor(red: 29/255, green: 204/255, blue: 146/255, alpha: 1)
                        }
                        self.tbl_Fundamentals.reloadData()
                       
                    })
                    
                   
                     self.loadDataForCorrectionList(symbolval: symbolval)
                    
                }
                else
                {
                    DispatchQueue.main.async {
                        self.actInd.stopAnimating()
                        
                       self.tbl_Fundamentals.reloadSections(IndexSet(integer: 0), with: .automatic)
                        
                        let alertobj = AppManager()
                        
                        alertobj.showAlert(title: "Error!", message: "No Data Found!" , navigationController: self.navigationController!)
                        
                    }
                    
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    let alertobj = AppManager()
                    self.actInd.stopAnimating()
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
            }
            
            
        }
    }

    
    
    
    //MARK: Getting data for Corection List
    func loadDataForCorrectionList(symbolval : String)
    {
        //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3dzLmRsZXZlbHMuY29tL2xvZ2luIiwiaWF0IjoxNDkxODkzNzg2LCJuYmYiOjE0OTE4OTM3ODYsImp0aSI6ImhCa0ZWdXFrdTFhN25DRFgiLCJzdWIiOjIwOTk4OCwieHBhc3MiOiIkUCRCTHVBdDVrYTBUOGc0N2hiYllHV2huN2VUOVhiMDAwIn0.HdXThhfTuf3uFpCOhTXmjZP_BwcA3q_3jFKTA97y1qc"
        
        var level_name = ""
        
        var  level_val = ""
        var indexval: NSDictionary = NSDictionary()
        let obj = WebService()
        let paremeters = "symbol=\(symbolval)"
        //print(paremeters)
        //actInd.startAnimating()
        
        obj.callWebServices(url: Urls.correction_report, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            //print("Correction List data is :  \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                if(tempArray.count > 0)
                {
                    self.tblitems.removeAll()
                    self.supreslevel.removeAll()
                    
                    DispatchQueue.main.async(execute: {
                        
                        for dict in tempArray {
                            
                            indexval = dict as! NSDictionary
                            level_name = indexval.value(forKey: "cd_Level_Name") as! String
                            level_val = indexval.value(forKey: "Level_Val") as! String
                            //print (level_val)
                            if (level_name  == "Recent High")
                            {
                                self.items.append("\(level_val)")
                                self.correctionlevel.append("\(level_name)")
                            }
                            else if (level_name  == "CORR_10")
                            {
                                self.items.append("\(level_val)")
                                self.correctionlevel.append("10% Correction")
                                
                            }
                            else if (level_name  == "CORR_20")
                            {
                                self.items.append("\(level_val)")
                                self.correctionlevel.append("20% Correction")
                                
                                
                            }
                            else if (level_name  == "CORR_30")
                            {
                                self.items.append("\(level_val)")
                                self.correctionlevel.append("30% Correction")
                                
                            }
                            
                            /*else if (level_name  == "Recent High")
                             {
                             self.tblitems.append("\(level_val)")
                             self.supreslevel.append("\(level_name)")
                             }*/
                            if((level_name != "CORR_10") && (level_name != "CORR_20") && (level_name != "CORR_30") && (level_name != "Recent High")){
                                self.tblitems.append("\(level_val)")
                                self.supreslevel.append("\(level_name)")
                                
                            }
                        }
                        
                        //print (self.tblitems)
                        //print ("Correction Table data: \(self.supreslevel)")
                        
                        self.tblHeightConstant.constant = CGFloat(self.supreslevel.count * 40)
                        
                       
                        self.tblcorrection.reloadData()
                        self.tblsupportresistance.reloadData()
                        
                        
                        if(self.category == "EXIT"){
                            
                           // DispatchQueue.main.async {
                                
                            self.bottomView.isHidden = true
                            self.botomCosntantWithTableView.constant =  0
                                
                                
                                 //print(self.chartViewTopConstant.constant)
                                //print(self.botomCosntantWithTableView.constant)
                                //print("dkljhsfoaigjsd;lbvlsdkzjvbhlkzdx")
                           // }
                            
                            
                            
                        }
                        else if (self.category == "BUY"){
                            
                            self.bottomView.isHidden = false
                            self.botomCosntantWithTableView.constant =  CGFloat(self.supreslevel.count * 40) + CGFloat(self.correctionlevel.count * 40)
                            
                            //print("Bottom View Height : \(self.botomCosntantWithTableView.constant)")
                            
                        }
                        else{
                           
                           self.bottomView.isHidden = false
                            //print("Bottom View Height : \(self.bottomView.frame.height)")
                            self.botomCosntantWithTableView.constant =  CGFloat(self.supreslevel.count * 40) + CGFloat(self.correctionlevel.count * 40)
                        }
                    })
                    
                }
                else
                {
                    DispatchQueue.main.async {
                        self.actInd.stopAnimating()
                        
                        self.tblcorrection.reloadSections(IndexSet(integer: 0), with: .automatic)
                        
                        let alertobj = AppManager()
                        
                        alertobj.showAlert(title: "Error!", message: "No Data Found!" , navigationController: self.navigationController!)
                    }
                    
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    let alertobj = AppManager()
                    self.actInd.stopAnimating()
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
            }
            
            
        }
    }
    
    
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblcorrection {
            return 30
        }
        
        return 40
    }
    
    //MARK: Table View Delegate
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    //MARK: Table View Delegate
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell?
        
        if tableView == self.tblcorrection {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "correctioncell", for: indexPath)
            cell.selectionStyle = .none
            
            
            //DispatchQueue.main.async {
            
            if(indexPath.row == 0)
            {
                
                cell.backgroundColor = UIColor(red: 255/255, green: 227/255, blue: 164/255, alpha: 1)
            }
            
            if(indexPath.row == 1)
            {
                cell.backgroundColor = UIColor(red: 250/255, green: 211/255, blue: 112/255, alpha: 1)
            }
            
            if(indexPath.row == 2)
            {
                cell.backgroundColor = UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
            }
            
            if(indexPath.row == 3)
            {
                cell.backgroundColor = UIColor(red: 229/255, green: 170/255, blue: 9/255, alpha: 1)
            }
            
            
            let lblCorrectionName = cell.viewWithTag(1001) as! UILabel
            lblCorrectionName.text = self.correctionlevel[indexPath.row]
            
            let lblCorrectionValue = cell.viewWithTag(1002) as! UILabel
            lblCorrectionValue.text = self.items[indexPath.row]
            
            
            
            
            //}
            return cell
            //result =  cell
            
        }
            
        else if(tableView == self.tblsupportresistance)
        {
            
            
            cell = tableView.dequeueReusableCell(withIdentifier:"supportcell", for: indexPath)
            cell?.selectionStyle = .none
            
            // DispatchQueue.main.async {
                
                cell?.backgroundColor = indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
                
                let lblsupreslevelName = cell?.viewWithTag(1003) as! UILabel
                lblsupreslevelName.text = self.supreslevel[indexPath.row]
                
                let lblsupresValue = cell?.viewWithTag(1004) as! UILabel
                lblsupresValue.text = self.tblitems[indexPath.row]
                
            // }
            
            return cell!
            //result = cell
        }
        else
        {
            cell = tableView.dequeueReusableCell(withIdentifier:"fundamentalcell", for: indexPath)
            cell?.selectionStyle = .none
            
            // DispatchQueue.main.async {
                
                cell?.backgroundColor = indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
                
                let lblsupreslevelName = cell?.viewWithTag(1005) as! UILabel
                lblsupreslevelName.text = self.fundamentalitems[indexPath.row]
            
            
                
                let lblsupresValue = cell?.viewWithTag(1006) as! UILabel
                lblsupresValue.text = self.fundamentalvalue[indexPath.row]
            
                if categoryFlag.range(of: fundamentalColName[indexPath.row]) != nil {
                    //lblsupreslevelName.textColor = UIColor.red
                    lblsupresValue.textColor = UIColor.red
                }
                
            // }
             return cell!
        }
       // return cell!
        
        
    }
    
    
    //MARK: Table View Delegate
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count:Int?
        //print(tableView)
        
        if (tableView == self.tblcorrection) {
            count = items.count
        }
        
        if (tableView == self.tblsupportresistance) {
            count = tblitems.count
        }
        if (tableView == self.tbl_Fundamentals) {
            count = fundamentalitems.count
        }

        return count!
    }
    
    override public func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        print(UIDevice.current.orientation.isPortrait)
    }

    
    func setChart(symbolval : String) {
        
        lineChartView.delegate=self
        
        let obj = WebService()
        let paremeters = "symbol=\(symbolval)"
        var result = ""
        var linechartDataSet = LineChartDataSet()
        
        //Mark: Call Service for Chart Data
        obj.callWebServices(url: Urls.chart_data, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            //print("Json Data is :  \(jsonData)")
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                DispatchQueue.main.async {
                    
                    let tempArray = jsonData.value(forKey: "response") as! NSArray
                    for dict in tempArray {
                        let dictValue = dict as! NSDictionary
                        result = dictValue.value(forKey: "DISPLAY_DATE") as! String
                        self.months.append(result)
                        self.values.append(dictValue.value(forKey: "Close") as! String)
                    }
                    
                    
                    //print(self.months)
                    //barChartView.noDataText = "You need to provide data for the chart."
                    
                    var dataEntries: [ChartDataEntry] = []
                    
                    for i in 0..<self.months.count {
                        
                        // let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
                        let dataEntry = ChartDataEntry(x: Double(i), y: Double(self.values[i])!)
                        dataEntries.append(dataEntry)
                    }
                    
                    linechartDataSet = LineChartDataSet(values: dataEntries, label: "Data Uploaded daily at 8:30 pm")
                    linechartDataSet.axisDependency = .left
                    linechartDataSet.drawCirclesEnabled = false
                    linechartDataSet.drawValuesEnabled = false
                    linechartDataSet.circleColors = [NSUIColor.white]
                    linechartDataSet.setCircleColor(UIColor.white)
                    // linechartDataSet.circleRadius = 0.0 // the radius of the node circle
                    //linechartDataSet.fillAlpha = 65 / 255.0
                    linechartDataSet.fillColor = UIColor.white
                    linechartDataSet.highlightColor = UIColor.white
                    linechartDataSet.drawCircleHoleEnabled = false
                    linechartDataSet.colors = [UIColor.black]
                    
                    self.lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: self.months)
                    self.lineChartView.xAxis.granularity = 1
                    self.lineChartView.xAxis.labelPosition = .bottom
                    self.lineChartView.rightAxis.enabled = false
                    self.lineChartView.data?.setDrawValues(false)
                    self.lineChartView.leftAxis.drawGridLinesEnabled = true
                    self.lineChartView.xAxis.drawGridLinesEnabled = true
                    self.lineChartView.legend.enabled = false
                    self.lineChartView.chartDescription?.text = ""
                    
                    var dataSets : [LineChartDataSet] = [LineChartDataSet]()
                    dataSets.append(linechartDataSet)
                    let data: LineChartData = LineChartData(dataSets: dataSets)
                    
                    data.setValueTextColor(UIColor.red)
                    self.lineChartView.data = data
                    self.actInd.stopAnimating()
                    
                }
                
                
            }
        }
        
        /* END Chart Section */
    }
    
    
    
    @IBAction func btnClickSector(_ sender: Any) {
        
        SearchForMultibaggerSector.serchValue = sector
        //SearchForCurrentMultibagger.viewcontrollername = "MultibaggerSectorList"
        User.isCommingFromSpecificPage = true
       // self.navigationController?.popViewController(animated: false)
        
        DispatchQueue.main.async {
            let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "SectorPerformanceDetailsViewController") as! SectorPerformanceDetailsViewController
            self.navigationController?.pushViewController(viewCont, animated: false)
        }
    }
    
    
    @IBAction func btn_Hamberger(_ sender: Any) {
        
        //print(SearchForCurrentMultibagger.viewcontrollername)
        self.navigationController?.popViewController(animated: false)
        //DispatchQueue.main.async {
        
       /* if(SearchForCurrentMultibagger.viewcontrollername == "MultibaggerList"){
            //let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
            //self.navigationController?.pushViewController(viewController, animated: false)
             self.navigationController?.popViewController(animated: false)
        }
        
        if (SearchForCurrentMultibagger.viewcontrollername == "YearWiseMultibagger"){
            
            //let viewController = self.storyboard?.instantiateViewController(withIdentifier: "PastPerformenceViewController") as! PastPerformenceViewController
            
            //self.navigationController?.pushViewController(viewController, animated: false)
             self.navigationController?.popViewController(animated: false)
        }
        
        if (SearchForCurrentMultibagger.viewcontrollername == "Search"){
            
            //let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
            
            //self.navigationController?.pushViewController(viewController, animated: false)
            self.navigationController?.popViewController(animated: false)
        }*/
       
        // }
    }

    
    
    
    
}
