//
//  DashBoardViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 25/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class DashBoardViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var btnHamburgerMenu: UIButton!
    @IBOutlet weak var tickerlistview: UITableView!
    @IBOutlet weak var bottomScrollView: UIScrollView!
    
    @IBOutlet weak var navigationView: UIView!
    
    let images  = ["Facebook.png","Google.png","logo 2.png"]
    let names   = ["INDICIES","INDICIES FUTURES","STOCKS"]
    let hint    = ["INDEX","FUTURE","STOCK"]
    
    
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    
    var response: NSArray = []
    var tablevwbindarr = [NSDictionary]()
    
    var color = UIColor(red: 40/255, green: 76/255, blue: 90/255, alpha: 1)
    var highlightcolor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
    var profitcolor = UIColor(red: 29/255, green: 204/255, blue: 136/255, alpha: 1)
    var losscolor = UIColor(red: 257/255, green: 65/255, blue: 61/255, alpha: 1)
    
    var isShowingMenu = false
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareScrollView();
        self.tickerlist();
        tickerlistview.tableFooterView = UIView()
        
        actInd.startAnimating()
        AppManager().setStatusBarBackgroundColor()
        
    }
    
    //MARK: For Activity Indicator
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.tickerlistview.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    
    @IBAction func temp_function(_ sender: UIButton) {
        instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view, navigationView: navigationView)
        isShowingMenu = false
    }
    @IBAction func backTo_LoginViewController(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell =
            tableView.dequeueReusableCell(withIdentifier:
                "tickercell", for: indexPath) 
        DispatchQueue.main.async {
            
            //let row = indexPath.row
            
            //cell.attractionLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
            
            var lblval = cell.viewWithTag(1000) as! UILabel
            lblval.text =  self.tablevwbindarr[indexPath.row].value(forKey: "INSTRUMENT_4") as? String
            
            lblval = cell.viewWithTag(1001) as! UILabel
            lblval.text = ""
            lblval.text =  self.tablevwbindarr[indexPath.row].value(forKey: "ClosePrice") as? String
            
            let lblimgvw = cell.viewWithTag(1002) as! UIImageView
            
            let val = self.tablevwbindarr[indexPath.row].value(forKey: "DIFF_PER") as! String
            let myDouble: Double! = Double(val)
            
            if( myDouble > Double(0))
            {
                lblval.textColor = self.profitcolor
                lblimgvw.image=UIImage(named: "Green_Rectangle")
                
            }
            else
            {
                lblval.textColor = self.losscolor
                lblimgvw.image=UIImage(named: "Down")
            }
            
            lblval = cell.viewWithTag(1003) as! UILabel
            lblval.text =  self.tablevwbindarr[indexPath.row].value(forKey: "DIFF") as? String
            
            lblval = cell.viewWithTag(1004) as! UILabel
            lblval.text =  (self.tablevwbindarr[indexPath.row].value(forKey: "DIFF_PER") as? String)! + "%"
            
        }
        
        
         return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tablevwbindarr.count
    }
    
    private func prepareScrollView() {
    
        bottomScrollView.showsHorizontalScrollIndicator = true
        
        var fromLeft: CGFloat = 10
        
        /*
        for (index,image) in images.enumerated(){
            
            let button = UIButton()
            
            button.accessibilityHint = hint[index]
            button.contentMode       = .scaleAspectFill
            button.setTitle(names[index], for: .normal)
            button.setTitleColor(color, for: .normal)
            button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 16)
            
            button.titleLabel?.adjustsFontSizeToFitWidth = true
            button.titleLabel?.lineBreakMode = .byClipping
            button.sizeToFit()
            
            let btnwidth = UIScreen.main.bounds.width/3-3
            button.frame = CGRect(x: fromLeft, y: 0, width: btnwidth, height: 50)
            button.contentHorizontalAlignment = .center
            button.layer.borderColor = UIColor.blue.cgColor
            
            fromLeft += button.frame.width - 3
            button.addTarget(self, action: #selector(DashBoardViewController.buttonTappedInScrollView(sender:)), for: .touchUpInside)
            
            button.tag = index
            bottomScrollView.addSubview(button)
            
            let highlightvw = UIView(frame: CGRect(x: button.frame.origin.x, y: button.frame.origin.y + button.frame.height, width: btnwidth, height: 5))
    
            highlightvw.tag = index
            if(index == 0)
            {
                highlightvw.backgroundColor = highlightcolor
                button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 10)
            }
            else
            {
                highlightvw.backgroundColor = UIColor.white
                button.titleLabel?.font = UIFont(name: "Montserrat-Light", size: 10)
            }
            
            bottomScrollView.addSubview(highlightvw)
            
            bottomScrollView.contentSize = CGSize(width: fromLeft, height: 1)
        }
        
        */
        
        for (index,_) in images.enumerated(){
            
            let button = UIButton()
            
            button.accessibilityHint = hint[index]
            button.contentMode       = .scaleAspectFill
            button.setTitle(names[index], for: .normal)
            button.setTitleColor(color, for: .normal)
            button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 16)
            
            button.titleLabel?.adjustsFontSizeToFitWidth = true
            button.titleLabel?.lineBreakMode = .byClipping
            button.sizeToFit()
            
            let btnwidth = UIScreen.main.bounds.width/3-3
            button.frame = CGRect(x: fromLeft, y: 0, width: btnwidth, height: 50)
            button.contentHorizontalAlignment = .center
            button.layer.borderColor = UIColor.blue.cgColor
            
            fromLeft += button.frame.width - 3
            button.addTarget(self, action: #selector(DashBoardViewController.buttonTappedInScrollView(sender:)), for: .touchUpInside)
            
            button.tag = index
            bottomScrollView.addSubview(button)
            
            let highlightvw = UIView(frame: CGRect(x: button.frame.origin.x, y: button.frame.origin.y + button.frame.height, width: btnwidth, height: 5))
            
            highlightvw.tag = index
            if(index == 0)
            {
                highlightvw.backgroundColor = highlightcolor
                button.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 10)
            }
            else
            {
                highlightvw.backgroundColor = UIColor.white
                button.titleLabel?.font = UIFont(name: "Montserrat-Light", size: 10)
            }
            
            bottomScrollView.addSubview(highlightvw)
            
            bottomScrollView.contentSize = CGSize(width: fromLeft, height: 1)
        }
        
        
        
        self.view.layoutIfNeeded()
    }
    
    //MARK: Scroll View Tap Action
    func buttonTappedInScrollView(sender: UIButton){
        
       let subViews = self.bottomScrollView.subviews
        for subview in subViews {
            
            if(subview.isKind(of: UIView.self))
            {
                if(subview.tag == sender.tag)
                {
                    subview.backgroundColor = highlightcolor
                }
                else
                {
                    subview.backgroundColor = UIColor.white
                }
            }
            if(subview.isKind(of: UIButton.self))
            {
            
                if(subview.tag == sender.tag)
                {
                    let btn = subview as! UIButton
                    btn.backgroundColor = UIColor.white
                    btn.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 10)
                    
                }
                else
                {
                    let btn = subview as! UIButton
                    btn.titleLabel?.font = UIFont(name: "Montserrat-Light", size: 10)
                }
            }
        }
        
        bindArrLoad(type: sender.accessibilityHint!)
        
    }
    
    
    
    func bindArrLoad(type: String)
    {
        self.tablevwbindarr.removeAll()
        
        //print(response)
        
        for element in response {
           
            let data = element as! NSDictionary
            if(data.value(forKey: "Type") as! String == type)
            {
                _ = data
                self.tablevwbindarr.append(data)
                
            }
        }
        
        DispatchQueue.main.async { 
            self.tickerlistview.reloadData()
            self.actInd.stopAnimating()
        }
    }
    
    //MARK: Getting data from WebService
    func tickerlist()
    {
        let obj = WebService()
        let type_param = "all"
        
        obj.callWebServices(url: Urls.dashboardTicker, methodName: "GET", parameters: "type=\(type_param)", istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
               self.response = jsonData.value(forKey: "response") as! NSArray
                self.bindArrLoad(type: "INDEX")
            }
            else
            {
                let alertobj = AppManager()
                
                alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                
            }
        }
    }
    
    @IBAction func onClickMultibaggerAction(_ sender: UIButton) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerLandingViewController") as! MultibaggerLandingViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func onClickContactUsAction(_ sender: UIButton) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    
    
    
    
    //MARK: For Slide Menu
    @IBAction func onClick_Hamberger(_ sender: UIButton) {
        
        if !isShowingMenu {
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburgerMenu.frame.origin.x += 250
            isShowingMenu = true
        } else {
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburgerMenu.frame.origin.x -= 250
            isShowingMenu = false
        }
    }   
    
}
    
    

