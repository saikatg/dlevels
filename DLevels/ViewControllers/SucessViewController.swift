//
//  SucessViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 13/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class SucessViewController: UIViewController {
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    @IBOutlet weak var btnStart: UIButton!
    var onBoardingText = ""
    
    @IBOutlet weak var btnstart: UIButton!
    @IBOutlet weak var btnskip: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addLoader()
        btnskip.layer.cornerRadius = 5
        btnskip.layer.borderWidth = 1
        btnskip.layer.borderColor = UIColor.black.cgColor
        btnskip.layer.cornerRadius = 5
        btnskip.layer.borderWidth = 1
        btnskip.layer.borderColor = UIColor.black.cgColor
        
        if(User.regThrough == "normal")
        {
            self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
        }
        else
        {
            User.password = User.socialToken
            self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // Ading Loader
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    
    //MARK: Start Button Click Option
    @IBAction func onClickStartButton(_ sender: UIButton) {
        actInd.startAnimating()
        btnStart.isEnabled = false
        DispatchQueue.main.async {
            
//            if(User.regThrough == "normal")
//            {
//                self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
//            }
//            else
//            {
//                User.password = User.socialToken
//                self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
//            }
            self.actInd.stopAnimating()
            
//            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
//
//            self.navigationController?.pushViewController(viewController, animated: true)
            
        }
    }
    
    func pushViewAfterSkipButtonPressed() {
        
        
      //  DispatchQueue.main.async {
            
//            if(User.regThrough == "normal")
//            {
//                self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
//            }
//            else
//            {
//                User.password = User.socialToken
//                self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
//            }
        
       // }
        
        let webServiceCall = WebService()
        let appManager     = AppManager()
        
        let param = "1|,2|,3|"
        
        let parameter   =   "values=\(param)" //values=\(User.OnboardinString)"
        //print("User token is:\(User.token)")
        
        webServiceCall.callWebServices(url: Urls.onBoarding, methodName: "POST", parameters: parameter, istoken: true, tokenval: User.token) { (success, jsonData) in
            
            print(jsonData)
            if jsonData.value(forKey: "errmsg") as! String == "" {
                //print("User Onboarding Data is \(User.OnboardinString)")
                DispatchQueue.main.async {
                    self.delay(1, closure: {
                        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
                        self.navigationController?.pushViewController(viewController, animated: true)
                    })
                }
                
            }else {
                
                DispatchQueue.main.async {
                    
                    // self.btnDone.isEnabled = true
                    if let msg = jsonData.value(forKey: "errormsg") {
                        appManager.showAlert(title: "Error", message: "\(msg)", navigationController: self.navigationController!)
                    }
                    
                }
            }
        }
        
    }
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    @IBAction func btn_skip(_ sender: Any) {
        
        User.OnboardinString    = "\(User.OnboardinString)\(onBoardingText)"
        pushViewAfterSkipButtonPressed()
    }
    // MARK: Performing Login Action
    func performLogin(email : String, password: String, regThrough: String) {
        
        let appManager = AppManager()
        let webService = WebService()
        
        let parameters = "user_email=\(email)&password=\(password)&logthrough=\(regThrough)"
        
        if appManager.isValidEmail(email: email) {
            
            UserDefaults.standard.set(User.email, forKey: "email")
            UserDefaults.standard.set(User.password, forKey: "password")
            UserDefaults.standard.set(User.regThrough, forKey: "regThrough")
            
//            webService.callWebServices(url: Urls.login, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
                            webService.callWebServices(url: Urls.login_without_emailverification, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
            print(dictData)
                if dictData.value(forKey: "error") as! NSInteger == 0 {
                    
                    let tokendic = dictData.value(forKey: "response") as! NSDictionary
                    User.token = (tokendic.value(forKey: "token") as! NSString) as String
                    
                   // DispatchQueue.main.async {
                        
//                        self.actInd.stopAnimating()
//                        
//                        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
//                        
//                        self.navigationController?.pushViewController(viewController, animated: true)
//                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        
                    self.btnStart.isEnabled = true
                    appManager.showAlert(title: "Error", message: (dictData.value(forKey: "response") as! NSString) as String, navigationController: self.navigationController!)
                    }
                }
            })
            
        } else{
            self.btnStart.isEnabled = true
            appManager.showAlert(title: "Error", message: "Email Id Not Valid! \n ", navigationController: self.navigationController!)
        }
    }
}
