//
//  MultibaggerSectorViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 15/02/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class MultibaggerSectorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var btnHamberger: UIButton!
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var segmentView: UIView!
   
    @IBOutlet weak var tblMultibaggerSectorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblMultibaggerSectorMinorHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var bottomScrollView: UIScrollView!
    @IBOutlet weak var tblMultibaggerSector: UITableView!
    
    @IBOutlet weak var tblMultibaggerSectorMinor: UITableView!
    @IBOutlet weak var lbl_sortedByText: UILabel!
    
    var selectedQuaterIndex = 0
    
    
    var typeOfMutibagger = ""
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var arrayData = [NSDictionary]()
    var segmentarr = [String]()
    var arrRepType   =  [String]()
    var month = 3
    
    var segment_date = "";
    
    var response = [NSDictionary]()
    var responseMinor = [NSDictionary]()
    var alldata = [NSDictionary]()
    
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    var isShowingMenu = false
    
    var gestureRecogn : UITapGestureRecognizer?
    var flag_sort:Int = 0
    var  sortedBydata : NSArray = []
    
    var highlightcolor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
    
    
    let names   = ["SmallCap List","MidCap List","LargeCap List","Quarterly Sector Performance","Yearly Sector Performance"]
    let hint    = ["SmallCap List","MidCap List","LargeCap List","Quarterly Sector Performance","Yearly Sector Performance"]
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
       
        lblHeader.text = "Multibaggers"
        
        prepareScrollView()
        
        self.lbl_sortedByText.text = "Sorted by chng % for \(MultibaggerStats.yearperformancetxt[0])"
        self.segmenmtDataLoad()
        loadAllData()
        
        bottomScrollView.setContentOffset(CGPoint(x: 240, y: 0), animated: false)
        
        gestureRecogn = UITapGestureRecognizer(target: self, action: #selector(MultibaggerLandingViewController.tapAction(sender:)))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = false // or false to disable rotation
        
        //self.tblMultibaggerSector.sizeToFit()
        addLoader()
        actInd.startAnimating() 
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    @IBAction func btn_sort_data(_ sender: Any) {
        
        self.response = self.response.reversed()
        
        let sectionIndex = IndexSet(integer: 0)
        self.tblMultibaggerSector.reloadSections(sectionIndex, with: .top)
        self.view.layoutIfNeeded()
        
        
    }
    
    func addLoader() {
        
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    //MARK": Getting Data for table.
    func loadAllData()
    {
        
        if(MultibaggerStats.multibaggersegmentdata.count == 0)
        {
            self.actInd.startAnimating()
            
            let obj = WebService()
            let paremeters = "date=1900-01-01"
            
            obj.callWebServices(url: Urls.sectorwise_Performence, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
                
                print("Json Data is :  \(jsonData)")
                
                if jsonData.value(forKey: "errmsg") as! String == "" {
                    self.alldata.removeAll()
                    
                    self.alldata = jsonData.value(forKey: "response") as! [NSDictionary]
                    //self.alldata = jsonData.value(forKey: "response") as! [NSDictionary]
                   
                    
                    print("AllDATALOAD \(self.alldata)")
                    self.loadDataForMultibaggerList(dateval: self.segmentarr[0])
                    MultibaggerStats.multibaggersegmentdata = self.alldata
                }
                else
                {
                    self.actInd.stopAnimating()
                    let alertobj = AppManager()
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
            }
        }
        else
        {
            self.alldata = MultibaggerStats.multibaggersegmentdata
            
            DispatchQueue.main.async(execute: {
                
                self.loadDataForMultibaggerList(dateval: self.segmentarr[0])
                
                self.actInd.stopAnimating()
                
            })
        }
    }
    
    
    //MARK": Getting Data for table.
    func loadDataForMultibaggerList(dateval: String)
    {
        
        self.response.removeAll()
        self.responseMinor.removeAll()
        for arr in alldata {
            
            if (arr.value(forKey: "mbd_Date") as! String) == dateval  {
                if (arr.value(forKey: "SectorType") as! String) == "MAJOR" {
                    
                    
                    self.response.append(arr)
                    
                }
                else{
                    
                    self.responseMinor.append(arr)
                    
                }
            }
        }
        
        
        print("Major Count is \(response.count) : \(response)")
        print("Minor Count is \(responseMinor.count) : \(responseMinor)")
        
        
        DispatchQueue.main.async(execute: {
            
            self.tblMultibaggerSectorMinorHeightConstraint.constant = CGFloat(self.responseMinor.count * 44)
            self.tblMultibaggerSectorHeightConstraint.constant = CGFloat(self.response.count * 44)
            
            
            self.tblMultibaggerSector.reloadSections(IndexSet(integer: 0), with: .automatic)
            self.tblMultibaggerSectorMinor.reloadSections(IndexSet(integer: 0), with: .automatic)
            
            self.actInd.stopAnimating()
            
        })
    }
    
    
    //MARK: Prepare Scroll View
    private func prepareScrollView() {
        
        bottomScrollView.showsHorizontalScrollIndicator = false
        var fromLeft: CGFloat = 10
        
        for index in 0..<5 {
            
            let button = UIButton()
            button.contentMode       = .scaleAspectFill
            button.setTitle(names[index], for: .normal)
            button.setTitleColor(UIColor(red: 40/255, green: 76/255, blue: 90/255, alpha: 1), for: .normal)
            button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 12)
            
            button.accessibilityHint = names[index]
            
            button.titleLabel?.lineBreakMode = .byClipping
            button.frame = CGRect(x: fromLeft, y: 0, width: 175, height: 35)
            button.contentHorizontalAlignment = .center
            button.layer.borderColor = UIColor.blue.cgColor
            
            fromLeft +=  175
            button.addTarget(self, action: #selector(self.buttonTappedInScrollView(sender:)), for: .touchUpInside)
            //button.backgroundColor = UIColor.yellow
            
            button.tag = index
            bottomScrollView.addSubview(button)
            
            let highlightvw = UIView(frame: CGRect(x: button.frame.origin.x, y: button.frame.origin.y + button.frame.height, width: 175, height: 2.5))
            
            highlightvw.tag = index
            
            //   print("--vvvvvv----\(index)")
            if(index == 4)
            {
                highlightvw.backgroundColor = highlightcolor
                button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 13)
            }
            else
            {
                highlightvw.backgroundColor = UIColor.white
                button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 11)
            }
            
            bottomScrollView.addSubview(highlightvw)
            bottomScrollView.contentSize = CGSize(width: 900, height: 1)
            
            
            
        }
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.bottomScrollView.contentOffset.x = CGFloat(175*3 + 45)
            }, completion: nil)
        }
        
        self.view.layoutIfNeeded()
    }
    
    
    //MARK: Scroll View Tap Action
    func buttonTappedInScrollView(sender: UIButton){
        
        
        if let page = sender.accessibilityHint {
            
            if page == "SmallCap List" {
                
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
                
                
            }
            if page == "MidCap List" {
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerMidCapViewController") as! MultibaggerMidCapViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
                
            }
            if page == "LargeCap List" {
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerLargeCapViewController") as! MultibaggerLargeCapViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
                
            }
            if page == "Yearly Sector Performance" || page == "YEARLY SECTOR PERFORMANCE" {
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerSectorViewController") as! MultibaggerSectorViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
            }
            
            if page == "Quarterly Sector Performance" || page == "QUARTERLY SECTOR PERFORMANCE" {
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerQuarterlySectorViewController") as! MultibaggerQuarterlySectorViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
            }
        }
    }
    
    
    
    func segmenmtDataLoad()
    {
        
        segmentarr = MultibaggerStats.segYearDateArr
        arrRepType = MultibaggerStats.segYearDisplayArr
        
        
        
        self.segment_date = self.segmentarr[0]
        self.buttonViewLoad()
       // var msgarr = arrRepType[0].components(separatedBy: "-")
        // self.lbl_sortedByText.text = "Sorted by chng % from 1st April \(msgarr[0]) till date"
        
        
    }
    
    
    
    //MARK: Creating Segment View
    func segmentViewLoad()
    {
        //let items = ["3 Months", "6 Months", "9 Months","12 Months"]
        
        
        //print("segment---------\(arrRepType)")
        
        let segmentedControl = UISegmentedControl(items: arrRepType)
        
        segmentedControl.frame = CGRect(x:-8.5 , y: bottomScrollView.frame.origin.x + 10, width: UIScreen.main.bounds.width - 35 , height: 30)
        segmentedControl.addTarget(self, action: #selector(MultibaggerViewController.segmentedControlValueChanged(segment:)), for: .valueChanged)
        let font = UIFont.systemFont(ofSize: 12)
        segmentedControl.setTitleTextAttributes([NSFontAttributeName: font],
                                                for: .normal)
        // segmentedControl.tintColor   = UIColor(red: 247/255, green: 197/255, blue: 58/255, alpha: 1)
        
        segmentedControl.tintColor   = UIColor(red: 247/255, green: 197/255, blue: 58/255, alpha: 1)
        segmentedControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(red: 40/255, green: 76/255, blue: 90/255, alpha: 1)], for: .selected)
        segmentedControl.backgroundColor = UIColor.white
        segmentedControl.selectedSegmentIndex = 0
        
        segmentView.addSubview(segmentedControl)
    }
    
    //MARK: Segment View Controller Tapped Action
    func segmentedControlValueChanged(segment: UISegmentedControl) {
        
        self.segment_date = segmentarr[segment.selectedSegmentIndex]
        print(self.segment_date)
        SearchForMultibaggerSector.seg_date = self.segment_date
        
        self.loadDataForMultibaggerList(dateval: (segmentarr[segment.selectedSegmentIndex] as NSString) as String);
        
        
        UIView.animate(withDuration: 0.4) {
            
            self.actInd.stopAnimating()
            // self.tblMultibaggerSectorHeightConstraint.constant = CGFloat(self.response.count * 44)
            //self.tblMultibaggerSectorMinorHeightConstraint.constant = CGFloat(self.responseMinor.count * 44)
            self.tblMultibaggerSector.reloadData()
            self.tblMultibaggerSectorMinor.reloadData()
            //self.tickerlistview.reloadSections(sectionIndex, with: .automatic)
        }
        
        
        
        
    }
    
    /*  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
     return UITableViewAutomaticDimension
     }
     
     
     func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
     return UITableViewAutomaticDimension
     }
     */
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tblMultibaggerSector{
            return response.count
        }
        else {
            return responseMinor.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell?
        
        if tableView == self.tblMultibaggerSector{
            
            cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            cell?.selectionStyle = .none
            
            cell?.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            
            //        cell?.backgroundColor = indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
            
            let lblSectorName = cell?.viewWithTag(1000) as! UILabel
            lblSectorName.text = response[indexPath.row].value(forKey: "mbd_Sector") as? String
            // lblSectorName.adjustsFontSizeToFitWidth = true
            
            let lblPerforManceReport = cell?.viewWithTag(1001) as! UILabel
            lblPerforManceReport.text = response[indexPath.row].value(forKey: "Performance") as? String
            
            let lblStocksinSector = cell?.viewWithTag(1002) as! UILabel
            lblStocksinSector.text = response[indexPath.row].value(forKey: "Symbol_Count") as? String
            
            return cell!
            
        }
        else{
            
            cell = tableView.dequeueReusableCell(withIdentifier: "cellminor")
            cell?.selectionStyle = .none
            
            cell?.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            
            //        cell?.backgroundColor = indexPath.row % 2 == 0 ? UIColor(red: 232/255, green: 232/255, blue: 232/255, alpha: 0.4) : UIColor.white
            
            let lblSectorName = cell?.viewWithTag(1003) as! UILabel
            lblSectorName.text = responseMinor[indexPath.row].value(forKey: "mbd_Sector") as? String
            // lblSectorName.adjustsFontSizeToFitWidth = true
            
            let lblPerforManceReport = cell?.viewWithTag(1004) as! UILabel
            lblPerforManceReport.text = responseMinor[indexPath.row].value(forKey: "Performance") as? String
            
            let lblStocksinSector = cell?.viewWithTag(1005) as! UILabel
            lblStocksinSector.text = responseMinor[indexPath.row].value(forKey: "Symbol_Count") as? String
            
            return cell!
            
        }
       // return cell!
        
    }
    
    func addGestureRecogniser(){
        let  tapGesture = UITapGestureRecognizer(target: self, action: #selector(MultibaggerLandingViewController.tapAction(sender:)))
        self.tblMultibaggerSector.addGestureRecognizer(tapGesture)
        
        let  swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(MultibaggerLandingViewController.tapAction(sender:)))
        swipeGesture.direction = .left
        //self.tblMultibaggerSector.addGestureRecognizer(swipeGesture)
        self.view.addGestureRecognizer(swipeGesture)
    }
    
    func tapAction(sender: UITapGestureRecognizer) {
        
        instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
        
        self.tblMultibaggerSector.removeGestureRecognizer(gestureRecogn!)
        //   btnHamburger.frame.origin.x -= 250
        isShowingMenu = false
    }
    
    
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == self.tblMultibaggerSector{
            
            SearchForMultibaggerSector.serchValue = response[indexPath.row].value(forKey: "mbd_Sector") as! String
        }
        else{
            SearchForMultibaggerSector.serchValue = responseMinor[indexPath.row].value(forKey: "mbd_Sector") as! String
            
        }
        
        SearchForMultibaggerSector.seg_date = self.segment_date
        SearchForCurrentMultibagger.viewcontrollername = "MultibaggerSectorList"
        
        DispatchQueue.main.async {
            let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "SectorDetailsViewController") as! SectorDetailsViewController
             viewCont.period = MultibaggerStats.yearperformancetxt[self.selectedQuaterIndex]
            
            self.navigationController?.pushViewController(viewCont, animated: false)
        }
        
        
    }
    
    func buttonViewLoad()
    {
        
        var originx = 0
        
        for (index, element) in arrRepType.enumerated() {
            
            print(segmentView.frame.width);
            
            let button = UIButton(frame: CGRect(x:originx , y: Int(segmentView.frame.origin.x-8), width: (Int(segmentView.frame.width/4) - 4) , height: 40))
            button.backgroundColor = UIColor.white
            button.setTitle(element, for: .normal)
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpOutside)
            
            
            button.setTitleColor(UIColor(red: 103/255, green: 128/255, blue: 138/255, alpha: 1), for: .normal)
            button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 12)
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
            
            button.layer.borderColor = UIColor(red: 103/255, green: 128/255, blue: 138/255, alpha: 1).cgColor
            button.layer.borderWidth = 1.0
            button.tag = index
            
            if(index == 0)
            {
                button.backgroundColor = self.highlightcolor
            }
            
            segmentView.addSubview(button)
            originx  = originx + Int(button.frame.width )
            
            print(button.frame.width)
        }
        
    }
    
    
    
    func buttonAction(sender: UIButton!) {
        
        self.loadDataForMultibaggerList(dateval: (segmentarr[sender.tag]) as String)
        self.segment_date = segmentarr[sender.tag] as String
        SearchForMultibaggerSector.seg_date = segmentarr[sender.tag] as String
       // let msg = arrRepType[sender.tag] as String
        //self.actInd.stopAnimating()
        self.tblMultibaggerSector.reloadData()
        //self.tickerlistview.reloadSections(sectionIndex, with: .automatic)
        sender.backgroundColor = self.highlightcolor
        
        selectedQuaterIndex = sender.tag
        
        
        self.lbl_sortedByText.text = "Sorted by chng % for \(MultibaggerStats.yearperformancetxt[sender.tag])"
        
        if self.segmentView != nil{
            
            for v in self.segmentView.subviews{
                if(v.tag != sender.tag)
                {
                    v.backgroundColor = UIColor.white
                }
            }
        }
    }
    
    @IBAction func btnMinorSectorSort(_ sender: Any) {
        
        
        self.responseMinor = self.responseMinor.reversed()
        
        let sectionIndex = IndexSet(integer: 0)
        self.tblMultibaggerSectorMinor.reloadSections(sectionIndex, with: .top)
        self.view.layoutIfNeeded()
        
        
        
    }
    @IBAction func btnSearchAction(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func onClickBackButton(_ sender: UIButton) {
        
        if !isShowingMenu {
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            btnHamberger.frame.origin.x += 250
            isShowingMenu = true
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            btnHamberger.frame.origin.x -= 250
            isShowingMenu = false
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        
        //self.view.setupContentViewForViewWithScroll(contentView: contentView)
        
        
    }
    
    
}
