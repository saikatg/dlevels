//
//  PhoneValidationViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 18/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class PhoneValidationViewController: UIViewController {
    
    @IBOutlet weak var txtPhoneNo: UITextField!
    
    @IBOutlet weak var txtCountryCode: UITextField!
    
    @IBOutlet weak var btn_verify: UIButton!
    var instanceOfLeftSlideMenu = SectorPopup()
    var isShowingMenu = false

    var isLogin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardOnTapOutside()
        if(CountryCode.countryCode != ""){
            self.txtCountryCode.text = "+\(CountryCode.countryCode)"}
        else{
            self.txtCountryCode.text = "+91"
        }

        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if(CountryCode.countryCode != ""){
            self.txtCountryCode.text = "+\(CountryCode.countryCode)"}
        else{
            self.txtCountryCode.text = "+91"
        }
        
        
    }
    
    @IBAction func btnVerify_Action(_ sender: UIButton) {
        
       // print(" Phone No is:   \(txtPhoneNo.text)")
        
        User.phone = txtPhoneNo.text!
        User.countryCode = txtCountryCode.text!
        btn_verify.isEnabled = false
        if isLogin {
            
            validate_Phone(email: User.email, phone: User.phone)
            
        }else {
            performSignUP(password: User.password, firstName: User.firstName, lastName: User.lastName, email: User.email, mobile: User.phone, platForm: "iOS", browser: "Mobile", regthrough: User.regThrough, socialId: User.socialId, socialToken: User.socialToken, CountryCode: User.countryCode)
        }
    }
    
    //MARK: Verify Phone
    func validate_Phone(email: String, phone: String){
        
        
        let webService = WebService()
        
        let parameters = "email=\(email)&phone=\(phone)"
        
        print("parametes is \(parameters)" )
        webService.callWebServices(url: Urls.generatePhoneOtp, methodName: "POST", parameters: parameters, istoken: false, tokenval: "") { (success, jsonDict) in
            
            print("json return for changing phone \(jsonDict)")
            
            if jsonDict.value(forKey: "error") as! Int  == 0 {
            
            DispatchQueue.main.async(execute: {
            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                viewController.checkotptype = "phone"
            
            self.navigationController?.pushViewController(viewController, animated: true)
            })
                
            } else {
                self.btn_verify.isEnabled = true

              let alrt = AppManager()
                alrt.showAlert(title: "Eror", message: "\(jsonDict.value(forKey: "error") as! String)", navigationController: self.navigationController!)
            }
            
        }
    }
    
    
    // MARK: Perform SignUp
    func performSignUP(password: String, firstName: String, lastName:String, email: String, mobile: String, platForm: String, browser: String, regthrough: String, socialId: String, socialToken: String, CountryCode: String) {
        let appManager = AppManager()
        let webservice = WebService()
        
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        
        print("Device ID is \(deviceID)")
        
        var parameters = ""
        
        if regthrough == RegThrough.gmail{
            
            parameters = "password=\(generateRandomDigits(8))&fname=\(firstName)&lname=\(lastName)&email=\(email)&platform=iOS&browser=Mobiled&phone=\(mobile)&phcode=\(CountryCode)&gtokenid=\(User.socialToken)&guid=\(User.socialId)&regthrough=\(regthrough)&ip=\(deviceID)"
            
            
        } else if regthrough == RegThrough.facebook{
            
            parameters = "password=\(generateRandomDigits(8))&fname=\(firstName)&lname=\(lastName)&email=\(email)&platform=iOS&browser=Mobiled&phone=\(mobile)&phcode=\(CountryCode)&fbtoken=\(User.socialToken)&fbuid=\(User.socialId)&regthrough=\(regthrough)&ip=\(deviceID)"
        }
        
        webservice.callWebServices(url: Urls.signUp, methodName: "POST", parameters: parameters, istoken: false, tokenval: "") { (sucdcess, jsonDict) in
            
            print(" Sign Up Return Data: \(jsonDict)")
            
            if jsonDict.value(forKey: "errmsg") as! String == "" {
                
                DispatchQueue.main.async(execute: {
                    
                    //let objWebService = WebService()
                    
                  //  objWebService.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
                    
                    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                    viewController.checkotptype = "phone"
                    
                    self.navigationController?.pushViewController(viewController, animated: true)
                })

            }else {
                self.btn_verify.isEnabled = true

                appManager.showAlert(title: "Error", message: jsonDict.value(forKey: "errmsg") as! String
                    , navigationController: self.navigationController!)
            }
        }
    }
    
    
    @IBAction func btnclick_countrycode(_ sender: Any) {
        
        
        DispatchQueue.main.async(execute: {
            
            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "CountryCodeSearchViewController") as! CountryCodeSearchViewController
            self.navigationController?.pushViewController(viewController, animated: true)
            
            
            
            
        })
        

    }
    
    
    // Mark: Creating Random No For Password
    func generateRandomDigits(_ digitNumber: Int) -> String {
        var number = ""
        for i in 0..<digitNumber {
            var randomNumber = arc4random_uniform(10)
            while randomNumber == 0 && i == 0 {
                randomNumber = arc4random_uniform(10)
            }
            number += "\(randomNumber)"
        }
        return number
    }
 
   
}
