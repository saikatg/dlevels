//
//  OnboardingRiskViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 30/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class OnboardingRiskViewController: UIViewController {
    
    @IBOutlet weak var vwRiskView: UIView!
    @IBOutlet weak var vw30To50PercentView: UIView!
    @IBOutlet weak var vwAbove50PercentView: UIView!
    
    var onBoardingText = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        
        if ViewToHighLight.vwRiskView == "vw30To50PercentView" {
            
            self.vwAbove50PercentView.backgroundColor    =   UIColor.white
            self.vw30To50PercentView.backgroundColor   =   UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
            
        } else if ViewToHighLight.vwRiskView == "vwAbove50PercentView" {
            
            self.vw30To50PercentView.backgroundColor     =   UIColor.white
            self.vwAbove50PercentView.backgroundColor   =   UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
            
        } else {
            
            self.vwAbove50PercentView.backgroundColor    =   UIColor.white
            self.vw30To50PercentView.backgroundColor     =   UIColor.white
        }
        
    }
    
    @IBAction func onClick30To50percent(_ sender: UIButton) {
        
        // 1|Experienced,2|50+,3|1-3)
        User.OnboardinString                        =   "\(User.OnboardinString),2|30-50"//"1|Begginner,2|30-50"
        self.vwAbove50PercentView.backgroundColor    =   UIColor.white
        self.vw30To50PercentView.backgroundColor   =   UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
        ViewToHighLight.vwRiskView      = "vw30To50PercentView"
        pushViewToNextViewController()
        
        
    }
    
    @IBAction func onClickAbove50percent(_ sender: UIButton) {
        
        User.OnboardinString                =   "\(User.OnboardinString),2|50+"
        self.vw30To50PercentView.backgroundColor     =   UIColor.white
        self.vwAbove50PercentView.backgroundColor   =   UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
        ViewToHighLight.vwRiskView      = "vwAbove50PercentView"
        
        pushViewToNextViewController()
        
    }
    
    
    func pushViewToNextViewController() {
        
        print("User Onboarding Data is \(User.OnboardinString)")
        
        DispatchQueue.main.async {
                
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "OnboardingInvestmentViewController") as! OnboardingInvestmentViewController
                self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func pushViewAfterSkipButtonPressed() {
        
        let webServiceCall = WebService()
        let appManager     = AppManager()
        
        let param = "1|,2|,3|"
        
        let parameter   =   "values=\(param)" //values=\(User.OnboardinString)"
        //print("User token is:\(User.token)")
        
        webServiceCall.callWebServices(url: Urls.onBoarding, methodName: "POST", parameters: parameter, istoken: true, tokenval: User.token) { (success, jsonData) in
            
            //print(jsonData)
            if jsonData.value(forKey: "errmsg") as! String == "" {
                //print("User Onboarding Data is \(User.OnboardinString)")
                DispatchQueue.main.async {
                    
                        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
                        self.navigationController?.pushViewController(viewController, animated: true)
                    
                }
                
            }else {
                
                DispatchQueue.main.async {
                    
                    // self.btnDone.isEnabled = true
                    if let msg = jsonData.value(forKey: "errormsg") {
                        appManager.showAlert(title: "Error", message: "\(msg)", navigationController: self.navigationController!)
                    }
                    
                }
            }
        }
        
    }
    
    
    
    @IBAction func onClickBackButton(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnskip(_ sender: Any) {
        User.OnboardinString    = "\(User.OnboardinString)\(onBoardingText)"
        pushViewAfterSkipButtonPressed()
    }
    
    
    @IBAction func onClickNextButton(_ sender: UIButton) {
        // 1|Experienced,2|50+,3|1-3)
        User.OnboardinString = "\(User.OnboardinString),2|\"\""
        pushViewToNextViewController()
    }
    
}
