//
//  WebinarsViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 25/05/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit


class CustomWebinarCell: UITableViewCell {
    
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnVideoLink: UIButton!
    @IBOutlet weak var lblDetails: UILabel!
}
class WebinarsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tableViewHeightConstant: NSLayoutConstraint!
    
    var isShowingMenu = false
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    var videodata         =     [NSDictionary]()
    var videolinkdata         =    [String]()
  
    @IBOutlet weak var btnHamburger: UIButton!
    
    
    @IBOutlet weak var webinartbl: UITableView!
    @IBOutlet weak var bottomScrollView: UIScrollView!
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadVideoData()
        addLoader()        
        webinartbl.tableFooterView = UIView()
        //bottomScrollView.contentSize.height = 900
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        //webinartbl.estimatedRowHeight = 1000
        //webinartbl.rowHeight = UITableViewAutomaticDimension
        
        
        //let range = Range(uncheckedBounds: (lower: 0, upper: self.webinartbl.numberOfSections))
       // self.webinartbl.reloadSections(IndexSet(integersIn: range), with: .none)
        
    }
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tapAction(sender: UITapGestureRecognizer) {
        
        instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
        btnHamburger.frame.origin.x -= 250
        isShowingMenu = false
    }
   
    @IBAction func searchClick(_ sender: Any) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    //MARK": Getting Data for table.
    func loadVideoData()
    {
        
         self.actInd.startAnimating()
            
            let obj = WebService()
            let paremeters = "id=0&type=Webinar"
            
            obj.callWebServices(url: Urls.webinar_video, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
                
                //print("Json Data is :  \(jsonData)")
                
                if jsonData.value(forKey: "errmsg") as! String == "" {
                   
                    
                  self.videodata.removeAll()
                  self.videodata = jsonData.value(forKey: "response") as! [NSDictionary]
                 
                    
                    for dict in self.videodata {
                       
                        self.videolinkdata.append(dict.value(forKey: "vl_Link") as! String)
                    }
                    print("VIDEOLOAD \(self.videolinkdata)")
                    
                    DispatchQueue.main.async(execute: {
                        //Thread.current.cancel()
                       
                        self.webinartbl.reloadData()
                        self.tableViewHeightConstant.constant = CGFloat(self.videodata.count * 250)
                        self.actInd.stopAnimating()
                        
                    })
                }
                else
                {
                    
                    DispatchQueue.main.async {
                        self.actInd.stopAnimating()
                        let alertobj = AppManager()
                          alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                    }
                }
            }
    }

    //MARK: Table View Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"webinarcell", for: indexPath) as! CustomWebinarCell
        cell.selectionStyle = .none
     
        cell.lblDate.text = self.videodata[indexPath.row].value(forKey: "vl_Date") as? String
        cell.lblDetails.text = self.videodata[indexPath.row].value(forKey: "vl_Header") as? String
        cell.btnVideoLink.tag = indexPath.row
        cell.btnVideoLink.addTarget(self, action:#selector(playvideo(sender:)), for: UIControlEvents.touchUpInside)
        let imageval = self.videodata[indexPath.row].value(forKey: "vl_Image_Path") as? String

        let urlpath = "https://www.dynamiclevels.com/wp-content/themes/dynamiclevels/v201503300003/dlapp/images/android/"+imageval!
        //cell.imgVideo.addCornerRadius(value: 12)
        cell.imgVideo.downloadedFrom(link: urlpath)
        
        return cell
    }
    
    func playvideo(sender:UIButton) {
        //let buttonRow = sender.tag
        print(videolinkdata[sender.tag])
        UIApplication.shared.openURL(NSURL(string: videolinkdata[sender.tag])! as URL)
    }
    
  
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videodata.count
    }
    

    // MARK: - Navigation
    @IBAction func onClickHambergerIcon(_ sender: UIButton) {
        
        if !isShowingMenu {
            
            //addGestureRecogniser()
            
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x += 250
            isShowingMenu = true
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x -= 250
            isShowingMenu = false
        }
    }

}
