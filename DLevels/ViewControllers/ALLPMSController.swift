//
//  ALLPMSController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 07/11/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class ALLPMSController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnHamburger: UIButton!
    var pageMenu : CAPSPageMenu!
    var controllerArray : [UIViewController] = []
    
    var dictValue:   [NSDictionary] = []
    var dataArr:     [NSDictionary] = []
    
    var isShowingMenu = false
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addingAllView()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func searchClick(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    
    
    func addingAllView() {
        
        let pms1instance = self.storyboard!.instantiateViewController(withIdentifier: "PMS1Container") as! PMS1Container
        pms1instance.title = "Why Dynamic PMS"
        controllerArray.append(pms1instance)
        
        let pms2instance = self.storyboard!.instantiateViewController(withIdentifier: "PMS2Container") as! PMS2Container
        //   pastOrder2Controller.nav = self.navigationController!
        pms2instance.title = "What is Dynamic PMS"
        controllerArray.append(pms2instance)
        
        let pms3instance = self.storyboard!.instantiateViewController(withIdentifier: "PMS3Container") as! PMS3Container
        //   pastOrder2Controller.nav = self.navigationController!
        pms3instance.title = "How to Invest"
        controllerArray.append(pms3instance)
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: containerView.frame.width, height: containerView.frame.height), pageMenuOptions: nil)
        containerView.backgroundColor = UIColor.white
        containerView.addSubview(pageMenu!.view)
    }
    
    
    @IBAction func btnBack_Action(_ sender: UIButton) {
        if !isShowingMenu {
            
            //addGestureRecogniser()
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x += 250
            isShowingMenu = true
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x -= 250
            isShowingMenu = false
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
