//
//  PortfolioCheckupViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 07/06/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class PortfolioCheckupViewController: UIViewController {

    @IBOutlet weak var lblContainer2Text: UILabel!
    @IBOutlet weak var lblContainer3Text: UILabel!
    
    @IBOutlet weak var btnHamburger: UIButton!
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    var isShowingMenu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

   
        self.lblContainer2Text.text = "For Portfolios above 25 lacs, get an exclusive opportunity to have a one to one discussion with Mr Shailesh Saraf, MD of Dynamic Group of Companies,  who has 24 years of experience in Financial markets. \n \n You will get the following reports with your Portfolio check up \n  \n • Your portfolio sector analysis. \n • Fundamental & technical analysis for all your Portfolio stocks. \n • List of extensively researched stocks to buy. \n • Analysis of the best performing sectors. \n • Know how to identify multibaggers stocks from a pool of 1800 NSE stocks."
        
        self.lblContainer3Text.text = "You can also join our product Portfolio Management Services. \n Click to Know details."
    
    }
    @IBAction func btnClickKnowDetails(_ sender: Any) {
        
         UIApplication.shared.openURL(NSURL(string: "https://www.dynamiclevels.com/en/invest-with-us/pms")! as URL)
        
    }
    
    @IBAction func searchClick(_ sender: Any) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    @IBAction func btnClickContacyUs(_ sender: Any) {
        
        let url = URL(string: "telprompt://8336087004")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }

    }
    
    @IBAction func btnHamburger(_ sender: Any) {
        
        if !isShowingMenu {
            
            //addGestureRecogniser()
            
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x += 250
            isShowingMenu = true
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x -= 250
            isShowingMenu = false
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
