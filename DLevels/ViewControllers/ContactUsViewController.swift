//
//  ContactUsViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 11/02/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit
import MessageUI
import CoreLocation
import MapKit

class ContactUsViewController: UIViewController,MFMailComposeViewControllerDelegate, MKMapViewDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var vwmap: MKMapView!
    @IBOutlet weak var btnHamberger: UIButton!
    
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    var isShowingMenu = false
    
    var locationManager:CLLocationManager!
    var mapView:MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vwmap.delegate = self
        vwmap.isZoomEnabled = true
        vwmap.isScrollEnabled = true
    }
    
    @IBAction func btnCall(_ sender: Any) {
        
        let url = URL(string: "telprompt://8336087004")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func searchClick(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    @IBAction func btnEmail(_ sender: Any) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppManager().setStatusBarBackgroundColor()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let annotation = MKPointAnnotation()
        
        let spandata = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region  = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 22.579684, longitude: 88.437881), span: spandata)
        
        vwmap.setRegion(region, animated: true)
        
        annotation.title = "DLevels"
        annotation.coordinate = CLLocationCoordinate2D(latitude: 22.579684, longitude: 88.437881)
        vwmap.addAnnotation(annotation)
    }
    
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["support@dynamiclevels.com"])
        mailComposerVC.setSubject("Email Subject")
        mailComposerVC.setMessageBody("Type Email Massage Here...", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        
        let appManager = AppManager()
        appManager.showAlert(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", navigationController: self.navigationController!)
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
    }
   
    
    @IBAction func onClickOfLinkedInButton(_ sender: UIButton) {
        
        openUrl(url: "https://www.linkedin.com/company/dynamic-levels")
    }
    
    @IBAction func onClickOfFacebookButton(_ sender: UIButton) {
        openUrl(url: "https://www.facebook.com/DynamicLevels/?business_id=775275362545133")
    }
    
    @IBAction func onClickOfTwitterButton(_ sender: UIButton) {
        openUrl(url: "https://twitter.com/dynamiclevels")
    }
    
    @IBAction func onClickOfGooglePlusButton(_ sender: UIButton) {
        openUrl(url: "https://plus.google.com/+Dynamiclevels")
    }
    
    @IBAction func onClickOfYoutubeButton(_ sender: UIButton) {
        openUrl(url: "https://www.youtube.com/channel/UCuZWu6Y1-DDpgGo7alNSovg")
    }
    
    func openUrl(url: String){
        
        let urlString = URL(string: url) //NSURL(string: url)
        //UIApplication.shared.openURL(urlString!)
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(urlString!, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
        
        
        
      //  UIApplication.shared.canOpenURL(urlString as! URL)
        
    }
    
    
    @IBAction func onClickMultibaggerAction(_ sender: UIButton) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerLandingViewController") as! MultibaggerLandingViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func onClickDashboardAction(_ sender: UIButton) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    // MARK: Adding Gesture Recogniser For Hiding Menu on tap any where
    func addGestureRecogniser(){
        //let  tapGesture = UITapGestureRecognizer(target: self, action: #selector(MultibaggerLandingViewController.tapAction(sender:)))
       // self.view.addGestureRecognizer(tapGesture)
        
        let  swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(MultibaggerLandingViewController.tapAction(sender:)))
        swipeGesture.direction = .left
        self.view.addGestureRecognizer(swipeGesture)
    }
    
    //MARK: GESTURE RECONGISER METHODE
    func tapAction(sender: UITapGestureRecognizer) {
        
        instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
        btnHamberger.frame.origin.x -= 250
        isShowingMenu = false
    }
    
    
    @IBAction func onClick_Hamberger(_ sender: UIButton) {
        if !isShowingMenu {
            addGestureRecogniser()
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            btnHamberger.frame.origin.x += 250
            isShowingMenu = true
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            btnHamberger.frame.origin.x -= 250
            isShowingMenu = false
        }
    }
    
}
