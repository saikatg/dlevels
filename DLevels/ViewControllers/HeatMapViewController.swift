//
//  HeatMapViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 15/02/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class HeatMapViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectioViewTopConstant: NSLayoutConstraint!
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var accordingvw: UITextView!
    
    @IBOutlet weak var btn_accor_arrow: UIButton!
    @IBOutlet weak var btnHeader: UILabel!
    @IBOutlet weak var clvHeatMapData: UICollectionView!
    
    var daywiseArray = [NSDictionary]()
    var isTapped = false
    
    @IBOutlet weak var topScrollView: UIScrollView!
    @IBOutlet weak var btnHamberger: UIButton!
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    var isShowingMenu = false
    
    var typeOfMutibagger = ""
    
    let names   = ["Multibagger List","Gainers/Losers","Sector Performance","Year Wise Multibagger"]
    let hint    = ["Multibagger List","Gainers/Losers","Sector Performance","Year Wise Multibagger"]
    var highlightcolor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        
        collectioViewTopConstant.constant = 50
        self.btnInfo.imageEdgeInsets = UIEdgeInsetsMake(0, self.btnInfo.frame.size.width - 20 , 0, 0)
        if MultibaggerType.typeOfMultibagger == MultibaggerType.midCap {btnHeader.text = "Small Cap/Mid Cap Multibaggers" } else { btnHeader.text = "Large Cap Multibaggers"}
        getHeatmapValue()
        addLoader()
        actInd.startAnimating()
        prepareScrollView()
        
        topScrollView.setContentOffset(CGPoint(x: 120, y: 0), animated: false)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = false // or false to disable rotation

        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func info(_ sender: Any) {


        if !isTapped{
           
            isTapped = true
            
            UIView.animate(withDuration: 1, animations: {
                self.btnInfo.transform = CGAffineTransform(scaleX: 1, y: -1);
            })

            UIView.animate(withDuration: 1, animations: {
                self.collectioViewTopConstant.constant += 75
                
            })
            
        } else {
            
            isTapped = false
            UIView.animate(withDuration: 1, animations: {
                self.btnInfo.transform = CGAffineTransform(scaleX: 1, y: 1);
            })

            UIView.animate(withDuration: 1, animations: {
            self.collectioViewTopConstant.constant -= 75
             
            })
        }
      
    }
    
    func addLoader() {
        
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    
    func getHeatmapValue() {
        
        
        var arrdata = [NSDictionary]()
        
        
        self.actInd.startAnimating()
        
        if MultibaggerType.typeOfMultibaggerForHeatMap == "MLT-MID"
        {
            arrdata = MultibaggerStats.multibaggersmallcapgainerlooserdata
        }
        else
        {
            arrdata = MultibaggerStats.multibaggerlargecapgainerlooserdata
        }
        

        
        
        
        
        if(arrdata.count == 0)
        {
        
        let objWebService = WebService()
        
        let parameters = "segment=\(MultibaggerType.typeOfMultibaggerForHeatMap)"
        
        // Token Value for test
        objWebService.callWebServices(url: Urls.multibagger_gainers, methodName: "GET", parameters: parameters, istoken: true, tokenval: User.token) { (success, jsonResult) in
            
            print("Json data in Heatmap Page:  \(jsonResult)")
            if jsonResult.value(forKey: "errmsg") as! String == "" {
                self.daywiseArray = jsonResult.value(forKey: "response") as! [NSDictionary]
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    
                    
                    if MultibaggerType.typeOfMultibaggerForHeatMap == "MLT-MID"
                    {
                        MultibaggerStats.multibaggersmallcapgainerlooserdata = self.daywiseArray
                    }
                    else
                    {
                        MultibaggerStats.multibaggerlargecapgainerlooserdata = self.daywiseArray
                    }
                    
                    
                    self.clvHeatMapData.reloadData()
                }
            }
            }
        }
        else
        {
            
            
            if MultibaggerType.typeOfMultibaggerForHeatMap == "MLT-MID"
            {
                self.daywiseArray = MultibaggerStats.multibaggersmallcapgainerlooserdata
            }
            else
            {
                self.daywiseArray = MultibaggerStats.multibaggerlargecapgainerlooserdata
            }
            
            
            DispatchQueue.main.async {
                self.actInd.stopAnimating()
                self.clvHeatMapData.reloadData()
            }
            
        }
        }
    
      // MARK: Adding Scroll View
    private func prepareScrollView() {
        
        topScrollView.showsHorizontalScrollIndicator = true
        var fromLeft: CGFloat = 10
        
        for index in 0..<4 {
            
            let button = UIButton()
            button.contentMode       = .scaleAspectFill
            button.setTitle(names[index], for: .normal)
            button.setTitleColor(UIColor(red: 40/255, green: 76/255, blue: 90/255, alpha: 1), for: .normal)
            button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 18)
            
            button.accessibilityHint = names[index]
            
            button.titleLabel?.lineBreakMode = .byClipping
            button.frame = CGRect(x: fromLeft, y: 0, width: 180, height: 35)
            button.contentHorizontalAlignment = .center
            button.layer.borderColor = UIColor.blue.cgColor
            
            fromLeft +=  180
            button.addTarget(self, action: #selector(DashBoardViewController.buttonTappedInScrollView(sender:)), for: .touchUpInside)
                      button.tag = index
            topScrollView.addSubview(button)
            
            let highlightvw = UIView(frame: CGRect(x: button.frame.origin.x, y: button.frame.origin.y + button.frame.height, width: 180, height: 2.5))
            
            highlightvw.tag = index
            
            //   print("--vvvvvv----\(index)")
            if(index == 1)
            {
                highlightvw.backgroundColor = highlightcolor
                button.titleLabel?.font = UIFont(name: "Helvetica Neue Bold", size: 14)
            }
            else
            {
                highlightvw.backgroundColor = UIColor.white
                button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 14)
            }
            
            topScrollView.addSubview(highlightvw)
            topScrollView.contentSize = CGSize(width: fromLeft, height: 1)
        }
        
        self.view.layoutIfNeeded()
    }

    
  
    

    // MARK: Button Tap IN Scroll View
    func buttonTappedInScrollView(sender: UIButton){
        
       // print(" You have tapped on: \(sender.accessibilityHint)")
        
        if let page = sender.accessibilityHint {
            
            if page == "MULTIBAGGER LIST" || page == "Multibagger List" {
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
                
            } else if page == "Sector Performance" || page == "SECTOR PERFORMANCE" {
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerSectorViewController") as! MultibaggerSectorViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
            } else if page == "Year Wise Multibagger" {
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "PastPerformenceViewController") as! PastPerformenceViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
            }else if page == "Gainers/Losers" || page == "Gainers/Losers"{
                
                DispatchQueue.main.async {
                    let viewCont = self.storyboard?.instantiateViewController(withIdentifier: "HeatMapViewController") as! HeatMapViewController
                    self.navigationController?.pushViewController(viewCont, animated: false)
                }
                
            }
        }
    }
    
    
    //MARK: Collection View Delegate Methode
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return daywiseArray.count
    }
     public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
     {
        SearchForCurrentMultibagger.serchValue = daywiseArray[indexPath.row].value(forKey: "Symbol") as! String
        
        SearchForCurrentMultibagger.instrument_4 = daywiseArray[indexPath.row].value(forKey: "INSTRUMENT_4") as! String
        SearchForCurrentMultibagger.viewcontrollername = "Search"
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "StockSpecificNewViewController") as! StockSpecificNewViewController
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    //MARK: Collection View Delegate Methode
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        let lblName = cell.viewWithTag(1000) as! UILabel
        lblName.text = "\(daywiseArray[indexPath.row].value(forKey: "INSTRUMENT_4") as! String)"
        //lblName.adjustsFontSizeToFitWidth = true
        
//        let lblSector = cell.viewWithTag(1001) as! UILabel
//        lblSector.text = "  \(daywiseArray[indexPath.row].value(forKey: "INSTRUMENT_4") as! String)"
        
        let lblLTP = cell.viewWithTag(1001) as! UILabel
        lblLTP.text = "\(daywiseArray[indexPath.row].value(forKey: "Ltp") as! String)"
        
        let lblPercentage = cell.viewWithTag(1002) as! UILabel
        
        
        let percentageDoubleValue = daywiseArray[indexPath.row].value(forKey: "Percent") as! String
        
        
        if Double(percentageDoubleValue)! >= 4.0 {
            
            cell.backgroundColor = UIColor(red: 29/255, green: 204/255, blue: 146/255, alpha: 1)
        }
            
        else if Double(percentageDoubleValue)! < 4 && Double(percentageDoubleValue)! >= 3
        {
            cell.backgroundColor = UIColor(red: 105/255, green: 221/255, blue: 183/255, alpha: 1)
        }
            
            
        else if Double(percentageDoubleValue)! < 3 && Double(percentageDoubleValue)! >= 2
        {
            cell.backgroundColor = UIColor(red: 156/255, green: 233/255, blue: 208/255, alpha: 1)
        }
            
        else if Double(percentageDoubleValue)! < 2 && Double(percentageDoubleValue)! >= 1
        {
            cell.backgroundColor = UIColor(red: 207/255, green: 244/255, blue: 232/255, alpha: 1)
        }
        else if Double(percentageDoubleValue)! < 1 && Double(percentageDoubleValue)! > 0
        {
            cell.backgroundColor = UIColor(red: 232/255, green: 252/255, blue: 246/255, alpha: 1)
        }
            
            
            
        else if Double(percentageDoubleValue) == 0.0
        {
            cell.backgroundColor = UIColor.white
        }
        else if Double(percentageDoubleValue)! < 0 && Double(percentageDoubleValue)! > -1
        {
            cell.backgroundColor = UIColor(red: 255/255, green: 235/255, blue: 234/255, alpha: 1)
        }
            
        else if Double(percentageDoubleValue)! < -1 && Double(percentageDoubleValue)! > -2
        {
            cell.backgroundColor = UIColor(red: 255/255, green: 212/255, blue: 210/255, alpha: 1)
        }
        else if Double(percentageDoubleValue)! < -2 && Double(percentageDoubleValue)! > -3
        {
            cell.backgroundColor = UIColor(red: 255/255, green: 171/255, blue: 168/255, alpha: 1)
        }
        
        else if Double(percentageDoubleValue)! < -3 && Double(percentageDoubleValue)! > -4
        {
            cell.backgroundColor = UIColor(red: 255/255, green: 121/255, blue: 117/255, alpha: 1)
        }
        
        else if Double(percentageDoubleValue)! < -4 {
            cell.backgroundColor = UIColor(red: 255/255, green: 67/255, blue: 61/255, alpha: 1)
        }
        
        if(Double(percentageDoubleValue)! > 0.00)
        {
            lblPercentage.text = "+\(daywiseArray[indexPath.row].value(forKey: "Percent") as! String)%"
        }
        else
        {
            lblPercentage.text = "\(daywiseArray[indexPath.row].value(forKey: "Percent") as! String)%"
        }
      
        return cell
    }
    
    //MARK: Collection View Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
    return CGSize(width: clvHeatMapData.bounds.width / 3  - 3, height: 90)
    
      //  return CGSize(width: UIScreen.main.bounds.width / 3 - 20, height: 90)
    }
    
    
    @IBAction func btnSearchAction(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    //MARK: Back Button Action
    @IBAction func addHamburgerMenu(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerLandingViewController") as! MultibaggerLandingViewController
            self.navigationController?.pushViewController(viewController, animated: false)
        }
        
    }
}
