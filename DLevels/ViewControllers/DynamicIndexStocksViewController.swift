//
//  DynamicIndexStocksViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 07/10/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class DynamicIndexStocksViewController: UIViewController, UITableViewDataSource,UITableViewDelegate  {

    @IBOutlet weak var tblvwCompany: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    var dataArr:     [NSDictionary] = []
    var indextype: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = false
        getData()
        lblTitle.text = "Dynamic \(indextype)cap  Index"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Getting Data
    func getData()
    {
        let obj = WebService()
        obj.callWebServices(url: Urls.indexstock, methodName: "GET", parameters: "type=\(indextype)", istoken: false, tokenval: "") { (returnValue, jsonData) in
            //print(jsonData)
            if jsonData.value(forKey: "errmsg") as! String == "" {
                let respDic = jsonData.value(forKey: "response") as! NSArray
                for dict in respDic {
                    let dictValue = dict as! NSDictionary
                    self.dataArr.append(dictValue)
                }
                 DispatchQueue.main.async {
                let sectionIndex = IndexSet(integer: 0)
                    self.tblvwCompany.reloadData()
                    //print(self.dataArr)
                }
            }
        }
    }
    
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"tickercell", for: indexPath)
        cell.selectionStyle = .none
        
        cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
        
        let lblCompany = cell.viewWithTag(1001) as! UILabel
        lblCompany.text = self.dataArr[indexPath.row].value(forKey: "st_company_name") as? String
        
        let lblsrno = cell.viewWithTag(1002) as! UILabel
        lblsrno.text = String(indexPath.row + 1)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArr.count
    }
    
    @IBAction func btnBack(_ sender: Any) {        
        self.navigationController?.popViewController(animated: true)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
