//
//  LoginViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 28/12/16.
//  Copyright © 2016 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit
import FBSDKLoginKit
import FBSDKCoreKit


class LoginViewController: UIViewController, UITextFieldDelegate, GIDSignInDelegate, GIDSignInUIDelegate{
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var x = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardOnTapOutside()
        
        // Activity Indicator For Temporary
        
        addLoader()
        
        txtEmail.delegate = self
        txtPassword.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    
    
    
        
        
        
       //txtEmail.text = "sudhanshubharti25@gmail.com"
        //txtPassword.text = "password"
        
//        UserDefaults.standard.set(nil, forKey: "phone")
//        UserDefaults.standard.set(nil, forKey: "email")
//        UserDefaults.standard.set(nil, forKey: "password")
//        UserDefaults.standard.set(nil, forKey: "regThrough")
        
        //print(GIDSignIn.sharedInstance().currentUser)
        
        //GIDSignIn.sharedInstance().signOut()
        //  FBSession.activeSession().closeAndClearTokenInformation()
        ///   let loginmamager =
        // FBSDKLoginManager.logOut(<#T##FBSDKLoginManager#>)
        
        //  let loginManager = FBSDKLoginManager()
        //  loginManager.logOut()
        //  FBSDKLoginManager().logOut()
        
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        if((FBSDKAccessToken.current() != nil))
        {
            
        }
        
        GIDSignIn.sharedInstance().signInSilently()
        
        self.title = "Let me test this once."
        
    }
    
    func keyboardWillShow(sender: NSNotification) {
        
        if(x == 0)
        {
            self.view.frame.origin.y -= 50
            x = -50
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        if(x < 0)
        {
            self.view.frame.origin.y += 50
            x = 0
        }
    }
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    // MARK:
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        actInd.stopAnimating()
        btnLogin.isEnabled = true
        
    }
    
    // MARK:
    override func viewDidAppear(_ animated: Bool) {
        actInd.stopAnimating()
        btnLogin.isEnabled = true
        
        let loginmanager=FBSDKLoginManager()
        loginmanager.logOut()
    }
        
    
        
    
    
    @IBAction func forgotPassword_Action(_ sender: UIButton) {
        
        let signUPScreenViewConroller = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(signUPScreenViewConroller, animated: true)
        
        // GIDSignIn.sharedInstance().signOut()
        
    }
    private func prepareScreen() {
        
        print(">>>>>Constriant value >>>>\(Constants.someNotification)")
        
        // Hide keybord on tap outside
        self.hideKeyboardOnTapOutside()
        AppManager().setStatusBarBackgroundColor() // Set status bar color
    }
    
    
    @IBAction func signUpScreen_Action(_ sender: UIButton) {
        
        let signUPScreenViewConroller = self.storyboard?.instantiateViewController(withIdentifier: "SignUPScreenViewController") as! SignUpViewController
        self.navigationController?.pushViewController(signUPScreenViewConroller, animated: true)
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error == nil {
            
            // ***
            User.firstName       =   user.profile.givenName
            User.lastName        =   user.profile.familyName
            User.email           =   user.profile.email
            User.socialId        =   user.userID
            User.password        =   user.authentication.idToken
            print(" access token is : \(user.authentication.idToken)")
            User.regThrough     = RegThrough.gmail
            // var idToken  = GIDGoogleUser.getauto
            print("Acess token is:  \(user.authentication.accessToken)")
            print(" User Id:  \(user.userID)")
         //    User.firstName
            
            User.socialToken    =   user.authentication.idToken
            
              print(" User name:  \( User.firstName)")
            performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
            
            
        } else{
            print(error.localizedDescription)
        }
    }
    
    
    // MARK: UITextField Delegate Methode to return keyboard on return key press
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
//    func isEmailExist(email : String, regThrough: String) {
//       actInd.startAnimating()
//        let webServiceObj = WebService()
//        
//        webServiceObj.callWebServices(url: Urls.accountStatus, methodName: "POST", parameters: "email=\(email)", istoken: false, tokenval: "") { (success, returnData) in
//            
//            print("Return data is: \(returnData)")
//            
//        }
//        
//    }
    
    // Mark : Checking Existance Of Email
    func isEmailExist(email : String, regthrough: String) {
        actInd.startAnimating()
        
        // self.view.backgroundColor=UIColor.red;
        let webService = WebService()
        let parameters = "email=\(email)"
        
        webService.callWebServices(url: Urls.accountStatus, methodName: "POST", parameters: parameters, istoken: false, tokenval: "") { (status, response) in
            print("Response is:  \(response)")
            
            if response.value(forKeyPath: "errmsg") as! String == ""{
                
                if(regthrough == "gmail")
                {
                    self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
                }
                
                if(regthrough == "facebook")
                {
                    self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
                }
                
                
            }
            else
            {
                
                
                if(regthrough == "gmail")
                {
                    
                    DispatchQueue.main.async(execute: {
                        
                        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "PhoneValidationViewController") as! PhoneValidationViewController
                        self.navigationController?.pushViewController(viewController, animated: true)
                    })
                    
                    
                }
                
                if(regthrough == "facebook")
                {
                    
                    DispatchQueue.main.async {
                        
                        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "PhoneValidationViewController") as! PhoneValidationViewController
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                  
                }
            }
        }
        
        
          actInd.stopAnimating()
    }
    

    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Login Function
    @IBAction func loginAction(_ sender: UIButton) {
        
        actInd.startAnimating()
        User.regThrough = RegThrough.normal
        User.email = txtEmail.text!
        User.password = txtPassword.text!
        
        if  Reachability.isInternetAvailable() {
            
            self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
            
            
            
        } else {
            
            let appManager = AppManager()
            appManager.showAlert(title: "Error", message: "No Internet Access!", navigationController: self.navigationController!)
        }
    }
    
    //MARK: Performing login Action
    func performLogin(email : String, password: String, regThrough: String) {
        
        let appManager = AppManager()
        let webService = WebService()
        
        let parameters = "user_email=\(email)&password=\(password)&logthrough=\(regThrough)"
        
        if appManager.isValidEmail(email: email) {
            
//            webService.callWebServices(url: Urls.login, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
//              
            webService.callWebServices(url: Urls.login_without_emailverification, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
                
                print("Login Return Result: \(dictData)  \n   Wait here...")
                
                UserDefaults.standard.set(User.email, forKey: "email")
                UserDefaults.standard.set(User.password, forKey: "password")
                UserDefaults.standard.set(User.regThrough, forKey: "regThrough")
                
                if dictData.value(forKey: "error") as! NSInteger == 216
                {
                    let errDict = dictData.value(forKey: "errmsg") as! NSDictionary
                    if errDict.value(forKey: "phone") as! Int == 0 {
                        self.actInd.stopAnimating()
                        DispatchQueue.main.async(execute: {
                            
                            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "PhoneValidationViewController") as! PhoneValidationViewController
                            viewController.isLogin = true
                            
                            self.navigationController?.pushViewController(viewController, animated: true)
                            
                        })
                        
                    }
                    else if errDict.value(forKey: "email") as! Int == 0
                    {
                        
                        self.actInd.stopAnimating()
                        DispatchQueue.main.async(execute: {
                            self.verifyEmail(email: User.email, phone: "")
                        })
                    }
                    
                }
                else if dictData.value(forKey: "error") as! NSInteger == 0
                {
                    self.actInd.stopAnimating()
                    
                    
                    //print(dictData)
                    let responce = dictData.value(forKey: "response") as! NSDictionary
                    User.token = responce.value(forKey: "token") as! String
                    self.getUser()
                    
                    
//                    if responce.value(forKey: "onboarding") as! Int == 1 {
//                        DispatchQueue.main.async(execute: {
//
//                            User.token = responce.value(forKey: "token") as! String
//
//                            self.getUser()
//                        })
//                    } else {
//
//                        DispatchQueue.main.async(execute: {
//
//                            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
//                            self.navigationController?.pushViewController(viewController, animated: true)
//                        })
//                    }
                    
                }
                    
                 else if dictData.value(forKey: "error") as! NSInteger == 211
                {
                    
                    UserDefaults.standard.set("", forKey: "email")
                    UserDefaults.standard.set("", forKey: "password")
                    UserDefaults.standard.set("", forKey: "regThrough")
                    
                    
                    DispatchQueue.main.async(execute: {
                        self.actInd.stopAnimating()
                        
                        appManager.showAlert(title: "Error", message: "Wrong EmailId/Password!", navigationController: self.navigationController!)
                    })
                    
                    
                    if(User.regThrough == "gmail")
                    {
                        
                        DispatchQueue.main.async(execute: {
                            
                            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "PhoneValidationViewController") as! PhoneValidationViewController
                            viewController.isLogin = false
                            
                            self.navigationController?.pushViewController(viewController, animated: true)
                            
                        })
                    }
                }
                    
                else
                {
                    let appManager = AppManager()
                    
                    DispatchQueue.main.async(execute: {
                        self.actInd.stopAnimating()
                        
                    })
                    
                    UserDefaults.standard.set("", forKey: "email")
                    UserDefaults.standard.set("", forKey: "password")
                    UserDefaults.standard.set("", forKey: "regThrough")
                    
                    if(User.regThrough == "gmail")
                    {
                        GIDSignIn.sharedInstance().signOut()
                    }
                    
                    appManager.showAlert(title: "Error", message: dictData.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
                }
            })
            
        }
        else
        {
            
            self.actInd.stopAnimating()
            
            appManager.showAlert(title: "Error", message: "Email Id Not Valid! \n ", navigationController: self.navigationController!)
        }
    }
    
    func multibaggerstatload()
    {
        let obj = WebService()
        let paremeters = ""
        
        self.actInd.startAnimating()
        
        obj.callWebServices(url: Urls.multibaggerStatus, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                print(jsonData)
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                MultibaggerStats.segQtrDateArr.removeAll()
                MultibaggerStats.segQtrDisplayArr.removeAll()
                MultibaggerStats.segYearDateArr.removeAll()
                MultibaggerStats.segYearDisplayArr.removeAll()
                MultibaggerStats.qutrniftyindex.removeAll()
                MultibaggerStats.qutrsmallcapindex.removeAll()
                MultibaggerStats.yearniftyindex.removeAll()
                MultibaggerStats.yearsmallcapindex.removeAll()
                MultibaggerStats.qutrmidcapindex.removeAll()
                
                
                for arr in tempArray
                {
                    //segQtrDateArr
                    if(arr.value(forKey: "mdt_Rep_Type") as! String == "QUARTER")
                    {
                        MultibaggerStats.segQtrDateArr.append(arr.value(forKey: "mdt_Date") as! String)
                        MultibaggerStats.segQtrDisplayArr.append(arr.value(forKey: "mdt_Display") as! String)
                        
                        if let mrdSmallCapPer = arr.value(forKey: "mrd_SmallCap_Per") as? String {
                            MultibaggerStats.qutrsmallcapindex.append(mrdSmallCapPer)
                        }
                        if let mrdMidCapPer = arr.value(forKey: "mrd_MidCap_Per") as? String {
                            MultibaggerStats.qutrmidcapindex.append(mrdMidCapPer)
                        }
                        if let mrdNiftyPer = arr.value(forKey: "mrd_Nifty_Per") as? String  {
                            MultibaggerStats.qutrniftyindex.append(mrdNiftyPer)
                        }
                        if let mdtPerfDt = arr.value(forKey: "mdt_Perf_Dt") as? String  {
                            MultibaggerStats.qutrperformancetxt.append(mdtPerfDt)
                        }
                    }
                    
                    if(arr.value(forKey: "mdt_Rep_Type") as! String == "YEAR")
                    {
                        MultibaggerStats.segYearDateArr.append(arr.value(forKey: "mdt_Date") as! String)
                        MultibaggerStats.segYearDisplayArr.append(arr.value(forKey: "mdt_Display") as! String)
                        
                        if let mrdSmallCapPer = arr.value(forKey: "mrd_SmallCap_Per") as? String {
                            MultibaggerStats.yearsmallcapindex.append(mrdSmallCapPer)
                        }
                        if let mrdNiftyPer = arr.value(forKey: "mrd_Nifty_Per") as? String {
                            MultibaggerStats.yearniftyindex.append(mrdNiftyPer)
                        }
                        MultibaggerStats.yearperformancetxt.append(arr.value(forKey: "mdt_Perf_Dt") as! String )
                    }
                }
                
                
                
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    self.nrifpipmsload()
                    
                }
            }
        }
    }
    
    func nrifpipmsload()
    {
        let obj = WebService()
        let data = "ALL"
        let param = "ptype=\(data)"
        
        self.actInd.startAnimating()
        
        obj.callWebServices(url: Urls.NRIFPIPMS, methodName: "GET", parameters: param, istoken: false, tokenval: "") { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                // print(jsonData)
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                
                for arr in tempArray
                {
                    
                    if(arr.value(forKey: "p_type") as! String == "PMS")
                    {
                        PMS_DATA.PMS1 = arr.value(forKey: "p_why") as! String
                        PMS_DATA.PMS2 = arr.value(forKey: "p_what") as! String
                        PMS_DATA.PMS3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "NRI")
                    {
                        NRI_DATA.NRI1 = arr.value(forKey: "p_what") as! String
                        NRI_DATA.NRI2 = arr.value(forKey: "p_why") as! String
                        NRI_DATA.NRI3 = arr.value(forKey: "p_how") as! String
                    }
                    
                    if(arr.value(forKey: "p_type") as! String == "FPI")
                    {
                        FPI_DATA.FPI1 = arr.value(forKey: "p_what") as! String
                        FPI_DATA.FPI2 = arr.value(forKey: "p_why") as! String
                        FPI_DATA.FPI3 = arr.value(forKey: "p_how") as! String
                    }
                }
                
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    self.getSeminarAd()
                    
                }
            }
        }
    }
    
    func getSeminarAd(){
        let webServices = WebService()
        webServices.callWebServices(url: Urls.seminar_ad, methodName: "GET", parameters: "", istoken: true, tokenval: User.token) { (success, jsonDict) in
            
            if jsonDict.value(forKey: "errmsg") as! String == "" {
                
                if  let response = jsonDict.value(forKey: "response") as? [NSDictionary] {
                    if let url = response[0].value(forKey: "URL") as? String {
                        seminar_ad.url = url
                        seminar_ad.templete = response[0].value(forKey: "TEMPLATE") as! String
                        if seminar_ad.templete != ""
                        {
                            DispatchQueue.main.async {
                                let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "WebinarAdsViewController") as! WebinarAdsViewController
                                self.navigationController?.pushViewController(viewController, animated: true)
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async{
                                let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
                                self.navigationController?.pushViewController(viewController, animated: true)
                                
                            }
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
    }
    
    func getUser() {
        
        let webServices = WebService()
        webServices.callWebServices(url: Urls.getUser, methodName: "GET", parameters: "", istoken: true, tokenval: User.token) { (success, jsonDict) in
            
            
            if jsonDict.value(forKey: "errmsg") as! String == "" {
                
                let response = jsonDict.value(forKey: "response") as! [NSDictionary]
                
                User.firstName = response[0].value(forKey: "firstName") as! String
                User.lastName = response[0].value(forKey: "lastName") as! String
                User.email = response[0].value(forKey: "emailID") as! String
                User.phone = response[0].value(forKey: "phone") as! String
                
                
                self.getSeminarAd()
                
            } else{
                let appManager = AppManager()
                appManager.showAlert(title: "Error!", message: jsonDict.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
            }
        }
        
        
        
    }
    
    
    func verifyEmail(email: String, phone: String) {
        
        let objWebService = WebService()
        var parameter = ""
        if phone == "" {
            parameter = "email=\(email)"
        } else {
            parameter = "email=\(email)"
        }
        
        objWebService.callWebServices(url: Urls.generateEmailOtp, methodName: "POST", parameters: parameter, istoken: false, tokenval: "") { (success, jsonResult) in
            
            print(jsonResult)   
            
            if jsonResult.value(forKey: "response") as!String == "success" {
                DispatchQueue.main.async(execute: {
                    
                    let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                    viewController.checkotptype = "email"
                    self.navigationController?.pushViewController(viewController, animated: true)
                })
                
            } else {
                
                let appManager = AppManager()
                appManager.showAlert(title: "Error", message: jsonResult.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
            }
            
            
        }
        
    }
    
   
    
    @IBAction func btnFacebookAction(_ sender: UIButton) {
        
   //     let loginManager = FBSDKLoginManager()
   //     loginManager.logOut()
        
        if Reachability.isInternetAvailable() {
            //actInd.startAnimating()
            
            
            
            let fbLoginManger : FBSDKLoginManager = FBSDKLoginManager()
           fbLoginManger.loginBehavior = .systemAccount
         //  fbLoginManger.loginBehavior = FBSDKLoggingBehaviorGraphAPIDebugWarnin
            
            fbLoginManger.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
                if error == nil {
                    
                    let fbLoginResult : FBSDKLoginManagerLoginResult = result!
                    
                    if (result?.isCancelled)! {
                        
                       //  [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
                        print("dssdsd")
                        FBSDKProfile.enableUpdates(onAccessTokenChange: true)
                        fbLoginManger.logOut()
                        return
                        
                        
                    }
                    if fbLoginResult.grantedPermissions.contains("email"){
                        self.getFBUserDate()
                         //fbLoginManger.logOut()
                    }
                    
                     //self.getFBUserDate()
                }
            }
        } else {
            
            let appManager = AppManager()
            appManager.showAlert(title: "Error", message: "No Internet Access!", navigationController: self.navigationController!)
            
        }
    }
    
    func getFBUserDate() {
        
        print(" facebook Token id is:  \(FBSDKAccessToken.current().tokenString)")
        
        
        
        if FBSDKAccessToken.current() != nil {
            
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, name, first_name, last_name, picture.type(large),email"]).start(completionHandler: { (connection, result, error) in
                
                if error == nil {
                    print(result!)
                    
                    
                    let returnResult : [String: Any] = result as! [String : Any]
                    print(returnResult["email"] as! String)
                    
                    User.email          =  returnResult["email"] as! String //email
                    User.firstName      =  returnResult["first_name"] as! String//firstName
                    User.lastName       =  returnResult["last_name"] as! String //lastName
                    User.socialToken    =  FBSDKAccessToken.current().tokenString
                    User.socialId       =  returnResult["id"] as! String
                    User.regThrough     =   RegThrough.facebook
                    
                    User.password =  FBSDKAccessToken.current().tokenString
                    
                    
                    self.isEmailExist(email: User.email, regthrough: User.regThrough)
                    
                   
                }
                
            })
        }
    }
    
    // Mark: Textfield Delgates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtPassword {
          
            if UIScreen.main.bounds.height <= CGFloat(Phone.iPhone_5s) {
            
            UIView.animate(withDuration: 0.2, animations: {
                self.view.frame.origin.y -= 100
            })
            }
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if UIScreen.main.bounds.height <= CGFloat(Phone.iPhone_5s) {
            
        if textField == txtPassword {
            UIView.animate(withDuration: 0.2, animations: {
                self.view.frame.origin.y += 100
            })
        }
            
        }
        
        if !(txtEmail.text == "" && txtPassword.text == "") {
            btnLogin.isEnabled = true
        }
    }
    
    
    @IBAction func btnGoogleAction(_ sender: UIButton) {
        
        
        if Reachability.isInternetAvailable() {
            
            GIDSignIn.sharedInstance().signIn()
            //actInd.startAnimating()
            
        } else {
            
            let appManager = AppManager()
            appManager.showAlert(title: "Error", message: "No Internet Access!", navigationController: self.navigationController!)
            
        }
    }
}
