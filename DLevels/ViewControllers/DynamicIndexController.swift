//
//  DynamicIndexController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 27/09/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class DynamicIndexController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var lbltitle: UILabel!
    
    @IBOutlet weak var lblreturnupdtdt: UILabel!
    
    @IBOutlet weak var ibivolatilitycol2: UILabel!
    @IBOutlet weak var ibivolatilitycol3: UILabel!
    
    
    @IBOutlet weak var lblupdatedate: UILabel!
    @IBOutlet weak var lblwhatis: UILabel!
    @IBOutlet weak var btnHamburger: UIButton!
    @IBOutlet weak var vwAbout: UIView!
    @IBOutlet weak var tblvwSector: UITableView!
    @IBOutlet weak var tblMinor: UITableView!
    
    @IBOutlet weak var tblvwCharacteristics: UITableView!
    @IBOutlet weak var tblvwReturns: UITableView!
    @IBOutlet weak var tblvwStatistics0: UITableView!
    @IBOutlet weak var tblvwStatistics1: UITableView!
    @IBOutlet weak var tblvwfundamental: UITableView!
    
    @IBOutlet weak var imgChart: UIImageView!
    @IBOutlet weak var txtvwMethodololy: UITextView!
    
    @IBOutlet weak var btnSmallCap: UIButton!
    @IBOutlet weak var btnMidCap: UIButton!
    @IBOutlet weak var btnLargeCap: UIButton!
    
    
    var currentButton = "SMALL"
    
    @IBOutlet weak var tblReturnHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var tblPortfolioHeightConstent: NSLayoutConstraint!
    @IBOutlet weak var tblStaticsHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var tblStaticsTwoHeight: NSLayoutConstraint!
    @IBOutlet weak var tblValocityHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var tblSectorMajorHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var tblSectorMinorHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var tblFundamentalHeightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var tblWeightageHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var portfolioCharecteristicHeaderHeightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var btnIndexMethodology: UIButton!
    @IBOutlet weak var portfolioCharDownArrow: UIButton!
    
    @IBOutlet weak var lblTopIndex: UILabel!
    @IBOutlet weak var staticsHeaderViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var lblAnnualizedValocityHeightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var sectorRepresentationHeaderHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var lblSectorRepresentatinMinorHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var topIndexHeaderHeightConstant: NSLayoutConstraint!
    
    //Header Views
    @IBOutlet weak var returnHeaserViewOne: UIView!
    
    
    //DropDown Button Constraints
    @IBOutlet weak var btnPortfolioDropDown: UIButton!
    
    @IBOutlet weak var btnStaticsDropDown: UIButton!
    
    @IBOutlet weak var btnTopIndexDropDown: UIButton!
    
    @IBOutlet weak var btnSectorRepresentation: UIButton!
    
    @IBOutlet weak var tblVolatility: UITableView!
    @IBOutlet weak var tblvwConstituents: UITableView!
    @IBOutlet weak var indexImg: UIImageView!
    @IBOutlet weak var lblLtp: UILabel!
    @IBOutlet weak var lblAsondate: UILabel!
    @IBOutlet weak var lblChangePercent: UILabel!
    
    @IBOutlet weak var txtvwAboutUs: UITextView!
    @IBOutlet weak var btnListOfStocks: UIButton!
    
    // returns columns
    
    @IBOutlet weak var col2: UILabel!
    
    @IBOutlet weak var col3: UILabel!
    
    @IBOutlet weak var col4: UILabel!
    
    @IBOutlet weak var col5
    : UILabel!
    
    @IBOutlet weak var col6: UILabel!
    
    
    
    var descriptionArr:     [NSDictionary] = []
    var charArr:            [NSDictionary] = []
    var returnArr:          [NSDictionary] = []
    var sectorMajorArr:     [NSDictionary] = []
    var sectorMinorArr:     [NSDictionary] = []
    var volatilityArr:      [NSDictionary] = []
    var constituentsArr:    [NSDictionary] = []
    var staticReturnsArr:   [NSDictionary] = []
    var deviationArr:       [NSDictionary] = []
    var fundamentalArr:     [NSDictionary] = []
    var sectorArr:          [NSDictionary] = []
    
    var indexMethodology = ""
    var isShowingMenu = false
    var instanceOfLeftSlideMenu = LeftSlideMenu()
    
    var idxtype = "Small"
    
    // Initialize the Dictionary
    var sectorStaticDataDic = ["col1": "Sector Representation - Major", "col2": ""]
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //255,205,80
        // vwAbout.addCornerRadiusWithColor(value: 10, colorCode: "000000")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = false
        getData(type: "SMALL")
        
        btnSmallCap.titleLabel?.font = UIFont(name: "Helvetica Neue Bold", size: 14)
        btnMidCap.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 12)
        btnLargeCap.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 12)
        
//        self.vwAbout.layer.borderWidth = 1
//        self.vwAbout.layer.borderColor = UIColor.init(red:255/255.0, green:205/255.0, blue:80/255.0, alpha: 1.0).cgColor
        
        addLoader()
    }
    
    //MARK: View Will Apear
    override func viewWillAppear(_ animated: Bool) {
        setDropDownButtonsTag()
        
        returnHeaserViewOne.isHidden    = false
       
    }
    
    //MARK: Seting Buttons Tag
    func setDropDownButtonsTag() {
        
        btnPortfolioDropDown.tag          = 10001
        btnStaticsDropDown.tag            = 10002
        btnSectorRepresentation.tag       = 10003
        btnTopIndexDropDown.tag           = 10004
        btnIndexMethodology.tag           = 10005
        
        self.tblPortfolioHeightConstent.constant = 0
        self.portfolioCharecteristicHeaderHeightConstant.constant = 0
        
        self.tblStaticsHeightConstant.constant = 0
        self.tblStaticsTwoHeight.constant = 0
        self.tblValocityHeightConstant.constant = 0
        self.tblFundamentalHeightConstant.constant = 0
        self.staticsHeaderViewHeightConstant.constant = 0
        self.lblAnnualizedValocityHeightConstant.constant = 0
        self.staticsHeaderViewHeightConstant.constant = 0
        
        self.lblSectorRepresentatinMinorHeightConstant.constant = 0
        self.sectorRepresentationHeaderHeightConstant.constant = 0
        self.tblSectorMajorHeightConstant.constant = 0
        self.tblSectorMinorHeightConstant.constant = 0
        
        self.topIndexHeaderHeightConstant.constant = 0
        self.tblWeightageHeightConstant.constant = 0
        self.lblTopIndex.text = ""
        
        let txt = NSAttributedString(string: "")
        UIView.animate(withDuration: 0.5, animations: {
            self.txtvwMethodololy.attributedText = txt
        })
 
    }
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    //MARK: Getting Data
    func getData(type: String)
    {
        let obj = WebService()
        self.actInd.startAnimating()
        obj.callWebServices(url: Urls.indexfactsheet, methodName: "GET", parameters: "type=\(type)", istoken: false, tokenval: "") { (returnValue, jsonData) in
            
            print(jsonData)
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                
                let respDic             = jsonData.value(forKey: "response") as! NSDictionary
                
                self.charArr            = respDic.value(forKey: "characteristics") as! [NSDictionary]
                self.returnArr          = respDic.value(forKey: "returns") as! [NSDictionary]
                self.sectorMajorArr    = respDic.value(forKey: "srmajor") as! [NSDictionary]
                self.sectorMinorArr           = respDic.value(forKey: "srminor") as! [NSDictionary]
                self.volatilityArr       = respDic.value(forKey: "volatility") as! [NSDictionary]
                self.constituentsArr     = respDic.value(forKey: "constituent") as! [NSDictionary]
                self.descriptionArr     = respDic.value(forKey: "description") as! [NSDictionary]
                self.deviationArr = respDic.value(forKey: "deviation") as! [NSDictionary]
                self.staticReturnsArr = respDic.value(forKey: "staticreturns") as! [NSDictionary]
                self.fundamentalArr = respDic.value(forKey: "fundamental") as! [NSDictionary]
                
                
                //                 print(self.charArr)
                //                 print(self.returnArr)
                //print(self.sectorArr)
                //                 print(self.volatilityArr)
                //print(self.constituentsArr)
                //indexFundamentalCell
                
                DispatchQueue.main.async {
                    
                    if(type == "SMALL")
                    {
                        self.lblwhatis.text = "What is Dynamic Smallcap Index ?"
                        self.ibivolatilitycol2.text = "Dynamic\nSmallcap Index";
                        self.ibivolatilitycol3.text = "NSE\nSmallcap Index";
                    }
                    
                    if(type == "MID")
                    {
                        self.lblwhatis.text = "What is Dynamic Midcap Index ?"
                        self.ibivolatilitycol2.text = "Dynamic\nMidcap Index";
                        self.ibivolatilitycol3.text = "Nifty Full\nMidcap 100 Index";
                    }
                    
                    if(type == "LARGE")
                    {
                        self.lblwhatis.text = "What is Dynamic Largecap Index ?"
                        self.ibivolatilitycol2.text = "Dynamic\nLargecap Index";
                        self.ibivolatilitycol3.text = "NSE\nNifty Index";
                    }
                    
                    self.tblReturnHeightConstant.constant = CGFloat(self.returnArr.count * 45)
                    self.tblvwReturns.reloadSections(IndexSet(integer: 0), with: .automatic)
                    
                  /*  let sectionIndex = IndexSet(integer: 0)
                    self.tblPortfolioHeightConstent.constant = CGFloat(self.charArr.count * 45)
                    self.tblvwCharacteristics.reloadSections(sectionIndex, with: .automatic)
//
                    //Table Return
                    
//                    
                    self.tblvwStatistics0.reloadSections(sectionIndex, with: .automatic)
                    self.tblStaticsHeightConstant.constant = CGFloat(self.staticReturnsArr.count * 45)

                    self.tblvwStatistics1.reloadSections(sectionIndex, with: .automatic)
                    self.tblStaticsTwoHeight.constant = CGFloat(self.deviationArr.count * 45)
//
                    self.tblFundamentalHeightConstant.constant = CGFloat(self.fundamentalArr.count * 45)
                    self.tblvwfundamental.reloadSections(sectionIndex, with: .automatic)
//                    
//                    self.tblSectorRepresentationHeightConstant.constant = CGFloat(self.sectorMajorArr.count * 45)
                    self.tblSectorMajorHeightConstant.constant = CGFloat(self.sectorMajorArr.count * 45)
                    self.tblvwSector.reloadSections(sectionIndex, with: .automatic)
//                    
                    self.tblSectorMinorHeightConstant.constant = CGFloat(self.sectorMinorArr.count * 45)
                    self.tblMinor.reloadSections(sectionIndex, with: .automatic)
//                    
                    self.tblWeightageHeightConstant.constant = CGFloat(self.constituentsArr.count * 45)
                    self.tblvwConstituents.reloadSections(sectionIndex, with: .automatic)
                   
                    self.tblVolatility.reloadSections(sectionIndex, with: .automatic)
                    self.tblValocityHeightConstant.constant = CGFloat(self.volatilityArr.count * 45)
                    */
                    
                    let lastClose  = self.descriptionArr[0].value(forKey: "Last_Close") as? String
                    let lastChange = self.descriptionArr[0].value(forKey: "Last_Change") as! String
                    let percent    = self.descriptionArr[0].value(forKey: "Percent") as! String
                    let asOnDate   = self.descriptionArr[0].value(forKey: "Last_Date") as! String
                    self.lblupdatedate.text   = self.descriptionArr[0].value(forKey: "ind_data_updated_on") as? String
                    self.lblreturnupdtdt.text   = self.descriptionArr[0].value(forKey: "ind_data_updated_on") as? String
                    self.lblreturnupdtdt.text = self.lblreturnupdtdt.text?.replacingOccurrences(of: "Ended", with: "as on ")
                    
                    
//                    self.btnListOfStocks.titleLabel?.text = self.descriptionArr[0].value(forKey: "ind_header2") as! String
//
                    self.btnListOfStocks.setTitle(self.descriptionArr[0].value(forKey: "ind_header2") as! String, for: UIControlState.normal)
                    
                    self.lblLtp.text = lastClose
                    self.lblLtp.sizeToFit()
                    
                    
                    if ((lastChange as NSString).floatValue >= 0)
                    {
                        self.lblChangePercent.text = "+\(lastChange) (+\(percent)%)"
                        self.lblChangePercent.textColor = UIColor(red: 29/255, green: 204/255, blue: 146/255, alpha: 1)
                        self.indexImg.image = #imageLiteral(resourceName: "up")
                        
                    }
                    else
                    {
                        self.lblChangePercent.text = "\(lastChange) (\(percent)%)"
                        self.lblChangePercent.textColor = UIColor.red
                        self.indexImg.image = #imageLiteral(resourceName: "Down")
                        
                    }
                    
                    self.lblChangePercent.sizeToFit()
                    self.lblAsondate.text = "As on \(asOnDate)"
                    self.lblAsondate.sizeToFit()
                    
                    if let url = self.descriptionArr[0].value(forKey: "ind_web_chart_path") as? String{
                        self.imgChart.downloadedFrom(link: url, contentMode: .scaleToFill)
                    }
                    
                    
                     self.indexMethodology = self.descriptionArr[0].value(forKey: "ind_methodology") as! String
                    
                    let attrMethodologyStr = try! NSAttributedString(
                        data: self.indexMethodology.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                        options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                        documentAttributes: nil)
                  //  self.txtvwMethodololy.attributedText = attrMethodologyStr
                    
                    let desc = self.descriptionArr[0].value(forKey: "ind_desc") as! String
                    
                    let attrStr = try! NSAttributedString(
                        data: desc.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                        options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                        documentAttributes: nil)
                    self.txtvwAboutUs.attributedText = attrStr
                    
                     self.actInd.stopAnimating()
                    
                }
                
            }
            else
            {
                self.actInd.stopAnimating()
                
            }
        }
        
        
        }
    
    
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblvwCharacteristics {
            
            return 45
        }
        
        return 45
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        var cell = UITableViewCell()
        
        if(tableView == self.tblvwCharacteristics)
        {
            
            cell = tableView.dequeueReusableCell(withIdentifier:"indexCharCell", for: indexPath)
            cell.selectionStyle = .none
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            
            let lblMethodology = cell.viewWithTag(1001) as! UILabel
            lblMethodology.text = self.charArr[indexPath.row].value(forKey: "methodology") as? String
            
            let lblAlphaBasedWaighting = cell.viewWithTag(1002) as! UILabel
            lblAlphaBasedWaighting.text =  self.charArr[indexPath.row].value(forKey: "alpha_based_waighting") as? String
        }
        
        if(tableView == self.tblvwReturns)
        {
            
            cell = tableView.dequeueReusableCell(withIdentifier:"indexReturnsCell", for: indexPath)
            cell.selectionStyle = .none
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            let lblPeriod = cell.viewWithTag(1001) as! UILabel
            lblPeriod.text = self.returnArr[indexPath.row].value(forKey: "ptp_period") as? String
            
            
            if currentButton != "LARGE" {
            
                let lblDynamicSmallMidCap = cell.viewWithTag(1002) as! UILabel
                lblDynamicSmallMidCap.text =  self.returnArr[indexPath.row].value(forKey: "ptp_small_midcap") as? String
            
                let lblNSESmallCap = cell.viewWithTag(1003) as! UILabel
                lblNSESmallCap.text =  self.returnArr[indexPath.row].value(forKey: "ptp_nse_small_cap") as? String
            
            
                let lblNSEMidCap = cell.viewWithTag(1004) as! UILabel
                lblNSEMidCap.text =  self.returnArr[indexPath.row].value(forKey: "ptp_nse_mid_cap") as? String
            
            
                let lblTopTenSmallCapMF = cell.viewWithTag(1005) as! UILabel
                lblTopTenSmallCapMF.text =  self.returnArr[indexPath.row].value(forKey: "ptp_top_10_small_cap_mf") as? String
            
                let lblNiftyIndex = cell.viewWithTag(1006) as! UILabel
                lblNiftyIndex.text =  self.returnArr[indexPath.row].value(forKey: "ptp_nifty_index") as? String
            
            }
            else
            {
                let lblDynamicSmallMidCap = cell.viewWithTag(1002) as! UILabel
                lblDynamicSmallMidCap.text =  ""
                
                let lblNSESmallCap = cell.viewWithTag(1003) as! UILabel
                lblNSESmallCap.text =  ""
                
                
                let lblNSEMidCap = cell.viewWithTag(1004) as! UILabel
                lblNSEMidCap.text =  self.returnArr[indexPath.row].value(forKey: "ptp_small_midcap") as? String
                
                
                let lblTopTenSmallCapMF = cell.viewWithTag(1005) as! UILabel
                lblTopTenSmallCapMF.text =  self.returnArr[indexPath.row].value(forKey: "ptp_nse_small_cap") as? String
                
                let lblNiftyIndex = cell.viewWithTag(1006) as! UILabel
                lblNiftyIndex.text =  self.returnArr[indexPath.row].value(forKey: "ptp_nse_mid_cap") as? String
            }
            
            /*if currentButton == "SMALL" {
                
                
                
            }
            
            else if currentButton == "MID" {
                
                cell = tableView.dequeueReusableCell(withIdentifier:"indexReturnsCellTwo", for: indexPath)
                cell.selectionStyle = .none
                
                cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
                
                let lblPeriod = cell.viewWithTag(100001) as! UILabel
                lblPeriod.text = self.returnArr[indexPath.row].value(forKey: "ptp_period") as? String
                
                let lblDynamicSmallMidCap = cell.viewWithTag(100002) as! UILabel
                lblDynamicSmallMidCap.text =  self.returnArr[indexPath.row].value(forKey: "ptp_small_midcap") as? String
                
                let lblNSESmallCap = cell.viewWithTag(100003) as! UILabel
                lblNSESmallCap.text =  self.returnArr[indexPath.row].value(forKey: "ptp_nse_small_cap") as? String
                
                
                let lblNSEMidCap = cell.viewWithTag(100004) as! UILabel
                lblNSEMidCap.text =  self.returnArr[indexPath.row].value(forKey: "ptp_nse_mid_cap") as? String
                
                
                let lblTopTenSmallCapMF = cell.viewWithTag(100005) as! UILabel
                lblTopTenSmallCapMF.text =  self.returnArr[indexPath.row].value(forKey: "ptp_top_10_small_cap_mf") as? String
                
               // let lblNiftyIndex = cell.viewWithTag(1006) as! UILabel
               // lblNiftyIndex.text =  self.returnArr[indexPath.row].value(forKey: "ptp_nifty_index") as? String
                
            } else if currentButton == "LARGE" {
                
                cell = tableView.dequeueReusableCell(withIdentifier:"indexReturnsCellThree", for: indexPath)
                cell.selectionStyle = .none
                
                cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
                
                let lblPeriod = cell.viewWithTag(1000001) as! UILabel
                lblPeriod.text = self.returnArr[indexPath.row].value(forKey: "ptp_period") as? String
                
                let lblDynamicSmallMidCap = cell.viewWithTag(1000002) as! UILabel
                lblDynamicSmallMidCap.text =  self.returnArr[indexPath.row].value(forKey: "ptp_small_midcap") as? String
                
                let lblNSESmallCap = cell.viewWithTag(1000003) as! UILabel
                lblNSESmallCap.text =  self.returnArr[indexPath.row].value(forKey: "ptp_nse_small_cap") as? String
                
                /*
                let lblNSEMidCap = cell.viewWithTag(1004) as! UILabel
                lblNSEMidCap.text =  self.returnArr[indexPath.row].value(forKey: "ptp_nse_mid_cap") as? String
                
                
                let lblTopTenSmallCapMF = cell.viewWithTag(1005) as! UILabel
                lblTopTenSmallCapMF.text =  self.returnArr[indexPath.row].value(forKey: "ptp_top_10_small_cap_mf") as? String
                
                let lblNiftyIndex = cell.viewWithTag(1006) as! UILabel
                lblNiftyIndex.text =  self.returnArr[indexPath.row].value(forKey: "ptp_nifty_index") as? String
                */
            }*/
            
            

        }
        
        
        
        if(tableView == self.tblvwConstituents)
        {
            
            cell = tableView.dequeueReusableCell(withIdentifier:"indexConstituentsCell", for: indexPath)
            cell.selectionStyle = .none
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            
            let lblCompany = cell.viewWithTag(1001) as! UILabel
            lblCompany.text = self.constituentsArr[indexPath.row].value(forKey: "tcw_company_name") as? String
            
            let lblWeightPercent = cell.viewWithTag(1002) as! UILabel
            lblWeightPercent.text =  self.constituentsArr[indexPath.row].value(forKey: "tcw_weightage") as? String
        }
        
        if(tableView == self.tblvwStatistics0)
        {
            
            cell = tableView.dequeueReusableCell(withIdentifier:"indexVolatilityCell", for: indexPath)
            cell.selectionStyle = .none
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            
            let lblReturn = cell.viewWithTag(1001) as! UILabel
            lblReturn.text = self.staticReturnsArr[indexPath.row].value(forKey: "Description") as? String
            
            let lblQtd = cell.viewWithTag(1002) as! UILabel
            lblQtd.text =  self.staticReturnsArr[indexPath.row].value(forKey: "Qtd") as? String
            
            let lblYtd = cell.viewWithTag(1003) as! UILabel
            lblYtd.text =  self.staticReturnsArr[indexPath.row].value(forKey: "Ytd") as? String
            
            
            let lblSinceInception = cell.viewWithTag(1004) as! UILabel
            lblSinceInception.text =  self.staticReturnsArr[indexPath.row].value(forKey: "Since_Inception") as? String
            
            
            
            
        }
        
        
        if(tableView == self.tblvwStatistics1)
        {
            
            cell = tableView.dequeueReusableCell(withIdentifier:"indexVolatilityCell", for: indexPath)
            cell.selectionStyle = .none
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            
            let lblDescription = cell.viewWithTag(1001) as! UILabel
            lblDescription.text = self.deviationArr[indexPath.row].value(forKey: "Description") as? String
            
            let lblSinceInception = cell.viewWithTag(1002) as! UILabel
            lblSinceInception.text =  self.deviationArr[indexPath.row].value(forKey: "Since_Inception") as? String
            
        }
        
        if(tableView == self.tblvwfundamental)
        {
            
            cell = tableView.dequeueReusableCell(withIdentifier:"indexFundamentalCell", for: indexPath)
            cell.selectionStyle = .none
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            
            let lblPe = cell.viewWithTag(1001) as! UILabel
            lblPe.text = self.fundamentalArr[indexPath.row].value(forKey: "PE") as? String
            
            let lblPb = cell.viewWithTag(1002) as! UILabel
            lblPb.text = self.fundamentalArr[indexPath.row].value(forKey: "PB") as? String
            
            
            let lblDividendYield = cell.viewWithTag(1003) as! UILabel
            lblDividendYield.text =  self.fundamentalArr[indexPath.row].value(forKey: "DIV_YIELD") as? String
            
        }
        
        if(tableView == self.tblvwSector)
        {
            
            cell = tableView.dequeueReusableCell(withIdentifier:"indexSectorCell", for: indexPath)
            cell.selectionStyle = .none
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            
            let lblSector = cell.viewWithTag(1001) as! UILabel
            lblSector.text = self.sectorMajorArr[indexPath.row].value(forKey: "sr_sector") as? String
            
            let lblWeightPercent = cell.viewWithTag(1002) as! UILabel
            lblWeightPercent.text =  self.sectorMajorArr[indexPath.row].value(forKey: "sr_weight") as? String
        }
        
        if(tableView == self.tblMinor)
        {
            
            cell = tableView.dequeueReusableCell(withIdentifier:"indexSectorCell", for: indexPath)
            cell.selectionStyle = .none
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            
            let lblSector = cell.viewWithTag(1001) as! UILabel
            lblSector.text = self.sectorMinorArr[indexPath.row].value(forKey: "sr_sector") as? String
            
            let lblWeightPercent = cell.viewWithTag(1002) as! UILabel
            lblWeightPercent.text =  self.sectorMinorArr[indexPath.row].value(forKey: "sr_weight") as? String
        }
        
        if(tableView == self.tblVolatility)
        {
            
            cell = tableView.dequeueReusableCell(withIdentifier:"indexFundamentalCell", for: indexPath)
            cell.selectionStyle = .none
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            
            let lblPeriod = cell.viewWithTag(1001) as! UILabel
            lblPeriod.text = self.volatilityArr[indexPath.row].value(forKey: "av_period") as? String
            
            let lblDynamicSmallCap = cell.viewWithTag(1002) as! UILabel
            lblDynamicSmallCap.text = self.volatilityArr[indexPath.row].value(forKey: "av_smallcap") as? String
            
            
            let lblNSESmallCap = cell.viewWithTag(1003) as! UILabel
            lblNSESmallCap.text =  self.volatilityArr[indexPath.row].value(forKey: "av_nse_smallcap") as? String
            
        }
        
        return cell
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var rowcount = 0
        
        if(tableView == self.tblvwCharacteristics)
        {
            rowcount =  self.charArr.count
        }
        
        if(tableView == self.tblvwReturns)
        {
            rowcount =  self.returnArr.count
        }
        
        if(tableView == self.tblvwConstituents)
        {
            rowcount =  self.constituentsArr.count
        }
        
        if(tableView == self.tblvwStatistics0)
        {
            rowcount =  self.staticReturnsArr.count
        }
        
        if(tableView == self.tblvwStatistics1)
        {
            rowcount =  self.deviationArr.count
        }
        
        if(tableView == self.tblvwfundamental)
        {
            rowcount =  self.fundamentalArr.count
        }
        
        if(tableView == self.tblvwSector)
        {
            rowcount =  self.sectorMajorArr.count
        }
        
        if tableView == self.tblMinor {
            
            rowcount = self.sectorMinorArr.count
        }
        
        if(tableView == self.tblvwConstituents)
        {
            rowcount =  self.constituentsArr.count
        }
        
        if(tableView == self.tblVolatility)
        {
            rowcount =  self.volatilityArr.count
        }


        
        return rowcount
    }
    
    @IBAction func btnhamburgerMenuClick(_ sender: Any) {
        
        if !isShowingMenu {
            
            //addGestureRecogniser()
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x += 250
            isShowingMenu = true
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x -= 250
            isShowingMenu = false
        }
        
    }
    
    
    
    @IBAction func btnIndexStockClick(_ sender: Any) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DynamicIndexStocksViewController") as! DynamicIndexStocksViewController
        
        viewController.indextype = idxtype
        
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    @IBAction func btnMultibaggerClick(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ALLPMSController") as! ALLPMSController
        self.navigationController?.pushViewController(viewController, animated: false)
        
    }
    
    
    func collapaseAllViews()
    {
        btnPortfolioDropDown.tag = 10001
        
        UIView.animate(withDuration: 0.3, animations: {
            self.tblPortfolioHeightConstent.constant = 0
            self.portfolioCharecteristicHeaderHeightConstant.constant = 0
        })
        
        
        btnStaticsDropDown.tag   = 10002
        
        UIView.animate(withDuration: 0.5, animations: {
            self.tblStaticsHeightConstant.constant = 0
            self.tblStaticsTwoHeight.constant = 0
            self.tblValocityHeightConstant.constant = 0
            self.tblFundamentalHeightConstant.constant = 0
            self.staticsHeaderViewHeightConstant.constant = 0
            self.lblAnnualizedValocityHeightConstant.constant = 0
            self.staticsHeaderViewHeightConstant.constant = 0
        })
        
        
        btnSectorRepresentation.tag = 10003
        
        UIView.animate(withDuration: 0.5, animations: {
            self.lblSectorRepresentatinMinorHeightConstant.constant = 0
            self.sectorRepresentationHeaderHeightConstant.constant = 0
            self.tblSectorMajorHeightConstant.constant = 0
            self.tblSectorMinorHeightConstant.constant = 0
        })
        
        btnTopIndexDropDown.tag = 10004
        
        UIView.animate(withDuration: 0.5, animations: {
            self.topIndexHeaderHeightConstant.constant = 0
            self.tblWeightageHeightConstant.constant = 0
            self.lblTopIndex.text = ""
        })
        
        btnIndexMethodology.tag = 10005
        
        let txt = NSAttributedString(string: "")
        
        UIView.animate(withDuration: 0.5, animations: {
            self.txtvwMethodololy.attributedText = txt
        })
        
        
    }
    
    //MARK: Portfolio Characteristics Button Action
    @IBAction func portfolioDownArrowClick(_ sender: Any) {
        
        if btnPortfolioDropDown.tag == 10001 {
            collapaseAllViews()
            btnPortfolioDropDown.tag = 20001
            
            UIView.animate(withDuration: 0.3, animations: {
                self.portfolioCharecteristicHeaderHeightConstant.constant = 45
                
                let sectionIndex = IndexSet(integer: 0)
                self.tblPortfolioHeightConstent.constant = CGFloat(self.charArr.count * 45)
                self.tblvwCharacteristics.reloadSections(sectionIndex, with: .automatic)
            })
            
        } else if btnPortfolioDropDown.tag == 20001 {
            btnPortfolioDropDown.tag = 10001
            
            UIView.animate(withDuration: 0.3, animations: {
                self.tblPortfolioHeightConstent.constant = 0
                self.portfolioCharecteristicHeaderHeightConstant.constant = 0
            })
        }
    }
    
    //MARK: Statics Button Action
    @IBAction func btnStaticsDownArrow_Click(_ sender: UIButton) {
        
        if btnStaticsDropDown.tag   == 10002 {
            collapaseAllViews()
            btnStaticsDropDown.tag   = 20002
            
            staticsHeaderViewHeightConstant.constant = 45
            UIView.animate(withDuration: 0.5, animations: {
                self.tblvwStatistics0.reloadSections(IndexSet(integer: 0), with: .automatic)
                self.tblStaticsHeightConstant.constant = CGFloat(self.staticReturnsArr.count * 45)
                
                self.tblvwStatistics1.reloadSections(IndexSet(integer: 0), with: .automatic)
                self.tblStaticsTwoHeight.constant = CGFloat(self.deviationArr.count * 45)
                //
                
                self.tblVolatility.reloadSections(IndexSet(integer: 0), with: .automatic)
                self.tblValocityHeightConstant.constant = CGFloat(self.volatilityArr.count * 45)
                
                self.tblFundamentalHeightConstant.constant = CGFloat(self.fundamentalArr.count * 45)
                self.tblvwfundamental.reloadSections(IndexSet(integer: 0), with: .automatic)
                self.staticsHeaderViewHeightConstant.constant = 45
                self.lblAnnualizedValocityHeightConstant.constant = 30
            })
            
        } else if btnStaticsDropDown.tag == 20002 {
            btnStaticsDropDown.tag   = 10002
            
            UIView.animate(withDuration: 0.5, animations: {
                self.tblStaticsHeightConstant.constant = 0
                self.tblStaticsTwoHeight.constant = 0
                self.tblValocityHeightConstant.constant = 0
                self.tblFundamentalHeightConstant.constant = 0
                self.staticsHeaderViewHeightConstant.constant = 0
                self.lblAnnualizedValocityHeightConstant.constant = 0
                self.staticsHeaderViewHeightConstant.constant = 0
            })
        }
        
    }
    
    //MARK: Sector Representation Action
    @IBAction func btnSectorRepresentationDownArrow_Action(_ sender: UIButton) {
        
        if btnSectorRepresentation.tag == 10003 {
            
            collapaseAllViews()
            btnSectorRepresentation.tag = 20003
            
            UIView.animate(withDuration: 0.5, animations: {
                self.lblSectorRepresentatinMinorHeightConstant.constant = 35
                self.sectorRepresentationHeaderHeightConstant.constant = 45
                
                self.tblSectorMinorHeightConstant.constant = CGFloat(self.sectorMinorArr.count * 45)
                self.tblMinor.reloadSections(IndexSet(integer: 0), with: .automatic)
                self.tblSectorMajorHeightConstant.constant = CGFloat(self.sectorMajorArr.count * 45)
                self.tblvwSector.reloadSections(IndexSet(integer: 0), with: .automatic)
            })
            
            
        } else if btnSectorRepresentation.tag == 20003 {
            btnSectorRepresentation.tag = 10003
            
            UIView.animate(withDuration: 0.5, animations: {
                self.lblSectorRepresentatinMinorHeightConstant.constant = 0
                self.sectorRepresentationHeaderHeightConstant.constant = 0
                self.tblSectorMajorHeightConstant.constant = 0
                self.tblSectorMinorHeightConstant.constant = 0
            })
            
        }
        
    }
    
    //MARK: Top Index Action
    @IBAction func btnTopIndexDropDown_Action(_ sender: UIButton) {
        
        if btnTopIndexDropDown.tag == 10004 {
            collapaseAllViews()
            
            btnTopIndexDropDown.tag = 20004
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.lblTopIndex.text = "# QTD, YTD and 1 year returns are Absolute returns. Returns for greater than one year are CAGR returns.* Average daily standard deviation annualized"
                
                self.topIndexHeaderHeightConstant.constant = 45
                self.tblWeightageHeightConstant.constant = CGFloat(self.constituentsArr.count * 45)
                self.tblvwConstituents.reloadSections(IndexSet(integer: 0), with: .automatic)
                
            })
            
            
        } else  if btnTopIndexDropDown.tag == 20004{
            btnTopIndexDropDown.tag = 10004
            
            UIView.animate(withDuration: 0.5, animations: {
                self.topIndexHeaderHeightConstant.constant = 0
                self.tblWeightageHeightConstant.constant = 0
                self.lblTopIndex.text = ""
            })
        }
    }
    
    @IBAction func btnIndexMethodology_Action(_ sender: UIButton) {
        
        if btnIndexMethodology.tag == 10005 {
            
            collapaseAllViews()
            btnIndexMethodology.tag = 20005
            
            UIView.animate(withDuration: 0.5, animations: {
                let attrMethodologyStr = try! NSAttributedString(
                    data: self.indexMethodology.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                    options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                    documentAttributes: nil)
                self.txtvwMethodololy.attributedText = attrMethodologyStr
            })
            
            
        } else if btnIndexMethodology.tag == 20005  {
            btnIndexMethodology.tag = 10005
            
            let txt = NSAttributedString(string: "")
            
            UIView.animate(withDuration: 0.5, animations: {
                self.txtvwMethodololy.attributedText = txt
            })
        }
    }
    
    @IBAction func smallcapClick(_ sender: Any) {
        
        self.idxtype = "Small"
        getData(type: "SMALL")
        currentButton = "SMALL"
        
        
       
        
        btnSmallCap.titleLabel?.font = UIFont(name: "Helvetica Neue Bold", size: 14)
        btnMidCap.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 12)
        btnLargeCap.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 12)
        
        col2.text = "Dynamic Smallcap Index"
        col3.text = "NSE Smallcap 100 Index"
        col4.text = "NSE Midcap 100 Index"
        col5.text = "Top 10 Smallcap MF"
        col6.text = "Nifty Index"
        
        returnHeaserViewOne.isHidden    = false
        
    }
    
    @IBAction func midcapClick(_ sender: Any) {
        
        self.idxtype = "Mid"
        getData(type: "MID")
        currentButton = "MID"
        
        btnSmallCap.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 12)
        btnMidCap.titleLabel?.font = UIFont(name: "Helvetica Neue Bold", size: 14)
        btnLargeCap.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 12)
        
        col2.text = "Dynamic Midcap Index"
        col3.text = "NSE Midcap 100 Index"
        col4.text = "NSE Midcap 50 Index"
        col6.text = "Top 10 Midcap MF"
        col5.text = "Nifty Index"
        
        returnHeaserViewOne.isHidden    = false
        
    }
    
    
    @IBAction func largecapClick(_ sender: Any) {
        self.idxtype = "Large"
        getData(type: "LARGE")
        currentButton = "LARGE"
        
        btnSmallCap.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 12)
        btnMidCap.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 12)
        btnLargeCap.titleLabel?.font = UIFont(name: "Helvetica Neue Bold", size: 14)
        
        col2.text = ""
        col3.text = ""
        col4.text = "Dynamic Largecap Index"
        col5.text = "Nifty 50 Index"
        col6.text = "Top 10 Largecap MF"
        
        
        returnHeaserViewOne.isHidden    = false
        
    }
    
    
    @IBAction func multibaggerButtonClick(_ sender: Any) {
        
       
        DispatchQueue.main.async {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
            self.navigationController?.pushViewController(viewController, animated: false)
        }
       
    }
    
    @IBAction func searchClick(_ sender: Any) {
         DispatchQueue.main.async {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(viewController, animated: false)
        }
        
    }
}
