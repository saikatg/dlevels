//
//  SectorDetailsViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 15/05/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class SectorDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource 
{
    
    @IBOutlet weak var btnhamburger: UIButton!
    
    @IBOutlet weak var lblChngPer: UILabel!
    @IBOutlet weak var lblnodatamsg: UILabel!
    @IBOutlet weak var headersector: UILabel!
    @IBOutlet weak var tblSlideMenuLlist: UITableView!
    
    @IBOutlet weak var btnChangeCategory: UIButton!
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    var sectorListArray = [NSDictionary]()
    var sectorListArraySortByCategory = [NSDictionary]()
    var countsecdtl = 0
    var col = "ALL"
    var mktcol = "ALL"
    
    
    var period = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblChngPer.text = "Chng% " + period

        // Do any additional setup after loading the view.
        addLoader()
        
        tickerlist()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addLoader() {
        
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    
    @IBAction func btnTableHeaderAction(_ sender: UIButton) {
        
        let actionSheeController = UIAlertController(title:"Data Fields", message: "", preferredStyle: .actionSheet)
        actionSheeController.view.tintColor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
        
        actionSheeController.addAction(UIAlertAction(title: "ALL", style: .default, handler: { (success) in
            //self.btnChangeCategory.setTitle("ALL", for: .normal)
            self.col = "ALL"
            self.sectorlistSortByCategory(mktCap: self.col, category: self.col)
            
            UIView.animate(withDuration: 0.4) {
                 self.tblSlideMenuLlist.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        actionSheeController.addAction(UIAlertAction(title: "BUY", style: .default, handler: { (success) in
            //self.btnChangeCategory.setTitle("BUY", for: .normal)
            self.col = "BUY"
            self.sectorlistSortByCategory(mktCap: self.mktcol, category: self.col)
            UIView.animate(withDuration: 0.4) {
                self.tblSlideMenuLlist.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "EXIT", style: .default, handler: { (success) in
            self.col = "EXIT"
            self.sectorlistSortByCategory(mktCap: self.mktcol, category: self.col)
            //self.btnChangeCategory.setTitle("EXIT", for: .normal)
            UIView.animate(withDuration: 0.4) {
                 self.tblSlideMenuLlist.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "HOLD", style: .default, handler: { (success) in
            self.col = "HOLD"
            self.sectorlistSortByCategory(mktCap: self.mktcol, category: self.col)
            //self.btnChangeCategory.setTitle("HOLD", for: .normal)
            UIView.animate(withDuration: 0.4) {
                self.tblSlideMenuLlist.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "RISKY STOCK", style: .default, handler: { (success) in
            
            self.col = "RISKY STOCK"
            self.sectorlistSortByCategory(mktCap: self.mktcol, category: self.col)
            //self.btnChangeCategory.setTitle("RISKY STOCK", for: .normal)
            UIView.animate(withDuration: 0.4) {
                self.tblSlideMenuLlist.reloadData()
                self.view.layoutIfNeeded()
            }
        }) )
        
        actionSheeController.addAction(UIAlertAction(title: "HOLD (LESS VOLUME)", style: .default, handler: { (success) in
            
            self.col = "HOLD (LESS VOLUME)"
            self.sectorlistSortByCategory(mktCap: self.mktcol, category: self.col)
            //self.btnChangeCategory.setTitle("HOLD(LESS VOLUME)", for: .normal)
            UIView.animate(withDuration: 0.4) {
                self.tblSlideMenuLlist.reloadData()
                self.view.layoutIfNeeded()
            }
        }) )
        
        actionSheeController.addAction(UIAlertAction(title: "BUY (LESS VOLUME)", style: .default, handler: { (success) in
            
            self.col = "BUY (LESS VOLUME)"
            self.sectorlistSortByCategory(mktCap: self.mktcol, category: self.col)
           // self.btnChangeCategory.setTitle("BUY(LESS VOLUME)", for: .normal)
            UIView.animate(withDuration: 0.4) {
                 self.tblSlideMenuLlist.reloadData()
                self.view.layoutIfNeeded()
            }
        }) )
        
        actionSheeController.addAction(UIAlertAction(title: "NEW LISTING", style: .default, handler: { (success) in
            
            self.col = "NEW LISTING"
            self.sectorlistSortByCategory(mktCap: self.mktcol, category: self.col)
            //self.btnChangeCategory.setTitle("NEW LISTING", for: .normal)
            UIView.animate(withDuration: 0.4) {
                self.tblSlideMenuLlist.reloadData()
                self.view.layoutIfNeeded()
            }
        }) )
        
       
        
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        
        
        self.present(actionSheeController, animated: true) {
            
        }
        
    }

    
    
    @IBAction func btn_sort(_ sender: Any) {
        
        
           self.sectorListArraySortByCategory = self.sectorListArraySortByCategory.reversed()
           self.tblSlideMenuLlist.reloadData()
            self.view.layoutIfNeeded()
    }
    
    
       //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return sectorListArraySortByCategory.count
       
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let viewControllersList = self.navigationController?.viewControllers {
            for tempView  in viewControllersList {
                if tempView.isKind(of: StockSpecificNewViewController.self) {
                    tempView.removeFromParentViewController()
                }
            }
        }
        
        SearchForCurrentMultibagger.serchValue = sectorListArraySortByCategory[indexPath.row].value(forKey: "Symbol_Name") as! String
        
        SearchForCurrentMultibagger.instrument_4 = sectorListArraySortByCategory[indexPath.row].value(forKey: "mbd_Instrument_4") as! String
        
        
        SearchForCurrentMultibagger.viewcontrollername = "Search"
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "StockSpecificNewViewController") as! StockSpecificNewViewController
        self.navigationController?.pushViewController(viewController, animated: false)
        
    }

    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var index = NSDictionary()
        let cell = tableView.dequeueReusableCell(withIdentifier:"tickercell", for: indexPath)
        cell.selectionStyle = .none
        
        DispatchQueue.main.async {
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
            cell.selectionStyle = .none
            
            cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
            
          
               index =  self.sectorListArraySortByCategory[indexPath.row]
           
            let lblName = cell.viewWithTag(2002) as! UILabel
            let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
            let underlineAttributedString = NSAttributedString(string: (index.value(forKey: "mbd_Instrument_4") as? String)!, attributes: underlineAttribute)
            lblName.attributedText = underlineAttributedString
            
            //Type
            let lblType = cell.viewWithTag(2005) as! UILabel
            lblType.text = index.value(forKey: "Type") as? String
            
            let lblPERatio = cell.viewWithTag(2003) as! UILabel
            lblPERatio.text = index.value(forKey: "Category") as? String
            
            
            let lblMonth = cell.viewWithTag(2004) as! UILabel
            lblMonth.text = index.value(forKey: "Performance") as? String
            
            cell.layoutMargins = UIEdgeInsets.zero
            
            cell.contentView.setNeedsLayout()
            cell.contentView.layoutIfNeeded()
            
        }
        
        return cell
}
    
    @IBAction func btnhamburgerclick(_ sender: Any) {
        
//        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerSectorViewController") as! MultibaggerSectorViewController
//        self.navigationController?.pushViewController(viewController, animated: false)
        self.navigationController?.popViewController(animated: false)

        
    }
    func sectorlistSortByCategory(mktCap : String, category : String)  {
        
        self.sectorListArraySortByCategory.removeAll()
        
        var str_mktCap = self.mktcol
        var str_mktcategory = self.col
        
        if(str_mktCap == "")
        {
            str_mktCap = "ALL"
        }
        if(str_mktcategory == "")
        {
            str_mktcategory = "ALL"
        }
        
        if(str_mktcategory == "ALL" && str_mktCap == "ALL")
        {
            self.sectorListArraySortByCategory = self.sectorListArray
        }
        else
        {
            
            for arr in self.sectorListArray
            {
                if(str_mktcategory == "ALL")
                {
                    if((arr.value(forKey: "Type") as! String).lowercased() == str_mktCap.lowercased())
                    {
                        self.sectorListArraySortByCategory.append(arr)
                    }
                }
                
                else if(str_mktCap == "ALL")
                {
                    if((arr.value(forKey: "Category") as! String).lowercased() == str_mktcategory.lowercased())
                    {
                        self.sectorListArraySortByCategory.append(arr)
                    }
                }
                else
                {
                    if(((arr.value(forKey: "Category") as! String).lowercased() == str_mktcategory.lowercased())
                        && ((arr.value(forKey: "Type") as! String).lowercased() == str_mktCap.lowercased()))
                    {
                        self.sectorListArraySortByCategory.append(arr)
                    }
                }
                
            }
        }
        
        if(self.sectorListArraySortByCategory.count > 0)
        {
            
            self.lblnodatamsg.text = ""
            self.lblnodatamsg.isHidden = true
            //tableView.backgroundView = lblnodatamsg;
            self.tblSlideMenuLlist.separatorStyle = .none;
            
        }
        else
        {
            self.lblnodatamsg.text = "No data found."
            self.lblnodatamsg.textColor = UIColor.black
            self.lblnodatamsg.numberOfLines = 0;
            //self.lblnodatamsg.textAlignment = .center;
            self.lblnodatamsg.font = UIFont(name: "TrebuchetMS", size: 15)
            //self.lblnodatamsg.sizeToFit()
            self.lblnodatamsg.isHidden = false
            //tableView.backgroundView = lblnodatamsg;
            self.tblSlideMenuLlist.separatorStyle = .none;
        }
        
    }
    
    
    @IBAction func btnmktcapClick(_ sender: Any) {
        
        let actionSheeController = UIAlertController(title:"Data Fields", message: "", preferredStyle: .actionSheet)
        actionSheeController.view.tintColor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
        
        actionSheeController.addAction(UIAlertAction(title: "ALL", style: .default, handler: { (success) in
            self.mktcol = "ALL"
            self.sectorlistSortByCategory(mktCap: self.mktcol, category: self.col)
            UIView.animate(withDuration: 0.4) {
                self.tblSlideMenuLlist.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        actionSheeController.addAction(UIAlertAction(title: "Small Cap", style: .default, handler: { (success) in
            self.mktcol = "Small Cap"
            self.sectorlistSortByCategory(mktCap: self.mktcol, category: self.col)
            UIView.animate(withDuration: 0.4) {
                self.tblSlideMenuLlist.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Small Cap Mini", style: .default, handler: { (success) in
            self.mktcol = "Small Cap Mini"
            self.sectorlistSortByCategory(mktCap: self.mktcol, category: self.col)
            UIView.animate(withDuration: 0.4) {
                self.tblSlideMenuLlist.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Mid Cap", style: .default, handler: { (success) in
            self.mktcol = "Mid Cap"
            self.sectorlistSortByCategory(mktCap: self.mktcol, category: self.col)
            UIView.animate(withDuration: 0.4) {
                self.tblSlideMenuLlist.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        actionSheeController.addAction(UIAlertAction(title: "Large Cap", style: .default, handler: { (success) in
            self.mktcol = "Large Cap"
            self.sectorlistSortByCategory(mktCap: self.mktcol, category: self.col)
            UIView.animate(withDuration: 0.4) {
                self.tblSlideMenuLlist.reloadData()
                self.view.layoutIfNeeded()
            }
        }))
        
        
        actionSheeController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (success) in
        }) )
        
        
        self.present(actionSheeController, animated: true) {
            
        }
        
        
        
    }
    

    func tickerlist()
    {
        
        self.actInd.startAnimating()
        
        let obj = WebService()
        var url_param = ""
        let sector = SearchForMultibaggerSector.serchValue.replacingOccurrences(of: " ", with: "%20").replacingOccurrences(of: "&", with: "%26")
        let segdt = SearchForMultibaggerSector.seg_date
        let type_param = "date=\(segdt)&sector=\(sector)"
       // print("Sector ----- \(type_param)")
        
        self.headersector.text = "SECTOR: \(SearchForMultibaggerSector.serchValue)"
        
        if(SearchForCurrentMultibagger.viewcontrollername == "MultibaggerQuarterlySectorViewController")
        {
            url_param = Urls.sector_Wise_Symbol_Quarterly
        }
        else
        {
            url_param = Urls.sector_Wise_Symbol
        }
        
        
        obj.callWebServices(url: url_param, methodName: "GET", parameters: type_param, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
           print("Sector List \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                print("Selected Value From Array is : \(SearchForMultibaggerSector.serchValue)")
                
                
                let tempArrayDict : [NSDictionary] = jsonData.value(forKey: "response") as! [NSDictionary]
                
                if(tempArrayDict.count > 0)
                {
                    self.lblnodatamsg.text = ""
                    self.lblnodatamsg.isHidden = true
                    //tableView.backgroundView = lblnodatamsg;
                    self.tblSlideMenuLlist.separatorStyle = .none;
                    
                    
                    
                    for dictValue in tempArrayDict {
                        self.sectorListArray.append(dictValue)
                    }
                    
                }
                else
                {
                    self.lblnodatamsg.text = "No data found."
                    self.lblnodatamsg.textColor = UIColor.black
                    self.lblnodatamsg.numberOfLines = 0;
                    self.lblnodatamsg.textAlignment = .center;
                    self.lblnodatamsg.font = UIFont(name: "TrebuchetMS", size: 15)
                    self.lblnodatamsg.sizeToFit()
                    self.lblnodatamsg.isHidden = false
                    //tableView.backgroundView = lblnodatamsg;
                    self.tblSlideMenuLlist.separatorStyle = .none;
                    
                }
                self.sectorListArraySortByCategory = self.sectorListArray
                
                
                DispatchQueue.main.async {
                    
                    self.lblnodatamsg.text = ""
                    //let sectionIndex = IndexSet(integer: 0)
                    self.tblSlideMenuLlist.reloadData()
                    self.actInd.stopAnimating()
                    
                }
            }
            else
            {
                let alertobj = AppManager()
                self.actInd.stopAnimating()
                print(jsonData.value(forKey: "errmsg") as! String)
                
            }
        }
    }

}
