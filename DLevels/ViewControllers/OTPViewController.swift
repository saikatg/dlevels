//
//  ViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 23/12/16.
//  Copyright © 2016 Dynamic-Mac-01. All rights reserved.
//

import UIKit
extension UITextField {
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
class OTPViewController: UIViewController, UITextFieldDelegate,UIAlertViewDelegate {
    
    @IBOutlet weak var lblResendOtp: UILabel!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var lbltimer: UILabel!
    @IBOutlet weak var lbltype: UILabel!
    @IBOutlet weak var btnresend: UIButton!
    @IBOutlet weak var txtotp: UITextField!
    @IBOutlet weak var btnsubmit: UIButton!
    @IBOutlet weak var lblVerificationText: UILabel!
    var code = "+91"
    var number = ""
    
    /////popupbox changenumber btn click //////
    @IBOutlet weak var lblemail: UILabel!
    @IBOutlet weak var txt_code: UITextField!
    @IBOutlet weak var txt_phone_number: UITextField!
    @IBOutlet weak var view_popupbox: UIView!
    
    
    var codeTextField: UITextField?;
    var numberTextField: UITextField?;
    var buttonField: UIButton?;
    var imageField: UIImageView?;
    
    
    var emailAddr   : String?
    var emailOld    : String?
    var phoneNum    : String?
    var checkotptype: String!
    
    var counter = 30
    var timer   = Timer()
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    @IBAction func btnChange(_ sender: Any) {
        
       //self.changeButtonClick(screenType: checkotptype)
        self.askForNumber(screenType: checkotptype);
        
}
    @IBAction func btn_cancel(_ sender: Any) {
        view_popupbox.isHidden=true
        btnsubmit.isEnabled=true
        btnresend.isEnabled=true


    }
    
    @IBAction func btn_ok(_ sender: Any) {
        
        //changeButtonClick(screenType: <#String#>)
    }
    
    @IBAction func btnOk(_ sender: Any) {
       // self.changeButtonClick(screenType: checkotptype)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view_popupbox.layer.borderWidth = 2.0
        view_popupbox.layer.borderColor = UIColor.gray.cgColor
        view_popupbox.layer.cornerRadius=5.0;
        
//        User.phone = "9007154608"
//        User.countryCode = "+91"
//        User.email = "anwesha@dynamiclevels.com"
//        checkotptype = "phone"
        if(CountryCode.countryCode != ""){
            code = "+\(CountryCode.countryCode)"
                    }
        else{
            code = "+91"
        }
        //self.codeTextField!.text = code

        self.hideKeyboardOnTapOutside()
        lbltype.text = User.phone
        lblemail.text = "and \(User.email)"
        btnresend.isHidden = true
        lblResendOtp.text = "Did not recieve OTP? Please wait \(counter) second(s)"
        
        btnresend.layer.borderColor = UIColor.black.cgColor
        btnresend.layer.borderWidth = 2.0
        
        emailAddr = User.email
        emailOld = User.email
        phoneNum = User.phone
        
        lbltype.text = "\(User.countryCode) - \(User.phone)"
        
        
        timer.invalidate() // just in case this button is tapped multiple times
        // start the timer
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
        addLoader()
    }
    
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }

    @IBAction func btnresend(_ sender: Any) {
        
        //let newmobileno: Int = { return Int(User.phone)! }()
        //let newmobilenoresend = "\(User.countryCode)|\(User.phone)"
        let newmobilenoresend = lbltype.text?.replacingOccurrences(of: " - ", with: "|")
        let parameters = "email=\(emailAddr!)&phone=\(newmobilenoresend)"
        
        self.resend(urlAddress: Urls.generatePhoneOtp, param: parameters)
        btnresend.isHidden = true
        
        self.counter = 30
        
        self.timer.invalidate() // just in case this button is tapped multiple times
        // start the timer
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
        
    }
    
    
    
    @IBAction func backButton_Action(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnsubmit(_ sender: Any)
    {
        actInd.startAnimating()
        let newmobileno: Int64 = Int64(User.phone)!
        let parameters = "email=\(emailAddr!)&phone=\(newmobileno)&otp=\(txtotp.text!)"
        self.checkOTP(urlAddress: Urls.updatePhoneNo, parameters: parameters)
    }
        
}


extension OTPViewController {
    
    
    
    func addDoneButtonOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(OTPViewController.doneButtonAction)
        )
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        txtotp.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        txtotp.resignFirstResponder()
    }
    
    func checkOTP(urlAddress: String, parameters: String)
    {
        let obj = WebService()
        let token = ""
        
        //print("the parameters is :  \(parameters)")
        
        
        // let otpval = txtotp.text
        //let parameters = "email=\(User.email)&phone=\(phoneNum)&otp=\(txtotp.text!)"
        
        obj.callWebServices(url: urlAddress, methodName: "POST", parameters: parameters, istoken: false, tokenval: token) { (returnValue, jsonData) in
            
            //print("Json Dict is : \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let emailval = "email=\(User.email)"
                obj.callWebServices(url: Urls.wellcome_phone, methodName: "POST", parameters:emailval, istoken: false, tokenval: "") { (returnValue, jsonData) in
                    print("Json Dict is : \(jsonData)")
                    
                }
                
                if(User.regThrough == "normal")
                {
                    User.email = self.emailAddr!
                    self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
                }
                else
                {
                    User.password = User.socialToken
                    self.performLogin(email: User.email, password: User.password, regThrough: User.regThrough)
                }
            }
            else
            {
                
                let alertobj = AppManager()
                
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
                
                
            }
        }
    }
    
    
    // MARK: Performing Login Action
    func performLogin(email : String, password: String, regThrough: String) {
        
        let appManager = AppManager()
        let webService = WebService()
        
        let parameters = "user_email=\(email)&password=\(password)&logthrough=\(regThrough)"
        
        if appManager.isValidEmail(email: email) {
            
            UserDefaults.standard.set(User.email, forKey: "email")
            UserDefaults.standard.set(User.password, forKey: "password")
            UserDefaults.standard.set(User.regThrough, forKey: "regThrough")
            
            //            webService.callWebServices(url: Urls.login, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
            webService.callWebServices(url: Urls.login_without_emailverification, methodName: "POST", parameters: parameters, istoken: false, tokenval: "", completion: { (success, dictData) in
                print(dictData)
                if dictData.value(forKey: "error") as! NSInteger == 0 {
                    
                    let tokendic = dictData.value(forKey: "response") as! NSDictionary
                    User.token = (tokendic.value(forKey: "token") as! NSString) as String
                    
                     DispatchQueue.main.async {
                        let sucessViewConroller = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
                        self.navigationController?.pushViewController(sucessViewConroller, animated: true)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        appManager.showAlert(title: "Error", message: (dictData.value(forKey: "response") as! NSString) as String, navigationController: self.navigationController!)
                    }
                }
            })
            
        } else{           
            appManager.showAlert(title: "Error", message: "Email Id Not Valid! \n ", navigationController: self.navigationController!)
        }
    }
    
    
    func sendEmailOTP(email: String) {
        
        let objWebService = WebService()
        
        objWebService.callWebServices(url: Urls.generateEmailOtp, methodName: "POST", parameters: "email=\(User.email)", istoken: false, tokenval: "") { (success, jsonDict) in
            
            //print(jsonDict)
            
            if jsonDict.value(forKey: "response") as! String == "success" {
                
               DispatchQueue.main.async {
                    let EmailViewController = self.storyboard?.instantiateViewController(withIdentifier: "EmailOTPViewController1") as! EmailOTPViewController
                    
                    
                    self.navigationController?.pushViewController(EmailViewController, animated: true)
                }
                
                
            } else {
                
                DispatchQueue.main.async {
                
                self.actInd.stopAnimating()
                let alert = AppManager()
                alert.showAlert(title: "Error!", message: jsonDict.value(forKey: "errmsg") as! String, navigationController: self.navigationController!)
                }
            }
        }
    }
    
    func resend(urlAddress: String, param: String)
    {
        let obj = WebService()
        let token = ""
        
        //print("email = \(emailaddr)")
        //let parameters = "email=\(emailAddr)&phone=\(phoneNum)"
        
        //print(param)
        
        obj.callWebServices(url: urlAddress, methodName: "POST", parameters: param, istoken: false, tokenval: token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let alertobj = AppManager()
                
                alertobj.showAlert(title: "Sucess", message: "OTP Send Sucessfully.", navigationController: self.navigationController!)
                
                
                
                
            }
            else
            {
                DispatchQueue.main.async {
                    self.actInd.stopAnimating()
                    let alertobj = AppManager()
                    
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
                
            }
            
        }
        
    }
    
    // must be internal or public.
    func timerAction() {
        if counter > 0 {
            lblResendOtp.text = "Did not recieve OTP? Please wait \(counter) second(s)"
            counter -= 1
        }
        else
        {
            self.btnresend.isHidden = false
            lblResendOtp.text = "Did not recieve OTP?"
            lblResendOtp.textAlignment = .left
            view.layoutIfNeeded()
        }
    }
    
    func phoneEmailChange(type: String, urlstr: String, inputtext: String, countryCode : String)
    {
        let obj = WebService()
        let token = ""
        var parameters_val = ""
        
        
        //let newmobileno: Int64 = Int64(inputtext)!
        let newmobileno = "\(countryCode)|\(inputtext)"

       // let newcountryCode: Int64 = Int64(countryCode)!
        //let newmobileno: Int = { return Int(inputtext)! }()
        parameters_val = "email=\(User.email)&phone=\(newmobileno)"
        self.actInd.startAnimating()
        
        obj.callWebServices(url: urlstr, methodName: "POST", parameters: parameters_val, istoken: false, tokenval: token) { (returnValue, jsonData) in
            
            
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                self.phoneNum = inputtext
                User.phone = inputtext
                
                
                DispatchQueue.main.async {
                    
                    self.lbltype.text = "\(countryCode) - \(inputtext)"
                    self.actInd.stopAnimating()
                    let alertobj = AppManager()
                    
                    alertobj.showAlert(title: "Sucess", message: "OTP has been Send in your new mobile no.", navigationController: self.navigationController!)
                    
                }
                
                //print(jsonData)
            }
            else
            {
                DispatchQueue.main.async {
                    
                    self.actInd.stopAnimating()
                    let alertobj = AppManager()
                    
                    alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
                }
            }
        }
        
        
    }
    
    func buttonClicked(sender: UIButton!) {
       
        DispatchQueue.main.async(execute: {
           self.dismiss(animated: true, completion: nil)
            let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "CountryCodeSearchViewController") as! CountryCodeSearchViewController
            self.navigationController?.pushViewController(viewController, animated: true)
            
            
            
            
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if(CountryCode.countryCode != ""){
            code = "+\(CountryCode.countryCode)"
            
            self.askForNumber(screenType: checkotptype);
            
    }
        else{
            code = "+91"
            
        }
        
       // lbltype.text = "\(code) - \(User.phone)"
        self.codeTextField?.text = code
        self.numberTextField?.text = number
        
    }

    func askForNumber(prefix: String = "", screenType: String){
        
        var urlstring   = Urls.generatePhoneOtp
        var title = "";
        var message = "Enter your mobile number\n\n\n";
        var alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert);
        alert.isModalInPopover = true;
      
        func handleOk(alertView: UIAlertAction!){
            
             code = self.codeTextField!.text!;
             number = self.numberTextField!.text!;
            if(CountryCode.countryCode != ""){
                code = "+\(CountryCode.countryCode)"}
            else{
               code = codeTextField!.text!
            }
           
            if (code.characters.count) < 2  {
                //AppManager().showAlert(title: "Invalid Country Code.", message: "", navigationController: (parent?.navigationController)!)
            }
            
            if (number.characters.count) < 10  {
                //AppManager().showAlert(title: "Invalid Phone Number.", message: "", navigationController: self.navigationController!)
            }
                
                
                
            else
            {
                
                self.phoneEmailChange(type: screenType ,urlstr: urlstring, inputtext: number, countryCode: code)
            }
        }
        
        
        let inputFrame = CGRect(x: 0, y: 70, width: 270, height: 25)
        let inputView: UIView = UIView(frame: inputFrame);
        

        let codeButtonFrame = CGRect(x: 20, y: 0, width: 65, height: 25)
        let countryCodeButtonField: UIButton = UIButton(frame: codeButtonFrame);
        countryCodeButtonField.setTitle("", for: .normal)
        countryCodeButtonField.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
	
      
        //let bottmConstraint = NSLayoutConstraint(
         //   item: triangleDownImage, attribute: .bottom, relatedBy: .equal, toItem: self.bottomLayoutGuide, attribute:.top, multiplier: 1, constant: 20)
        //triangleDownImage.addConstraint(bottmConstraint)
        
        let codeFrame = CGRect(x: 20, y: 0, width: 65, height: 25)
        let countryCodeTextField: UITextField = UITextField(frame: codeFrame);
        countryCodeTextField.placeholder = "Code";
        countryCodeTextField.borderStyle = UITextBorderStyle.roundedRect;
        countryCodeTextField.keyboardType = UIKeyboardType.decimalPad;
        countryCodeTextField.text = code
        
        let triangleDownImageFrame = CGRect(x: countryCodeTextField.frame.width , y: countryCodeTextField.frame.height/2, width: 17, height: 15)
        let triangleDownImage: UIImageView = UIImageView(frame: triangleDownImageFrame)
        triangleDownImage.image = #imageLiteral(resourceName: "Triangle_Down")
        
        let numberFrame = CGRect(x: 90, y: 0, width: 170, height: 25)
        let myNumberTextField: UITextField = UITextField(frame: numberFrame);
        myNumberTextField.placeholder = "Mobile No";
        myNumberTextField.borderStyle = UITextBorderStyle.roundedRect;
        myNumberTextField.keyboardType = UIKeyboardType.decimalPad;
        
        self.codeTextField = countryCodeTextField;
        self.numberTextField = myNumberTextField;
        self.buttonField = countryCodeButtonField;
        self.imageField = triangleDownImage;
        
        
        //inputView.addSubview(prefixLabel);
        inputView.addSubview(self.codeTextField!);
        inputView.addSubview(self.numberTextField!);
        inputView.addSubview(self.buttonField!);
        inputView.addSubview(self.imageField!);
       
        alert.view.addSubview(inputView);
        
        
        alert.addAction(UIAlertAction(title: "Change", style: UIAlertActionStyle.default, handler:handleOk));
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:nil));
        
        self.present(alert, animated: true, completion: nil);
        
    }
}

    
    func changeButtonClick(screenType: String )
    {
        /********************************************************************
        
        var urlstring   =   ""
        var titlestr    =   ""
        var msg         =   ""
        
        urlstring   = Urls.generatePhoneOtp
        titlestr    = "Change Mobile No"
        msg         = "Please enter your new Mobile No."
        
        
        
        
 
        //1. Create the alert controller.
        let alert = UIAlertController(title: titlestr, message: msg, preferredStyle: .alert)
       // alert.view.tintColor = UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
        
       
        
        
        //2. Add the text field. You can configure it however you need.
        
        alert.addTextField { (textField) in
            textField.text = "+91"
            textField.setBottomBorder()
            textField.keyboardType = UIKeyboardType.phonePad;
            
//            textField.frame = CGRect(x: 0, y: 18, width: alert.view.frame.width / 5, height: 30)
            
            var oldFrame = textField.frame
            oldFrame.origin.y = 18
            oldFrame.size.height = 30
            oldFrame.size.width = 25
            textField.frame = oldFrame

            
        }
        
        alert.addTextField { (textField) in
            
            textField.placeholder = "mobile no";
            //textField.layer.addSublayer(border)
            textField.layer.masksToBounds = true
            //border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
            textField.keyboardType = UIKeyboardType.phonePad;
            
             textField.frame = CGRect(x: textField.frame.origin.x + 5, y: 18, width: 130, height: 30)
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (AAA) in
            
        }))
        
        alert.addAction(UIAlertAction(title: "Verify", style: .default, handler: { [weak alert] (_) in
            
            
            if (alert!.textFields![1].text?.characters.count)! < 10  {
                AppManager().showAlert(title: "Invalid Phone Number.", message: "", navigationController: self.navigationController!)
            }
            else
            {
                
                self.phoneEmailChange(type: screenType ,urlstr: urlstring, inputtext: alert!.textFields![1].text!, countryCode: alert!.textFields![0].text!)
            }
            
            
            
        }))
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
 
        
        
        ********************************************************************/
        
        
    

}
