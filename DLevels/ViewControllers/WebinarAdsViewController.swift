//
//  WebinarAdsViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 01/06/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class WebinarAdsViewController: UIViewController {

    @IBOutlet weak var ivwebinarad: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebinarad()
    }

    @IBAction func btn_click_link(_ sender: Any) {
        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        UIApplication.shared.openURL(NSURL(string: seminar_ad.url)! as URL)        
    }
    @IBAction func btn_close(_ sender: Any) {
                       
//        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerLandingViewController") as! MultibaggerLandingViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
        
        
        let viewController  =   self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
   
    func loadWebinarad(){
        
        DispatchQueue.main.async {
            let urlpath = seminar_ad.templete
            //        ivwebinarad.downloadedFrom(link: urlpath)
            self.ivwebinarad.downloadedFrom(link: urlpath, contentMode: .scaleAspectFill)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
