//
//  OnboardingViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 27/01/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class OnboardingViewController: UIViewController {
    
    @IBOutlet weak var vwExperienceView: UIView!
    @IBOutlet weak var begginerView: UIView!
    @IBOutlet weak var expertView: UIView!
    var onBoardingText = ""
    var noOfViewVisible = 1
    
    // MARK: ViewController Delegate
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.layoutIfNeeded()
        
    }
    
    
    //MARK: View Will Appear Delegate
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    //MARK: Click On Begining
    @IBAction func onBeginner_Click(_ sender: UIButton) {
        
        User.OnboardinString                =   "1|Begginner"
        self.expertView.backgroundColor     =   UIColor.white
        self.begginerView.backgroundColor   =   UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
        pushViewToNextViewController()
    }
    
    
    //MARK: Click On Experience
    @IBAction func onExperienceClick(_ sender: UIButton) {
        
        User.OnboardinString = "1|Experienced"
        
        self.begginerView.backgroundColor = UIColor.white
        self.expertView.backgroundColor  = UIColor(red: 247/255, green: 187/255, blue: 26/255, alpha: 1)
        pushViewToNextViewController()
    }
    
    func pushViewToNextViewController() {
        
        print("User Onboarding Data is \(User.OnboardinString)")
        DispatchQueue.main.async {
            
            
                
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "OnboardingRiskViewController") as! OnboardingRiskViewController
                self.navigationController?.pushViewController(viewController, animated: true)
            
        }
    }
    
    @IBAction func onClick_NextButton(_ sender: UIButton) {
        
        User.OnboardinString    = "1|\"\""
        pushViewToNextViewController()
        
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        
    }
    
    @IBAction func perform_LogOut(_ sender: UIButton) {
        
        let appManager = WebService()
        appManager.callWebServices(url: Urls.logOut, methodName: "POST", parameters: "", istoken: false, tokenval: "") { (success, jsonDict) in
            
            print(jsonDict)
            print(User.token)
            
            DispatchQueue.main.async(execute: {
                
                self.navigationController?.popViewController(animated: true)
                
            })
        }
    }
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }

    func pushViewAfterSkipButtonPressed() {
        
        let webServiceCall = WebService()
        let appManager     = AppManager()
        
        let param = "1|,2|,3|"
        
        let parameter   =   "values=\(param)" //values=\(User.OnboardinString)"
        //print("User token is:\(User.token)")
        
        webServiceCall.callWebServices(url: Urls.onBoarding, methodName: "POST", parameters: parameter, istoken: true, tokenval: User.token) { (success, jsonData) in
            
            //print(jsonData)
            if jsonData.value(forKey: "errmsg") as! String == "" {
                //print("User Onboarding Data is \(User.OnboardinString)")
                DispatchQueue.main.async {
                    self.delay(1, closure: {
                        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
                        self.navigationController?.pushViewController(viewController, animated: true)
                    })
                }
                
            }else {
                
                DispatchQueue.main.async {
                    
                   // self.btnDone.isEnabled = true
                    if let msg = jsonData.value(forKey: "errormsg") {
                        appManager.showAlert(title: "Error", message: "\(msg)", navigationController: self.navigationController!)
                    }
                    
                }
            }
        }
        
    }

    @IBAction func btn_skip(_ sender: Any) {
        
        User.OnboardinString    = "\(User.OnboardinString)\(onBoardingText)"
        pushViewAfterSkipButtonPressed()
        
    }
    
}
