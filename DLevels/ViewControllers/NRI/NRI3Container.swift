//
//  AllControllerContainer.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 07/11/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import UIKit

class NRI3Container: UIViewController {

    var controllerArray : [UIViewController] = []
    
    @IBOutlet weak var webvw: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webvw.loadHTMLString(NRI_DATA.NRI3, baseURL: nil)
        // Do any additional setup after loading the view.
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
