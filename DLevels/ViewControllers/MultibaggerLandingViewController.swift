//
//  MultibaggerLandingViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 08/02/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import Crashlytics
import GoogleMobileAds

class MultibaggerLandingViewController: UIViewController, GADInterstitialDelegate {
    
    @IBOutlet weak var btnsearch: UIButton!
    @IBOutlet weak var btninfo: UIButton!
    @IBOutlet weak var accourdingvwHeight: NSLayoutConstraint!
    @IBOutlet weak var btnsmallcap: UIButton!
    @IBOutlet weak var btnHamburger: UIButton!
   
    @IBOutlet weak var segview: UIView!
    
    @IBOutlet weak var btnlargcap: UIButton!
    
    @IBOutlet weak var lblmutibagger: UILabel!
    @IBOutlet weak var accordionvw: UIView!
    @IBOutlet weak var veButtonConteinerView: UIView!
    
    @IBOutlet weak var vwSearchContainerView: UIView!
  
    @IBOutlet weak var lastpartview: UIView!
   
    @IBOutlet weak var lbl_smallcap: UILabel!
    
    var isDataLoaded = false
    var isAccordianTap = false
    var highlightcolor = UIColor(red: 247/255, green: 196/255, blue: 58/255, alpha: 1)
    
        var instanceOfLeftSlideMenu = LeftSlideMenu()
    var isShowingMenu = false
    var btn_tag = 1
    var interstitial: GADInterstitial!
    
    let names   = ["Small Cap/Mid Cap Multibaggers","Large Cap Multibaggers"]
    
    var multibaggertabdata: [MultibaggerStats] = []
    
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btninfo.imageEdgeInsets = UIEdgeInsetsMake(0, self.btninfo.frame.size.width - 60 , 0, 0)
        self.btnsearch.imageEdgeInsets = UIEdgeInsetsMake(0, self.btnsearch.frame.size.width - 50 , 0, 0)
       
            self.accordionvw.isHidden = true
            self.accourdingvwHeight.constant = 0
        
        // New Button Changes for Round Corner
        veButtonConteinerView.layer.borderColor = UIColor.black.cgColor
        veButtonConteinerView.layer.borderWidth = 1
        btnsmallcap.layer.cornerRadius = 8
        btnlargcap.layer.cornerRadius = 8
        veButtonConteinerView.layer.cornerRadius = 8
        
        vwSearchContainerView.layer.borderColor = UIColor.black.cgColor
        vwSearchContainerView.layer.borderWidth = 1        
        vwSearchContainerView.layer.cornerRadius = 8
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = false // or false to disable rotation
        
        self.btnlargcap.isUserInteractionEnabled = false
        self.btnsmallcap.isUserInteractionEnabled = false
        self.view.isUserInteractionEnabled = false
        
        /**********************************************************************************/
        /*
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-4603335742863525/5299548097")
        
        let request = GADRequest()
        interstitial.load(request)
        */
        /**********************************************************************************/
        
       
        self.loadAddMob(success: { (success) in
            if success {
                
            }
        })
        
        addLoader()
        if !User.isLandingDataLaded {
            
            actInd.startAnimating()
            multibaggerstatload()
            
        }
    }
    
    
    func loadAddMob(success: (_ success:Bool) -> Void) {
        
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-4603335742863525/5299548097")
        interstitial.delegate = self
        let request = GADRequest()
     //   request.testDevices = [kGADSimulatorID]
//        request.testDevices = @[ kGADSimulatorID ];
        interstitial.load(request)
        success(true)
    }
    
    @IBAction func crashButtonTapped(_ sender: AnyObject) {
        Crashlytics.sharedInstance().crash()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
               
        print(User.isLandingDataLaded )
        if User.isLandingDataLaded {
           // self.actInd.stopAnimating()
            
            
            self.view.isUserInteractionEnabled = true
            self.btnlargcap.isUserInteractionEnabled = true
            self.btnsmallcap.isUserInteractionEnabled = true
        }
    }
    
    func addLoader() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0) //CGRectMake(0.0, 0.0, 40.0, 40.0);
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        self.view.addSubview(actInd)
    }
    
    func multibaggerstatload()
    {
        let obj = WebService()
        let paremeters = ""
        
         self.actInd.startAnimating()
        
        //LoadingOverlay.shared.showOverlay(view: self.view)
        
        obj.callWebServices(url: Urls.multibaggerStatus, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! [NSDictionary]
                
                MultibaggerStats.segQtrDateArr.removeAll()
                MultibaggerStats.segQtrDisplayArr.removeAll()
                MultibaggerStats.segYearDateArr.removeAll()
                MultibaggerStats.segYearDisplayArr.removeAll()
                MultibaggerStats.qutrniftyindex.removeAll()
                MultibaggerStats.qutrsmallcapindex.removeAll()
                MultibaggerStats.yearniftyindex.removeAll()
                MultibaggerStats.yearsmallcapindex.removeAll()

                
                for arr in tempArray
                {
                    //segQtrDateArr
                    if(arr.value(forKey: "mdt_Rep_Type") as! String == "QUARTER")
                    {
                        MultibaggerStats.segQtrDateArr.append(arr.value(forKey: "mdt_Date") as! String)
                        MultibaggerStats.segQtrDisplayArr.append(arr.value(forKey: "mdt_Display") as! String)
                        if let mrdSmallCapPer = arr.value(forKey: "mrd_SmallCap_Per") as? String {
                            MultibaggerStats.qutrsmallcapindex.append(mrdSmallCapPer)
                        }
                        if let mrdNiftyPer = arr.value(forKey: "mrd_Nifty_Per") as? String  {
                            MultibaggerStats.qutrniftyindex.append(mrdNiftyPer)
                        }
                        if let mdtPerfDt = arr.value(forKey: "mdt_Perf_Dt") as? String  {
                            MultibaggerStats.qutrperformancetxt.append(mdtPerfDt)
                        }
                    }
                    
                    if(arr.value(forKey: "mdt_Rep_Type") as! String == "YEAR")
                    {
                        MultibaggerStats.segYearDateArr.append(arr.value(forKey: "mdt_Date") as! String)
                        MultibaggerStats.segYearDisplayArr.append(arr.value(forKey: "mdt_Display") as! String)
                        if let mrdSmallCapPer = arr.value(forKey: "mrd_SmallCap_Per") as? String {
                           MultibaggerStats.yearsmallcapindex.append(mrdSmallCapPer)
                        }
                        if let mrdNiftyPer = arr.value(forKey: "mrd_Nifty_Per") as? String {
                            MultibaggerStats.yearniftyindex.append(mrdNiftyPer)
                        }
                         MultibaggerStats.yearperformancetxt.append(arr.value(forKey: "mdt_Perf_Dt") as! String )
                    }
                }
                
                User.isLandingDataLaded = true
                
                //LoadingOverlay.shared.hideOverlayView(view: self.view)
                DispatchQueue.main.async {
                    //self.loadAddMob(success: { (success) in
                       // if success {
                            
                            self.actInd.stopAnimating()
                            self.view.isUserInteractionEnabled = true
                            self.btnlargcap.isUserInteractionEnabled = true
                            self.btnsmallcap.isUserInteractionEnabled = true
                     //   }
                   // })
                }
            }
        }
    }
    
    
    
    func buttonAction(sender: UIButton!) {
        
//        var tag = sender.tag
//        sender.backgroundColor = self.highlightcolor
        
    }

    
    @IBAction func btnlargecap(_ sender: Any) {
        
        self.lbl_smallcap.text = "Get a list of approx 50 top performing Large Cap Stocks which can give you great return in the future. Invest in these stocks now!"
        self.btn_tag = 2
        
        
        
        btnsmallcap.backgroundColor = UIColor.white
        btnlargcap.backgroundColor = UIColor(red: 250.0 / 255, green: 197.0 / 255, blue: 31.0 / 255, alpha: 1)
        
    }
    

    @IBAction func btnsmallcap(_ sender: Any) {
        
        self.lbl_smallcap.text = "Get a list of approx 100 top performing Small and Mid Cap Stocks which can give you great return in the future. Invest in these stocks now!"
        self.btn_tag = 1
     
        btnlargcap.backgroundColor = UIColor.white
        btnsmallcap.backgroundColor = UIColor(red: 250.0 / 255, green: 197.0 / 255, blue: 31.0 / 255, alpha: 1)
    }
    
    
    func buttonViewLoad()
    {
        
        var originx = 0
        
        for (index, element) in names.enumerated() {
            
            let button = UIButton(frame: CGRect(x:originx , y: Int(segview.frame.origin.x-8), width: (Int(segview.frame.width/2)) , height: 40))
            button.backgroundColor = UIColor.white
            button.setTitle(element, for: .normal)
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpOutside)
            
            
            button.setTitleColor(UIColor(red: 103/255, green: 128/255, blue: 138/255, alpha: 1), for: .normal)
            button.titleLabel?.font = UIFont(name: "Montserrat", size: 12)
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
            
            button.layer.borderColor = UIColor(red: 103/255, green: 128/255, blue: 138/255, alpha: 1).cgColor
            button.layer.borderWidth = 1.0
            button.tag = index
            
            if(index == 0)
            {
                button.backgroundColor = self.highlightcolor
            }
            
            segview.addSubview(button)
            originx  = originx + Int(segview.frame.width/2 )
        }
        
        
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppManager().setStatusBarBackgroundColor()
        self.navigationController?.isNavigationBarHidden = true
        
        
    }
    
    @IBAction func btn_info(_ sender: Any) {
        
        if !isAccordianTap{
            isAccordianTap = true
            UIView.animate(withDuration: 1, animations: {
                self.btninfo.transform = CGAffineTransform(scaleX: 1, y: -1);
                self.lblmutibagger.text = "Dlevels Multibaggers are those stocks which have been extensively researched and filtered from a basket of all listed NSE stocks.  These Stocks have very strong fundamentals like - \n  \n • Low PE \n • Low debt to equity ratio \n • High Institutions Investment \n • High Dividend Yield \n • High EPS growth"
                //self.lblmutibagger.font = UIFont(name: "Montserrat-Regular", size: 15)

            })
            
            UIView.animate(withDuration: 2, animations: {
                self.accourdingvwHeight.constant += 200
                self.accordionvw.isHidden = false
            })
            
        } else {
            
            isAccordianTap = false
            UIView.animate(withDuration: 1, animations: {
                self.btninfo.transform = CGAffineTransform(scaleX: 1, y: 1);
            })
            UIView.animate(withDuration: 2) {
                self.accourdingvwHeight.constant -= 200
                self.accordionvw.isHidden = true
            }
        }
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    @IBAction func btn_getlist(_ sender: Any) {
        
        self.loadAddMob(success: { (success) in
            if success {
                
            }
        })
        
        
        if self.btn_tag == 1 {
            MultibaggerType.typeOfMultibagger = MultibaggerType.midCap
            MultibaggerType.typeOfMultibaggerForPastPer = MultibaggerType.midCapForPastPerformance
            MultibaggerType.typeOfMultibaggerForHeatMap = MultibaggerType.midCapForHeatmap
        } else if self.btn_tag == 2 {
            MultibaggerType.typeOfMultibagger = MultibaggerType.largeCap
            MultibaggerType.typeOfMultibaggerForPastPer = MultibaggerType.LargeCapForPastPerformance
            MultibaggerType.typeOfMultibaggerForHeatMap = MultibaggerType.largeCapForHeatmap
        }
        
        DispatchQueue.main.async {
            
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
        
            self.navigationController?.pushViewController(viewController, animated: false)
        }
        
    }
    
    @IBAction func btnGetList(_ sender: UIButton) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        
        print("Testing Will PresentScreen")
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        
        print("Add id REceived")
        self.interstitial.present(fromRootViewController: self)
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        
        
        if self.btn_tag == 1 {
            MultibaggerType.typeOfMultibagger = MultibaggerType.midCap
            MultibaggerType.typeOfMultibaggerForPastPer = MultibaggerType.midCapForPastPerformance
            MultibaggerType.typeOfMultibaggerForHeatMap = MultibaggerType.midCapForHeatmap
        } else if self.btn_tag == 2 {
            MultibaggerType.typeOfMultibagger = MultibaggerType.largeCap
            MultibaggerType.typeOfMultibaggerForPastPer = MultibaggerType.LargeCapForPastPerformance
            MultibaggerType.typeOfMultibaggerForHeatMap = MultibaggerType.largeCapForHeatmap
        }
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
        self.navigationController?.pushViewController(viewController, animated: false)
        
    }
    
    @IBAction func onClickDashboardAction(_ sender: UIButton) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func onClickContactUsAction(_ sender: UIButton) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
     }
    
    
    // MARK: Adding Gesture Recogniser For Hiding Menu on tap any where
    func addGestureRecogniser(){
       let  tapGesture = UITapGestureRecognizer(target: self, action: #selector(MultibaggerLandingViewController.tapAction(sender:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let  swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(MultibaggerLandingViewController.swipeAction(sender:)))
        swipeGesture.direction = .left
        self.view.addGestureRecognizer(swipeGesture)
    }
    
    //MARK: GESTURE RECONGISER METHODE
    func tapAction(sender: UITapGestureRecognizer) {
        
        instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
        btnHamburger.frame.origin.x -= 250
        isShowingMenu = false
    }
    func swipeAction(sender: UISwipeGestureRecognizer) {
        
        instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
        btnHamburger.frame.origin.x -= 250
        isShowingMenu = false
    }

    
    @IBAction func searchAction(_ sender: Any) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController       
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
        
    }
    
    
    
    // MARK : ADD Slide Menu
    @IBAction func onClickHambergerIcon(_ sender: UIButton) {
        
        if !isShowingMenu {
            
            //addGestureRecogniser()
            
            instanceOfLeftSlideMenu.prepareScreenWithView(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x += 250
            isShowingMenu = true
        } else {
            
            instanceOfLeftSlideMenu.hideSlideMenu(navigationController: self.navigationController!, viewSize: self.view)
            btnHamburger.frame.origin.x -= 250
            isShowingMenu = false
        }

    }
    
}
