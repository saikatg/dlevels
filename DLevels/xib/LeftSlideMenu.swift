//
//  LeftSlideMenu.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 02/02/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit


class LeftSlideMenu: UIView, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var lbl_version: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
    
    @IBOutlet weak var tblSlideMenuLlist: UITableView!
    var navController: UINavigationController?
    
    let nameArray = ["Multibaggers","Dynamic Indices","Invest in Our PMS",  "NRI Investments", "FPI Investments", "Webinars", "Free Portfolio Checkup", "Privacy Policy","Contact Us","Log Out"]
    let imageName = ["Multibager","SmallcapIndex","PMSHamburger","FPI","FPI","Webinars","Pms","PrivacyPolicy","ContactUs","LogOut"]
    
    var nibView: UIView!
    
    func prepareScreenWithView(navigationController: UINavigationController, viewSize: UIView) {
        
        
        nibView = Bundle.main.loadNibNamed("LeftSlideMenu", owner: self, options: nil)?[0] as! UIView
        
        navController = navigationController
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = UIColor.clear
        
        nibView.tag = 100
        var tempRect = nibView.frame
        print(tempRect)
        
        lblUserName.text = "\(User.firstName)  \(User.lastName)"
        lblUserEmail.text = User.email
        
        
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        print(version)
        lbl_version.text = "ver \(version)"
        
        
        tempRect =  CGRect(x: -250, y: 0, width: tempRect.width, height: UIScreen.main.bounds.height)
        viewSize.addSubview(self.nibView)
        
        self.tblSlideMenuLlist.tableHeaderView = nil;
        self.tblSlideMenuLlist.tableFooterView = nil;
        
        let sectionIndex = IndexSet(integer: 0)
        tblSlideMenuLlist.reloadSections(sectionIndex, with: .top)
        
        UIView.transition(with: self.nibView, duration: 0.4, options: [.curveEaseIn], animations: {
            
            tempRect.origin.x +=  250
            self.nibView.frame = tempRect
            
        }) { (success) in
            
        }
       
    }
    
    
    
    @IBAction func btn_dlevelslink(_ sender: Any) {
        
        
        UIApplication.shared.openURL(NSURL(string: "https://www.dynamiclevels.com")! as URL)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // let cell
        
        let cell = Bundle.main.loadNibNamed("TableViewCell", owner: nil, options: nil)?[0] as! UITableViewCell
        cell.selectionStyle = .none
        
        let lblName = cell.viewWithTag(2002) as! UILabel
        print(nameArray[indexPath.row])
        lblName.text = nameArray[indexPath.row]
        let image = cell.viewWithTag(2001) as? UIImageView
        image?.image = UIImage(named: imageName[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let currentCell = indexPath.row
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if currentCell == 0 {
            
                       let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "MultibaggerViewController") as! MultibaggerViewController
            
            
                viewcontroller.isFromLeftMenu = true
                       navController?.pushViewController(viewcontroller, animated: true)
            

        }   else if currentCell == 1 {
            
            let viewController = storyBoard.instantiateViewController(withIdentifier: "DynamicIndexController") as! DynamicIndexController
            navController?.pushViewController(viewController, animated: true)
            
        }    else if currentCell == 7 {
            
            let viewController = storyBoard.instantiateViewController(withIdentifier: "TermAndConditions") as! DisclaimerViewController
            navController?.pushViewController(viewController, animated: true)
            
        } else if currentCell == 8 {
            
            let viewController = storyBoard.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
            navController?.pushViewController(viewController, animated: true)
            
        } else if currentCell == 4 {
            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "AllFPIViewController") as! AllFPIViewController
            navController?.pushViewController(viewcontroller, animated: true)
        }else if currentCell == 5 {
            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "WebinarsViewController") as! WebinarsViewController
            navController?.pushViewController(viewcontroller, animated: true)
        }
        
        
        else if currentCell == 6 {
            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "PortfolioCheckupViewController") as! PortfolioCheckupViewController
            navController?.pushViewController(viewcontroller, animated: true)
        } else if currentCell == 2 {
            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "ALLPMSController") as! ALLPMSController
            navController?.pushViewController(viewcontroller, animated: true)
        }else if currentCell == 3 {
            let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "AllControllerContainer") as! AllControllerContainer
            navController?.pushViewController(viewcontroller, animated: true)
        }
        else if currentCell == 9 {
            
            if(User.regThrough == "gmail")
            {
                GIDSignIn.sharedInstance().signOut()
            }
            
            let webService = WebService()
            webService.callWebServices(url: Urls.logOut, methodName: "POST", parameters: "", istoken: true, tokenval: User.token, completion: { (success, jsonResult) in
                
                DispatchQueue.main.async {
                    
                    UserDefaults.standard.set(nil, forKey: "phone")
                    UserDefaults.standard.set(nil, forKey: "email")
                    UserDefaults.standard.set(nil, forKey: "password")
                    UserDefaults.standard.set(nil, forKey: "regThrough")
                    
                    
                    
                    let viewcontroller = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.navController?.pushViewController(viewcontroller, animated: false)
                    
                    
                }
            })
        }
    }
    
    func share_link(controller:UINavigationController)
    {
        
        // let textToShare = "Swift is awesome!   http://www.codingexplorer.com/ Check out this website about it!"
        let textToShare = "DLEVELS Download the DLEVELS App now- the best App for getting the latest Multibaggers.You should try it too. https://itunes.apple.com/us/app/dlevels/id1204153729?ls=1&mt=8"
        
        //if NSURL(string: http://www.codingexplorer.com/") != nil {
        let objectsToShare = [textToShare]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        //New Excluded Activities Code
        activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
        //
        
        // self.presentingViewController(activityVC,animated:true,	completion :nil)
        // activityVC.popoverPresentationController?.sourceView = sender
        controller.present(activityVC, animated: true, completion: nil)
        // }
        
        
    }
    
    
    
    @IBAction func btn_share(_ sender: Any) {
        
         share_link(controller:navController!)        

        
    }
    
    //MARK: For Hiding Left Slide View
    func hideSlideMenu(navigationController: UINavigationController,viewSize: UIView) {
        
        UIView.animate(withDuration: 0.4) {
            for views in viewSize.subviews {
                if views.tag == 100 {
                    
                    navigationController.navigationBar.frame.origin.x = 0
                    
                    self.nibView.frame.origin.x = -self.nibView!.frame.width
                    AppManager().setStatusBarBackgroundColor()
                    
                }
            }
        }
    }
    
    
    func addGestureRecogniser(){
        let  tapGesture = UITapGestureRecognizer(target: self, action: #selector(MultibaggerLandingViewController.tapAction(sender:)))
            nibView.addGestureRecognizer(tapGesture)
        
        let  swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(MultibaggerLandingViewController.tapAction(sender:)))
        swipeGesture.direction = .left
        nibView.addGestureRecognizer(swipeGesture)
    }
    
    //MARK: GESTURE RECONGISER METHODE
    func tapAction(sender: UITapGestureRecognizer) {
        
        //self.hideSlideMenu(navigationController: navController!, viewSize: nibView)
        //btnHamburger.frame.origin.x -= 250
       // isShowingMenu = false
    }
  
    
    func hideSlideMenu(navigationController: UINavigationController,viewSize: UIView, navigationView: UIView) {
        
        
        UIView.animate(withDuration: 0.4) {
            for views in viewSize.subviews {
                if views.tag == 100 {
                    //navigationController.navigationBar.frame.origin.x = 0
                    navigationView.frame.origin.x = 0
                    self.nibView.frame.origin.x = -self.nibView!.frame.width
                    AppManager().setStatusBarBackgroundColor()
                }
            }
        }
    }
}
