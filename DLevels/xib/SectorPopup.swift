//
//  LeftSlideMenu.swift
//  DLevels
//
//  Created by Dynamic-Mac-02 on 02/02/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit


class SectorPopup: UIView, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {
    
    var countryListArray = [NSDictionary]()
    var countryListArrayToBindTableView = [NSDictionary]()
    var month = 3
    @IBOutlet weak var lblMonthChange: UILabel!
    @IBOutlet weak var lblNodataAvailable: UILabel!
    var flag_sort:Int = 1
    var sortedBydata : NSArray = []
    @IBOutlet weak var searchcontroller: UISearchBar!
  
    
    @IBOutlet weak var tblSlideMenuLlist: UITableView!
    var navController: UINavigationController?
    
    let nameArray = ["My Dashboard","Multibaggers","Privacy Policy","Contact Us"]
    let imageName = ["Dashboard","Multibager","Disclaimer","ContactUs"]
    
    var nibView: UIView!
    
    //MARK: Prepare Screen
    func prepareScreenWithView(navigationController: UINavigationController, viewSize: UIView) {
        
        nibView = Bundle.main.loadNibNamed("SectorPopup", owner: self, options: nil)?[0] as! UIView
        
        navController = navigationController
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = UIColor.clear
        
        nibView.tag = 100
        var tempRect = nibView.frame
        //print(tempRect)
        searchcontroller.delegate = self
        searchcontroller.becomeFirstResponder()
        self.lblNodataAvailable.isHidden = true
        countrylist()  // Loading data for table
        tempRect =  CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        viewSize.addSubview(self.nibView)
        
        let sectionIndex = IndexSet(integer: 0)
        tblSlideMenuLlist.reloadSections(sectionIndex, with: .top)
        
        UIView.transition(with: self.nibView, duration: 0.4, options: [.curveEaseIn], animations: {
          
            self.nibView.frame = tempRect
            
        }) { (success) in
            
        }
    }
    
    
    // called whenever text is changed.
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
            loadDataForSearchResult(termval: searchText)
            if(searchText == "")
        {
           //ba self.countryListArray.removeAll()
            self.tblSlideMenuLlist.reloadData()
            
        }
    }

    func loadDataForSearchResult(termval : String)
    {
        
        self.countryListArray.removeAll()
        for dict in self.countryListArrayToBindTableView {
            
            let countryname = dict.value(forKey: "CountryName") as! String
            
            
                if(countryname.hasPrefix(termval))
                {
                    countryListArray.append(dict)
                }
                if (termval == ""){
                   countryListArray = countryListArrayToBindTableView
            }
            
            
        }
        
        //	print(countryListArray)
        self.tblSlideMenuLlist.reloadData()
    }
   
        
    
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return countryListArray.count
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35.0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*CountryCode.countryCode = countryListArray[indexPath.row].value(forKey: "CountryCode") as! String
        
        closePopup()*/
        
        //getting the index path of selected row
        let indexPath = tableView.indexPathForSelectedRow
        
        //getting the current cell from the index path
        let currentCell = tableView.cellForRow(at: indexPath!)! as UITableViewCell
        
        //getting the text of that cell
        let currentItem = currentCell.textLabel!.text
        
        let alertController = UIAlertController(title: "Simplified iOS", message: "You Selected " + currentItem! , preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Close Alert", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        //presentView(alertController, animated: true, completion: nil)
        
        
        
        

    }
    
    
    
    
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = Bundle.main.loadNibNamed("TableViewCell", owner: nil, options: nil)?[1] as! UITableViewCell
        cell.selectionStyle = .none
        
        
        
        let index = countryListArray[indexPath.row]
        
        let CountryName = cell.viewWithTag(2002) as! UILabel
        CountryName.text = index.value(forKey: "CountryName") as? String
        CountryName.adjustsFontSizeToFitWidth = true
      
        
        
        let next = cell.viewWithTag(2003) as! UILabel
        next.text = ""
        
        let CountryCode = cell.viewWithTag(2004) as! UILabel
        CountryCode.text = index.value(forKey: "CountryCode") as? String

                
        
        return cell
    }
    
    
    @IBAction func btn_Cross_OnClick(_ sender: Any) {
     
       closePopup()
    }
    
    func closePopup(){
    
        UIView.transition(with: self.nibView, duration: 1, options: [.curveEaseInOut], animations: {
            
            self.nibView.frame.origin.y -=  200
            
        }) { (success) in
            self.countryListArray.removeAll()
            self.nibView.removeFromSuperview()
        }
    }
    
    //MARK: Getting Data From Array
    func countrylist()
    {
        let obj = WebService()
        
        obj.callWebServices(url: Urls.country_code, methodName: "GET", parameters: "", istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            //print("country code : \(jsonData)")
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
               
            
                let tempArrayDict : [NSDictionary] = jsonData.value(forKey: "response") as! [NSDictionary]
                
                for dictValue in tempArrayDict {
                    
                    self.countryListArray.append(dictValue)
                    self.countryListArrayToBindTableView.append(dictValue)
                    
                }
                
                
                
                DispatchQueue.main.async {
                    
                    if self.countryListArray.count == 0 {
                        self.lblNodataAvailable.isHidden = false
                    } else {
                        self.lblNodataAvailable.isHidden = true
                    }
                 let sectionIndex = IndexSet(integer: 0)
                 self.tblSlideMenuLlist.reloadSections(sectionIndex, with: .automatic)
 
                }
            }
            else
            {
                let alertobj = AppManager()
                
               // print(jsonData.value(forKey: "errmsg") as! String)
                
            }
        }
    }
    
    
    
    //MARK: For Hiding Left Slide View
    func hideSlideMenu(navigationController: UINavigationController,viewSize: UIView) {
        
        UIView.animate(withDuration: 0.4) {
            for views in viewSize.subviews {
                if views.tag == 100 {
                    
                    navigationController.navigationBar.frame.origin.x = 0
                    
                    self.nibView.frame.origin.x = -self.nibView!.frame.width
                    
                    self.nibView.removeFromSuperview()
                    AppManager().setStatusBarBackgroundColor()
                    
                }
            }
        }
    }
    
    
    @IBAction func btnRemoveViewClick(_ sender: UIButton) {
        
        UIView.transition(with: self.nibView, duration: 1, options: [.curveEaseInOut], animations: {
            
            self.nibView.frame.origin.y -=  200
            
        }) { (success) in
            self.countryListArray.removeAll()
            self.nibView.removeFromSuperview()
            
        }
        
    }
    
    
    func hideSlideMenu(navigationController: UINavigationController,viewSize: UIView, navigationView: UIView) {
        
        UIView.animate(withDuration: 0.4) {
            for views in viewSize.subviews {
                if views.tag == 100 {
                    navigationView.frame.origin.x = 0
                    self.nibView.frame.origin.x = -self.nibView!.frame.width
                    AppManager().setStatusBarBackgroundColor()
                }
            }
        }
    }
}
