//
//  SearchViewController.swift
//  DLevels
//
//  Created by Dynamic-Mac-01 on 26/05/17.
//  Copyright © 2017 Dynamic-Mac-01. All rights reserved.
//

import Foundation
import UIKit

class SearchViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate , UINavigationControllerDelegate {
    
    
   
    @IBOutlet weak var searchcontroller: UISearchBar!
    
    @IBOutlet weak var tblviewresult: UITableView!
    var tremvalstring = ""
    var response = [NSDictionary]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchcontroller.delegate = self
        searchcontroller.becomeFirstResponder()
        searchcontroller.placeholder = "Search Stocks to know buy, hold or exit"
        tblviewresult.dataSource = self
        tblviewresult.delegate = self
        
        tblviewresult.estimatedRowHeight = 1000
        tblviewresult.rowHeight = UITableViewAutomaticDimension
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.shouldRotate = false // or false to disable rotation
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        AppUtility.lockOrientation(.all)
        
        tblviewresult.dataSource = self
        tblviewresult.delegate = self
        
        tblviewresult.estimatedRowHeight = 1000
        tblviewresult.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewDidAppear(_ animated: Bool) {
        searchcontroller.becomeFirstResponder()
    }
    
    // called whenever text is changed.
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(searchText.characters.count > 1)
        {
            loadDataForResultView(termval: searchText,pagenameval: "")
        }
        else
        {
            self.response.removeAll()
            self.tblviewresult.reloadData()
        }
        
        if(searchText == "")
        {
            self.response.removeAll()
            self.tblviewresult.reloadData()
        
        }
    }
    
    
    
    @IBAction func btnback(_ sender: Any) {
        
//        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MultibaggerLandingViewController") as! MultibaggerLandingViewController
//
//        self.navigationController?.pushViewController(viewController, animated: true)
        self.navigationController?.popViewController(animated: false)
        
    }
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        return UITableViewAutomaticDimension
//    }
//    
//    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
//    
    
    
    //MARK: Table View Delegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var instrumrnt2 = ""
        var exchname = ""
        var exch = ""
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"searchcell", for: indexPath)
        cell.selectionStyle = .none
        
        cell.backgroundColor = indexPath.row % 2 == 0 ?  UIColor(red: 215/255, green: 222/255, blue: 225/255, alpha: 1.0) : UIColor(red: 236/255, green: 239/255, blue: 240/255, alpha: 1.0)
        
        let lblinstrument2 = cell.viewWithTag(1001) as! UILabel
//        instrumrnt2 = self.response[indexPath.row].value(forKey: "INSTRUMENT_2") as! String
//        lblinstrument2.text = instrumrnt2

        if let instrumrnt2 = self.response[indexPath.row].value(forKey: "INSTRUMENT_2") {
            lblinstrument2.text = instrumrnt2 as! String
//            
            print("INSTRUMENT : \(instrumrnt2)")

        }
//        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
//        
//        let underlineAttributedString = NSAttributedString(string: (self.response[indexPath.row].value(forKey: "INSTRUMENT_2") as? String)!, attributes: underlineAttribute)
//        
//        lblinstrument2.attributedText = underlineAttributedString
        
        
        if let exchangename = self.response[indexPath.row].value(forKey: "EXCHANGE_NAME") as? String{
            exchname = exchangename
        }
        
        if let exchange = self.response[indexPath.row].value(forKey: "EXCHANGE_1") as? String{
            exch = exchange
        }
        
        let lblexchange1 = cell.viewWithTag(1002) as! UILabel
        lblexchange1.text = "\(exch) , \(exchname)"
        print("Exchange : \(lblexchange1)")
        return cell
    }
    
    //MARK: Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if response.count > 0 {
            
            return response.count
        } else {
            
            return 0
        }
        
        //return response.count
    }
    //MARK: Orientati
    func navigationControllerSupportedInterfaceOrientations(_ navigationController: UINavigationController) -> UIInterfaceOrientationMask {
        return .portrait
    }
   
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        SearchForCurrentMultibagger.serchValue = response[indexPath.row].value(forKey: "Symbol_Name") as! String
        
        SearchForCurrentMultibagger.instrument_4 = response[indexPath.row].value(forKey: "INSTRUMENT_2") as! String
        SearchForCurrentMultibagger.viewcontrollername = "Search"
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "StockSpecificNewViewController") as! StockSpecificNewViewController
        self.navigationController?.pushViewController(viewController, animated: false)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK: Getting data
    func loadDataForResultView(termval: String, pagenameval: String)
    {
        
        tremvalstring = termval.replacingOccurrences(of: " ", with: "%20")
        
        let obj = WebService()
        let paremeters = "term=\(tremvalstring)&pageName=\(pagenameval)"
        
        obj.callWebServices(url: Urls.autosearch_stock, methodName: "GET", parameters: paremeters, istoken: true, tokenval: User.token) { (returnValue, jsonData) in
            
            print(jsonData)
            
            if jsonData.value(forKey: "errmsg") as! String == "" {
                
                let tempArray = jsonData.value(forKey: "response") as! NSArray
                
                self.response.removeAll()
                
                for dict in tempArray {
                    let dictValue = dict as! NSDictionary
                    self.response.append(dictValue)
                }
                
               print("Response count is: \(self.response.count)")
                
            //    DispatchQueue.main.async {
                 
                    if self.response.count > 0 {
                        self.tblviewresult.reloadData()
                    } else {
                        
                        self.response.removeAll()
                        self.tblviewresult.reloadData()
                    }
                    
               // }
            }
            else
            {
                let alertobj = AppManager()
                alertobj.showAlert(title: "Error!", message: jsonData.value(forKey: "errmsg") as! String , navigationController: self.navigationController!)
            }
            
            
        }
        
    }
}
